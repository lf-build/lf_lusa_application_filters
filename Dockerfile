FROM registry.lendfoundry.com/base:beta8

ADD ./src/LendFoundry.Applications.Filters.Abstractions /app/LendFoundry.Applications.Filters.Abstractions
WORKDIR /app/LendFoundry.Applications.Filters.Abstractions
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Applications.Filters.Persistence /app/LendFoundry.Applications.Filters.Persistence
WORKDIR /app/LendFoundry.Applications.Filters.Persistence
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Applications.Filters /app/LendFoundry.Applications.Filters
WORKDIR /app/LendFoundry.Applications.Filters
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Applications.Filters.Api /app/LendFoundry.Applications.Filters.Api
WORKDIR /app/LendFoundry.Applications.Filters.Api
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel
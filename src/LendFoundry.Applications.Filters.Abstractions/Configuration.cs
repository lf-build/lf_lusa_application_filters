﻿using System.Collections.Generic;

namespace LendFoundry.Applications.Filters
{
    public class Configuration
    {
        public EventMapping[] Events { get; set; }
        public Dictionary<string, Rule> Tags { get; set; }
        public string[] ApprovedStatuses { get; set; }
        public string[] SubmittedStatuses { get; set; }
        public string[] StatusesNotExpired { get; set; }
    }
}
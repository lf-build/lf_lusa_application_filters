﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.Applications.Filters
{
    public class FilterView : Aggregate, IFilterView
    {
        public string ApplicationId { get; set; }
        public string Applicant { get; set; }
        public DateTimeOffset Submitted { get; set; }
        public double Amount { get; set; }
        public double MaxApproval { get; set; }
        public double Payout { get; set; }
        public double PaymentAmount { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public string SourceType { get; set; }
        public string SourceId { get; set; }
        public IEnumerable<string> Tags { get; set; }
        public double InterestRate { get; set; }
        public TimeBucket FundedDate { get; set; }
        public DateTimeOffset ExpirationDate { get; set; }
        public string StatusLabel { get; set; }
        public string Ssn { get; set; }
        public TimeBucket ActiveOn { get; set; }
        public Dictionary<string, string> Assignees { get; set; }
    }
}
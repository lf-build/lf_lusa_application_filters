﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.Applications.Filters
{
    public interface IFilterView: IAggregate
    {
        string ApplicationId { get; set; }
        string Applicant { get; set; }
        DateTimeOffset Submitted { get; set; }
        double Amount { get; set; }
        double MaxApproval { get; set; }
        double Payout { get; set; }
        double PaymentAmount { get; set; }
        string StatusCode { get; set; }
        string StatusName { get; set; }
        string SourceType { get; set; }
        string SourceId { get; set; }
        IEnumerable<string> Tags { get; set; }
        double InterestRate { get; set; }
        TimeBucket FundedDate { get; set; }
        DateTimeOffset ExpirationDate { get; set; }
        string StatusLabel { get; set; }
        string Ssn { get; set; }
        TimeBucket ActiveOn { get; set; }
        Dictionary<string, string> Assignees { get; set; }
    }
}
﻿using System;

namespace LendFoundry.Applications.Filters
{
    public interface IFilterViewUnassigned
    {
        string ApplicationNumber { get; set; }

        string FullName { get; set; }

        string Last4Ssn { get; set; }

        DateTimeOffset ApplicationDate { get; set; }
    }
}
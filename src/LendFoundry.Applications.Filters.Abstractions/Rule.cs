﻿namespace LendFoundry.Applications.Filters
{
    public class Rule
    {
        public string Name { get; set; }
        public string Version { get; set; }
    }
}

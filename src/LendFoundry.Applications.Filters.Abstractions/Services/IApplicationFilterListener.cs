﻿namespace LendFoundry.Applications.Filters
{
    public interface IApplicationFilterListener
    {
        void Start();
    }
}

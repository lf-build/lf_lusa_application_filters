﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Applications.Filters
{
    public interface IApplicationFilterService
    {
        IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId);

        IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId);

        Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId);

        Task<int> GetTotalSubmittedForCurrentMonthAndStatus(string sourceType, string sourceId);

        Task<int> GetTotalSubmittedForCurrentYearAndStatus(string sourceType, string sourceId);

        Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name);

        Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<IEnumerable<IFilterView>> GetByTag(string sourceType, string sourceId, IEnumerable<string> tags);

        Task<int> GetCountByTag(string sourceType, string sourceId, IEnumerable<string> tags);

        double GetTotalAmountApprovedBySource(string sourceType, string sourceId);

        Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber);

        Task<IEnumerable<IFilterView>> GetAllExpiredApplications();

        Task<IEnumerable<IFilterViewUnassigned>> GetUnassignedByStatus(IEnumerable<string> statuses);

        Task<IEnumerable<IFilterViewUnassigned>> GetUnassignedByTags(IEnumerable<string> tags);

        Task<IEnumerable<IFilterViewUnassigned>> GetAllUnassigned();

        Task<IEnumerable<IFilterView>> GetAllAssignedApplications();
        Task<IEnumerable<IFilterView>> GetAll();
        Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses);
        Task<IEnumerable<IFilterView>> GetByTags(IEnumerable<string> tags);
        Task<IEnumerable<IFilterView>> GetAssignedByStatus(IEnumerable<string> statuses);
        Task<IEnumerable<IFilterView>> GetAssignedByTags(IEnumerable<string> tags);
    }
}
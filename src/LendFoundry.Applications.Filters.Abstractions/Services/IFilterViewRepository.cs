﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Applications.Filters
{
    public interface IFilterViewRepository : IRepository<IFilterView>
    {
        void AddOrUpdate(IFilterView view);
        IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId);
        IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses);
        Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId, DateTimeOffset currentMonth);
        Task<int> GetTotalSubmittedForCurrentMonthAndStatus(string sourceType, string sourceId, DateTimeOffset currentMonth, IEnumerable<string> validStatuses);
        Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId, DateTimeOffset currentYear);
        Task<int> GetTotalSubmittedForCurrentYearAndStatus(string sourceType, string sourceId, DateTimeOffset currentYear, IEnumerable<string> validStatuses);
        Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name);
        Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);
        Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);
        Task<IEnumerable<IFilterView>> GetByTag(string sourceType, string sourceId, IEnumerable<string> tags);
        Task<int> GetCountByTag(string sourceType, string sourceId, IEnumerable<string> tags);
        double GetTotalAmountApprovedBySourceAndStatus(string sourceType, string sourceId, string[] statuses);
        Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber);
        Task<IEnumerable<IFilterView>> GetAllExpiredApplications(DateTimeOffset todayDate,string[] verifiedStatuses);
        Task<IEnumerable<IFilterViewUnassigned>> GetUnassignedByStatus(IEnumerable<string> roles, IEnumerable<string> statuses);
        Task<IEnumerable<IFilterViewUnassigned>> GetUnassignedByTags(IEnumerable<string> roles, IEnumerable<string> tags);
        Task<IEnumerable<IFilterViewUnassigned>> GetAllUnassigned(IEnumerable<string> roles);
        Task<IEnumerable<IFilterView>> GetAllAssignedApplications(string userName);
        Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses);
        Task<IEnumerable<IFilterView>> GetByTags(IList<string> tags);
        Task<IEnumerable<IFilterView>> GetAssignedByStatus(string userName,IList<string> statuses);
        Task<IEnumerable<IFilterView>> GetAssignedByTags(string userName,string[] tags);
        Task<IEnumerable<IFilterView>> GetAll();
    }
}
﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Applications.Filters
{
    public interface IFilterViewRepositoryFactory
    {
        IFilterViewRepository Create(ITokenReader reader);
    }
}

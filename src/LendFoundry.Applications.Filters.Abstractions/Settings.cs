﻿using LendFoundry.Foundation.Services.Settings;

namespace LendFoundry.Applications.Filters
{
    public class Settings
    {
        public static string ServiceName { get; } = "application-filters";
        private static string Prefix { get; } = ServiceName.ToUpper();
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");
        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");
        public static ServiceSettings OfferEngine { get; } = new ServiceSettings($"{Prefix}_OFFER_ENGINE", "offer-engine");
        public static ServiceSettings Application { get; } = new ServiceSettings($"{Prefix}_APPLICATION", "application");
        public static ServiceSettings StatusManagement { get; } = new ServiceSettings($"{Prefix}_STATUS_MANAGEMENT", "status-management");
        public static ServiceSettings DecisionEngine { get; } = new ServiceSettings($"{Prefix}_DECISION_ENGINE_RUNTIME", "decision-engine-runtime");
        public static ServiceSettings Assignment { get; } = new ServiceSettings($"{Prefix}_ASSIGNMENT", "assignment");
        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", ServiceName);
        public static ServiceSettings Identity { get; } = new ServiceSettings($"{Prefix}_IDENTITY", "identity");
    }
}
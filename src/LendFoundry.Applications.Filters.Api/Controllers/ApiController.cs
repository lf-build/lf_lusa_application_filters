﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace LendFoundry.Applications.Filters.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IApplicationFilterService service, ILogger logger) : base(logger)
        {
            if (service == null) throw new ArgumentException($"{nameof(service)} is madatory");

            Service = service;
        }

        private IApplicationFilterService Service { get; }

        [HttpGet("/{sourceType}/{sourceId}/all")]
        public IActionResult GetAllBySource(string sourceType, string sourceId)
        {
            return Execute(() => new HttpOkObjectResult(Service.GetAllBySource(sourceType, sourceId)));
        }

        [HttpGet("/{sourceType}/{sourceId}/metrics/submit/month")]
        public Task<IActionResult> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId)
        {
            return ExecuteAsync(async () => new HttpOkObjectResult(await Service.GetTotalSubmittedForCurrentMonthAndStatus(sourceType, sourceId)));
        }

        [HttpGet("/{sourceType}/{sourceId}/metrics/submit/year")]
        public Task<IActionResult> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId)
        {
            return ExecuteAsync(async () => new HttpOkObjectResult(await Service.GetTotalSubmittedForCurrentYearAndStatus(sourceType, sourceId)));
        }

        [HttpGet("/{sourceType}/{sourceId}/status/{*statuses}")]
        public Task<IActionResult> GetByStatus(string sourceType, string sourceId, string statuses)
        {
            return ExecuteAsync(async () =>
            {
                var listStatus = statuses?.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return new HttpOkObjectResult(await Service.GetByStatus(sourceType, sourceId, listStatus));
            });
        }

        [HttpGet("/{sourceType}/{sourceId}/count/status/{*statuses}")]
        public Task<IActionResult> GetCountByStatus(string sourceType, string sourceId, string statuses)
        {
            return ExecuteAsync(async () =>
            {
                var listStatus = statuses?.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return new HttpOkObjectResult(await Service.GetCountByStatus(sourceType, sourceId, listStatus));
            });
        }

        [HttpGet("/{sourceType}/{sourceId}/tag/{*tags}")]
        public Task<IActionResult> GetByTag(string sourceType, string sourceId, string tags)
        {
            return ExecuteAsync(async () =>
            {
                var listTags = tags?.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return new HttpOkObjectResult(await Service.GetByTag(sourceType, sourceId, listTags));
            });
        }

        [HttpGet("/{sourceType}/{sourceId}/count/tag/{*tags}")]
        public Task<IActionResult> GetCountByTag(string sourceType, string sourceId, string tags)
        {
            return ExecuteAsync(async () =>
            {
                var listTags = tags?.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return new HttpOkObjectResult(await Service.GetCountByTag(sourceType, sourceId, listTags));
            });
        }

        [HttpGet("/{sourceType}/{sourceId}/search/{name}")]
        public Task<IActionResult> GetByName(string sourceType, string sourceId, string name)
        {
            return ExecuteAsync(async () =>
            {
                name = WebUtility.UrlDecode(name);
                return new HttpOkObjectResult(await Service.GetByName(sourceType, sourceId, name));
            });
        }

        [HttpGet("/{sourceType}/{sourceId}/metrics/total-approved-amount")]
        public IActionResult GetTotalAmountApprovedBySource(string sourceType, string sourceId)
        {
            return Execute(() => Ok(Service.GetTotalAmountApprovedBySource(sourceType, sourceId)));
        }

        [HttpGet("/{sourceType}/{sourceId}/{applicationNumber}")]
        public Task<IActionResult> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            return ExecuteAsync(async () => new HttpOkObjectResult(await Service.GetByApplicationNumber(sourceType, sourceId, applicationNumber)));
        }

        [HttpGet("/expired/all")]
        public async Task<IActionResult> GetAllExpiredApplications()
        {
            return await ExecuteAsync(async () => new HttpOkObjectResult(await Service.GetAllExpiredApplications()));
        }

        [HttpGet("/unassigned/status/{*statuses}")]
        public Task<IActionResult> GetUnassignedByStatus(string statuses)
        {
            return ExecuteAsync(async () =>
            {
                var listStatus = statuses?.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return new HttpOkObjectResult(await Service.GetUnassignedByStatus(listStatus));
            });
        }

        [HttpGet("/unassigned/tag/{*tags}")]
        public Task<IActionResult> GetUnassignedByTags(string tags)
        {
            return ExecuteAsync(async () =>
            {
                var listTags = tags?.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return new HttpOkObjectResult(await Service.GetUnassignedByTags(listTags));
            });
        }

        [HttpGet("/unassigned/all")]
        public Task<IActionResult> GetAllUnassigned()
        {
            return ExecuteAsync(async () => new HttpOkObjectResult(await Service.GetAllUnassigned()));
        }

        [HttpGet("/mine")]
        public Task<IActionResult> GetAllAssignedApplications()
        {
            return ExecuteAsync(async () => new HttpOkObjectResult(await Service.GetAllAssignedApplications()));
        }

        [HttpGet("/assigned/status/{*statuses}")]
        public async Task<IActionResult> GetAssignedByStatus(string statuses)
        {
            return await ExecuteAsync(async () =>
            {
                var listStatus = statuses?.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return new HttpOkObjectResult(await Service.GetAssignedByStatus(listStatus));
            });
        }

        [HttpGet("/assigned/tags/{*tags}")]
        public async Task<IActionResult> GetAssignedByTags(string tags)
        {
            return await ExecuteAsync(async () =>
            {
                var listTags = tags?.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return new HttpOkObjectResult(await Service.GetAssignedByTags(listTags));
            });
        }

        [HttpGet("/all")]
        public async Task<IActionResult> GetAll()
        {
            return await ExecuteAsync(async () => new HttpOkObjectResult(await Service.GetAll()));
        }

        [HttpGet("/status/{*statuses}")]
        public async Task<IActionResult> GetAllByStatus(string statuses)
        {
            return await ExecuteAsync(async () =>
            {
                var listStatus = statuses?.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return new HttpOkObjectResult(await Service.GetByStatus(listStatus));
            });
        }

        [HttpGet("/tags/{*tags}")]
        public async Task<IActionResult> GetAllByTags(string tags)
        {
            return await ExecuteAsync(async () =>
            {
                var listTags = tags?.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return new HttpOkObjectResult(await Service.GetByTags(listTags));
            });
        }

    }
}
﻿using LendFoundry.Foundation.Services;
using RestSharp;
using System.Collections.Generic;
using System.Text;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace LendFoundry.Applications.Filters.Client
{
    public class ApplicationFilterClient : IApplicationFilterService
    {
        public ApplicationFilterClient(IServiceClient client) { Client = client; }

        private IServiceClient Client { get; }

        public IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId)
        {
            Validade(sourceType, sourceId);

            var request = new RestRequest("/{sourceType}/{sourceId}/all", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);

            return Client.Execute<List<FilterView>>(request);
        }

        public IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validade(sourceType, sourceId);

            var request = new RestRequest("/{sourceType}/{sourceId}/status", Method.GET);
            AppendItemsToRoute(request, statuses);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);

            return Client.Execute<List<FilterView>>(request);
        }

        public Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId)
        {
            Validade(sourceType, sourceId);

            var request = new RestRequest("/{sourceType}/{sourceId}/metrics/submit/month", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);

            var result = Client.ExecuteAsync(request, typeof(int));
            if (result.Result == null)
                return Task.FromResult(0);

            return Task.FromResult(Convert.ToInt32(result.Result));
        }

        public Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId)
        {
            Validade(sourceType, sourceId);

            var request = new RestRequest("/{sourceType}/{sourceId}/metrics/submit/year", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);

            var result = Client.ExecuteAsync(request, typeof(int));
            if (result.Result == null)
                return Task.FromResult(0);

            return Task.FromResult(Convert.ToInt32(result.Result));
        }

        public Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name)
        {
            Validade(sourceType, sourceId);

            if (string.IsNullOrWhiteSpace(name))
                throw new InvalidArgumentException($"{nameof(name)} is mandatory", nameof(name));

            var request = new RestRequest("/{sourceType}/{sourceId}/search/{name}", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);
            request.AddUrlSegment(nameof(name), name);

            return Client.ExecuteAsync<IEnumerable<IFilterView>>(request);
        }

        public async  Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validade(sourceType, sourceId);
            var request = new RestRequest("/{sourceType}/{sourceId}/status", Method.GET);
            AppendItemsToRoute(request, statuses);
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);
            return await  Client.ExecuteAsync<IEnumerable<FilterView>>(request);
        }

        
        public Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validade(sourceType, sourceId);
            var request = new RestRequest("/{sourceType}/{sourceId}/count/status", Method.GET);
            AppendItemsToRoute(request, statuses);
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);
            return Client.ExecuteAsync<int>(request);
        }

        public Task<IEnumerable<IFilterView>> GetByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            Validade(sourceType, sourceId);
            var request = new RestRequest("/{sourceType}/{sourceId}/tag", Method.GET);
            AppendItemsToRoute(request, tags);
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);
            return Client.ExecuteAsync<IEnumerable<IFilterView>>(request);
        }

        public Task<int> GetCountByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            Validade(sourceType, sourceId);
            var request = new RestRequest("/{sourceType}/{sourceId}/count/tag", Method.GET);
            AppendItemsToRoute(request, tags);
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);
            return Client.ExecuteAsync<int>(request);
        }

        public double GetTotalAmountApprovedBySource(string sourceType, string sourceId)
        {
            var request = new RestRequest("/{sourceType}/{sourceId}/metrics/total-approved-amount", Method.GET);
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);
            return Client.Execute<double>(request);
        }

        public Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            Validade(sourceType, sourceId);

            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new InvalidArgumentException($"{nameof(applicationNumber)} is mandatory", nameof(applicationNumber));

            var request = new RestRequest("/{sourceType}/{sourceId}/{applicationNumber}", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);
            request.AddUrlSegment(nameof(applicationNumber), applicationNumber);

            return Client.ExecuteAsync<IFilterView>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications()
        {
            var request = new RestRequest("/expired/all", Method.GET);
            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        private void Validade(string sourceType, string sourceId)
        {
            if (string.IsNullOrWhiteSpace(sourceType))
                throw new InvalidArgumentException($"{nameof(sourceType)} is mandatory", nameof(sourceType));

            if (string.IsNullOrWhiteSpace(sourceId))
                throw new InvalidArgumentException($"{nameof(sourceId)} is mandatory", nameof(sourceId));
        }

        public Task<IEnumerable<IFilterViewUnassigned>> GetUnassignedByStatus(IEnumerable<string> statuses)
        {
            var request = new RestRequest("/unassigned/status", Method.GET);
            AppendItemsToRoute(request, statuses);
            return Client.ExecuteAsync<IEnumerable<IFilterViewUnassigned>>(request);
        }

        public Task<IEnumerable<IFilterViewUnassigned>> GetUnassignedByTags(IEnumerable<string> tags)
        {
            var request = new RestRequest("/unassigned/tag", Method.GET);
            AppendItemsToRoute(request, tags);
            return Client.ExecuteAsync<IEnumerable<IFilterViewUnassigned>>(request);
        }

        public Task<IEnumerable<IFilterViewUnassigned>> GetAllUnassigned()
        {
            var request = new RestRequest("/unassigned/all", Method.GET);
            return Client.ExecuteAsync<IEnumerable<IFilterViewUnassigned>>(request);
        }

        public Task<IEnumerable<IFilterView>> GetAllAssignedApplications()
        {
            var request = new RestRequest("/mine", Method.GET);
            return Client.ExecuteAsync<IEnumerable<IFilterView>>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetAll()
        {
            var request = new RestRequest("/all", Method.GET);
            return await  Client.ExecuteAsync<List <FilterView>>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses)
        {
            var request = new RestRequest("/status", Method.GET);
            AppendItemsToRoute(request, statuses);
            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetByTags(IEnumerable<string> tags)
        {
            var request = new RestRequest("/tags", Method.GET);
            AppendItemsToRoute(request, tags);
            return await  Client.ExecuteAsync<List <FilterView>>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetAssignedByStatus(IEnumerable<string> statuses)
        {

            var request = new RestRequest("/assigned/status", Method.GET);
            AppendItemsToRoute(request, statuses);
            return await  Client.ExecuteAsync<List<FilterView>>(request);
        }

        public async  Task<IEnumerable<IFilterView>> GetAssignedByTags(IEnumerable<string> tags)
        {
            var request = new RestRequest("/assigned/tags", Method.GET);
            AppendItemsToRoute(request, tags);
            return await  Client.ExecuteAsync<List<FilterView>>(request);
        }

        private static void AppendItemsToRoute(IRestRequest request, IEnumerable<string> items)
        {
            if (items == null)
                return;

            var itemList = items as IList<string> ?? items.ToList();
            if (!itemList.Any())
                return;

            var url = request.Resource;
            for (var index = 0; index < itemList.Count; index++)
            {
                var tag = itemList[index];
                url = url + $"/{{item{index}}}";
                request.AddUrlSegment($"item{index}", tag);
            }
            request.Resource = url;
        }

        public async Task<int> GetTotalSubmittedForCurrentMonthAndStatus(string sourceType, string sourceId)
        {
            return await GetTotalSubmittedForCurrentMonth(sourceType, sourceId);
        }

        public async Task<int> GetTotalSubmittedForCurrentYearAndStatus(string sourceType, string sourceId)
        {
            return await GetTotalSubmittedForCurrentYear(sourceType, sourceId);
        }
    }

}
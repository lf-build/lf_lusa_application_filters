﻿using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using System;

namespace LendFoundry.Applications.Filters.Client
{
    public class ApplicationFilterClientFactory : IApplicationFilterClientFactory
    {
        public ApplicationFilterClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IApplicationFilterService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new ApplicationFilterClient(client);
        }
    }
}

﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Applications.Filters.Client
{
    internal static class NumberGeneratorServiceExtensions
    {
        public static IServiceCollection AddAppFlowService(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<IApplicationFilterClientFactory>(p => new ApplicationFilterClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IApplicationFilterClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}

﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Applications.Filters.Client
{
    public interface IApplicationFilterClientFactory
    {
        IApplicationFilterService Create(ITokenReader reader);
    }
}
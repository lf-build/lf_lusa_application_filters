﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Options;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Applications.Filters.Persistence
{
    public class FilterViewRepository : MongoRepository<IFilterView, FilterView>, IFilterViewRepository
    {
        static FilterViewRepository()
        {
            BsonClassMap.RegisterClassMap<FilterView>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Submitted).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.ExpirationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.Assignees)
                .SetSerializer(new DictionaryInterfaceImplementerSerializer<Dictionary<string, string>>
                    (
                        DictionaryRepresentation.ArrayOfDocuments
                    )
                );
                var type = typeof(FilterView);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<FilterViewUnassigned>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.ApplicationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                var type = typeof(FilterViewUnassigned);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public FilterViewRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "application-filters")
        {
            CreateIndexIfNotExists("applicant", Builders<IFilterView>.IndexKeys.Ascending(i => i.TenantId).Ascending(i=>i.Applicant));
            CreateIndexIfNotExists("status-code", Builders<IFilterView>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.StatusCode));
            CreateIndexIfNotExists("status-name", Builders<IFilterView>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.StatusName));
            CreateIndexIfNotExists("expiration-date", Builders<IFilterView>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.ExpirationDate));
        }

        public void AddOrUpdate(IFilterView view)
        {
            if (view == null)
                throw new ArgumentNullException(nameof(view));

            System.Linq.Expressions.Expression<Func<IFilterView, bool>> query = l =>
                l.TenantId == TenantService.Current.Id &&
                l.ApplicationId == view.ApplicationId;

            var existingId = Query
                .Where(v => v.ApplicationId == view.ApplicationId)
                .Select(v => v.Id)
                .FirstOrDefault();

            view.TenantId = TenantService.Current.Id;
            view.Id = existingId ?? ObjectId.GenerateNewId().ToString();

            Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
        }

        public Task<IEnumerable<IFilterView>> GetAll()
        {
            return Task.FromResult<IEnumerable<IFilterView>>(OrderIt(Query));
        }

        public Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses)
        {
            return Task.FromResult<IEnumerable<IFilterView>>(OrderIt(Query.Where(x => statuses.Contains(x.StatusCode))));
        }

        public IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId)
        {
            return OrderIt(Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId));
        }

        public IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return OrderIt(Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId && statuses.Contains(x.StatusCode)));
        }

        private IQueryable<IFilterView> OrderIt(IEnumerable<IFilterView> filterView)
        {
            return filterView.OrderBy(x => x.StatusCode)
                             .ThenBy(x => x.Submitted).AsQueryable();
        }

        public Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId, DateTimeOffset currentMonth)
        {
            return Task.FromResult(Query.Count
                (
                    app => app.SourceType.ToLower() == sourceType.ToLower() &&
                    app.SourceId == sourceId &&
                    app.Submitted >= currentMonth
                ));
        }


        public Task<int> GetTotalSubmittedForCurrentMonthAndStatus(string sourceType, string sourceId, DateTimeOffset currentMonth, IEnumerable<string> validStatuses)
        {
            return Task.FromResult(Query.Count
                (
                    app => app.SourceType.ToLower() == sourceType.ToLower() &&
                    app.SourceId == sourceId &&
                    app.Submitted >= currentMonth &&
                    validStatuses.Contains(app.StatusCode)
                ));
        }


        public Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId, DateTimeOffset currentYear)
        {
            return Task.FromResult(Query.Count
                (
                    app => app.SourceType.ToLower() == sourceType.ToLower() &&
                    app.SourceId == sourceId &&
                    app.Submitted >= currentYear
                ));
        }

        public Task<int> GetTotalSubmittedForCurrentYearAndStatus(string sourceType, string sourceId, DateTimeOffset currentYear, IEnumerable<string> validStatuses)
        {
            return Task.FromResult(Query.Count
                (
                    app => app.SourceType.ToLower() == sourceType.ToLower() &&
                    app.SourceId == sourceId &&
                    app.Submitted >= currentYear &&
                    validStatuses.Contains(app.StatusCode)
                ));
        }

        public Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name)
        {
            return Task.FromResult<IEnumerable<IFilterView>>(
                OrderIt(
                    Query.Where(a => a.SourceType.ToLower() == sourceType.ToLower()
                                    && a.SourceId == sourceId
                                    && a.Applicant.ToLower().Contains(name.ToLower()))));
        }

        public Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return Task.FromResult<IEnumerable<IFilterView>>
            (
                OrderIt(Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() &&
                                         x.SourceId == sourceId &&
                                         statuses.Contains(x.StatusCode)))
            );
        }

        public Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return Task.FromResult<int>
            (
                Query.Count(x => x.SourceType.ToLower() == sourceType.ToLower() &&
                                         x.SourceId == sourceId &&
                                         statuses.Contains(x.StatusCode))
            );
        }

        public Task<IEnumerable<IFilterView>> GetByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            return Task.FromResult<IEnumerable<IFilterView>>
            (
                OrderIt(from db in Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId).ToList()
                        from dbTags in db.Tags
                        where (from tag in tags select tag).Contains(dbTags)
                        select db).Distinct()
            );
        }

        public Task<int> GetCountByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            return Task.FromResult<int>
            (
                (from db in Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId).ToList()
                 from dbTags in db.Tags
                 where (from tag in tags select tag).Contains(dbTags)
                 select db).Distinct().Count()
            );
        }

        public double GetTotalAmountApprovedBySourceAndStatus(string sourceType, string sourceId, string[] statuses)
        {
            return Query.Where(x => x.SourceType == sourceType && x.SourceId == sourceId && statuses.Contains(x.StatusCode))
                        .Sum(x => x.Amount);
        }

        public Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            return Task.FromResult
                (
                    Query.Where(a =>
                        a.SourceType.ToLower() == sourceType.ToLower() &&
                        a.SourceId == sourceId &&
                        a.ApplicationId == applicationNumber).FirstOrDefault()
                );
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications(DateTimeOffset todayDate, string[] statusesNotExpired)
        {

            return await Task.FromResult
            (

               Query.Where(p => p.ExpirationDate < todayDate && !statusesNotExpired.Contains(p.StatusCode)).ToList()

            );
        }

        public async Task<IEnumerable<IFilterViewUnassigned>> GetUnassignedByStatus(IEnumerable<string> roles, IEnumerable<string> statuses)
        {
            var builders = Builders<IFilterView>.Filter;
            var filter = builders.AnyNin("Assignees.k", roles);
            var tenantId = TenantService.Current.Id;
            var selectQuery = Collection.Find(filter).ToEnumerable().AsQueryable().Where(i => i.TenantId == tenantId);

            return await Task.FromResult
            (
                selectQuery
                .Where(db => statuses.Contains(db.StatusCode))
                .Select(i => new FilterViewUnassigned(i as FilterView))
                .AsEnumerable<IFilterViewUnassigned>().ToList()
            );
        }

        public async Task<IEnumerable<IFilterViewUnassigned>> GetUnassignedByTags(IEnumerable<string> roles, IEnumerable<string> tags)
        {
            var builders = Builders<IFilterView>.Filter;
            var filter = builders.AnyNin("Assignees.k", roles);
            var tenantId = TenantService.Current.Id;
            var selectQuery = Collection.Find(filter).ToEnumerable().AsQueryable().Where(i => i.TenantId == tenantId);

            return await Task.FromResult
            (
                selectQuery
                .Where(db => tags.Any(t => db.Tags.Contains(t)))
                .Select(i => new FilterViewUnassigned(i as FilterView))
                .AsEnumerable<IFilterViewUnassigned>().ToList()
            );
        }

        public async Task<IEnumerable<IFilterViewUnassigned>> GetAllUnassigned(IEnumerable<string> roles)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterNotInRoles = builders.AnyNin("Assignees.k", roles);
            var result = await Collection.Find(builders.And(filterByTenantId, filterNotInRoles)).ToListAsync();

            return result.Select(i => new FilterViewUnassigned(i)).AsEnumerable<IFilterViewUnassigned>().ToList();
        }

        public async Task<IEnumerable<IFilterView>> GetAllAssignedApplications(string userName)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.v", new List<string>() { userName });
            return await Collection.Find(builders.And(filterByTenantId, filterByUserName)).ToListAsync();
        }

        public async Task<IEnumerable<IFilterView>> GetAssignedByTags(string userName, string[] tags)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.v", new List<string>() { userName });
            var filterByStatus = builders.AnyIn(db => db.Tags, tags);
            return await Collection.Find(builders.And(filterByTenantId, filterByUserName, filterByStatus)).ToListAsync();
        }

        public async Task<IEnumerable<IFilterView>> GetAssignedByStatus(string userName, IList<string> statuses)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db=>db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.v", new List<string>(){ userName});
            var filterByStatus = builders.Where(db => statuses.Contains(db.StatusCode));
            return await  Collection.Find(builders.And(filterByTenantId,filterByUserName, filterByStatus)).ToListAsync();
        }


        public async Task<IEnumerable<IFilterView>> GetByTags(IList<string> tags)
        {
            return await Query.Where(db => tags.Any(t => db.Tags.Contains(t))).ToListAsync();
        }
    }
}
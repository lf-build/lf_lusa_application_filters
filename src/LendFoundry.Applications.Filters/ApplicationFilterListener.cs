﻿using LendFoundry.Application;
using LendFoundry.Application.Client;
using LendFoundry.AssignmentEngine;
using LendFoundry.AssignmentEngine.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.OfferEngine;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Tenant.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Applications.Filters
{
    public class ApplicationFilterListener : IApplicationFilterListener
    {
        public ApplicationFilterListener
        (
            IConfigurationServiceFactory<Configuration> apiConfigurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            IFilterViewRepositoryFactory repositoryFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IApplicationServiceClientFactory applicationServiceFactory,
            IOfferEngineServiceFactory offerEngineFactory,
            IStatusManagementServiceFactory statusManagementFactory,
            IDecisionEngineClientFactory decisionEngineFactory,
            IAssignmentServiceClientFactory assignmentFactory
        )
        {
            if (apiConfigurationFactory == null) throw new ArgumentException($"{nameof(apiConfigurationFactory)} is mandatory");
            if (tokenHandler == null) throw new ArgumentException($"{nameof(tokenHandler)} is mandatory");
            if (eventHubFactory == null) throw new ArgumentException($"{nameof(eventHubFactory)} is mandatory");
            if (repositoryFactory == null) throw new ArgumentException($"{nameof(repositoryFactory)} is mandatory");
            if (loggerFactory == null) throw new ArgumentException($"{nameof(loggerFactory)} is mandatory");
            if (tenantServiceFactory == null) throw new ArgumentException($"{nameof(tenantServiceFactory)} is mandatory");
            if (applicationServiceFactory == null) throw new ArgumentException($"{nameof(applicationServiceFactory)} is mandatory");
            if (offerEngineFactory == null) throw new ArgumentException($"{nameof(offerEngineFactory)} is mandatory");
            if (statusManagementFactory == null) throw new ArgumentException($"{nameof(statusManagementFactory)} is mandatory");
            if (decisionEngineFactory == null) throw new ArgumentException($"{nameof(decisionEngineFactory)} is mandatory");
            if (assignmentFactory == null) throw new ArgumentException($"{nameof(assignmentFactory)} is mandatory");

            EventHubFactory = eventHubFactory;
            ApiConfigurationFactory = apiConfigurationFactory;
            TokenHandler = tokenHandler;
            RepositoryFactory = repositoryFactory;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            ApplicationServiceFactory = applicationServiceFactory;
            OfferEngineFactory = offerEngineFactory;
            StatusManagementFactory = statusManagementFactory;
            DecisionEngineFactory = decisionEngineFactory;
            AssignmentFactory = assignmentFactory;
        }

        private IAssignmentServiceClientFactory AssignmentFactory { get; }

        private IDecisionEngineClientFactory DecisionEngineFactory { get; }

        private IStatusManagementServiceFactory StatusManagementFactory { get; }

        private IOfferEngineServiceFactory OfferEngineFactory { get; }

        private IApplicationServiceClientFactory ApplicationServiceFactory { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IConfigurationServiceFactory<Configuration> ApiConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private IFilterViewRepositoryFactory RepositoryFactory { get; }

        private ILoggerFactory LoggerFactory { get; }

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info($"Eventhub listener started");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var hub = EventHubFactory.Create(emptyReader);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    // Tenant token creation
                    logger.Info($"Processing tenant #{tenant.Id}");
                    var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName);
                    var reader = new StaticTokenReader(token.Value);

                    // Needed resources for this operation
                    var assignmentService = AssignmentFactory.Create(reader);
                    var statusManagementService = StatusManagementFactory.Create(reader);
                    var offerEngineService = OfferEngineFactory.Create(reader);
                    var repository = RepositoryFactory.Create(reader);
                    var applicationService = ApplicationServiceFactory.Create(reader);
                    var decisionEngine = DecisionEngineFactory.Create(reader);
                    var configuration = ApiConfigurationFactory.Create(reader).Get();
                    if (configuration == null)
                        throw new ArgumentException("Api configuration cannot be found, please check");

                    // Attach all configured events to be listen
                    configuration
                        .Events
                        .ToList()
                        .ForEach(eventConfig =>
                        {
                            hub.On(eventConfig.Name, AddView(eventConfig, repository, logger, applicationService, offerEngineService, statusManagementService, decisionEngine, configuration, assignmentService));
                            logger.Info($"Event #{eventConfig.Name} attached");
                        });
                });
                hub.StartAsync();
            }
            catch (WebSocketSharp.WebSocketException ex)
            {
                logger.Error("Error while listening eventhub", ex);
                Start();
            }
        }

        private static Action<EventInfo> AddView
        (
            EventMapping eventConfiguration,
            IFilterViewRepository repository,
            ILogger logger,
            IApplicationService applicationService,
            IOfferEngineService offerEngineService,
            IEntityStatusService statusManagementService,
            IDecisionEngineService decisionEngine,
            Configuration configuration,
            IAssignmentService assignmentService
        )
        {
            return @event =>
            {
                try
                {
                    var applicationNumber = eventConfiguration.ApplicationNumber.FormatWith(@event);
                    var application = applicationService.GetByApplicationNumber(applicationNumber);
                    if (application != null)
                    {
                        if (application.Applicants == null || !application.Applicants.Any())
                        {
                            logger.Warn($"No applicants found to application #{applicationNumber}");
                            return;
                        }

                        // snapshot creation
                        var filterView = new FilterView();

                        var offerDefinition = offerEngineService.GetByApplication(applicationNumber);
                        var applicationStatus = statusManagementService.GetStatusByEntity(Constants.Application, applicationNumber);

                        // application source values
                        var applicant = application.Applicants.FirstOrDefault(a => a.IsPrimary);
                        filterView.ApplicationId = application.ApplicationNumber;
                        filterView.Applicant = $"{applicant.FirstName} {applicant.MiddleName} {applicant.LastName}";
                        filterView.Submitted = application.SubmittedDate.Time;
                        filterView.SourceId = application.Source.Id;
                        filterView.SourceType = application.Source.Type.ToString().ToLower();
                        filterView.Ssn = applicant.Ssn;
                        
                        // gets all application assigness
                        var assignees = new Dictionary<string, string>();
                        var assignments = assignmentService.Get("Application", application.ApplicationNumber).Result;

                        if (assignments == null || assignments.Any() == false)
                            logger.Warn($"No assignments found for this snapshot using EntityId#{application.ApplicationNumber}");

                        if (assignments != null && assignments.Any())
                            assignees = assignments.ToDictionary(x => x.Role, x => x.Assignee);

                        filterView.Assignees = assignees;

                        if (application.ExpirationDate != null)
                            filterView.ExpirationDate = application.ExpirationDate.Time;

                        // offer source values
                        var offer = offerDefinition?.Offers.FirstOrDefault(o => o.Selected);
                        if (offer != null)
                        {
                            filterView.InterestRate = offer.Rate.Amount;
                            filterView.FundedDate = offerDefinition.Date;
                            filterView.Amount = offer.OfferAmount;
                            filterView.MaxApproval = offerDefinition.MaxAmount;
                            filterView.Payout = GetOfferPayout(offerDefinition);
                            filterView.PaymentAmount = offer.PaymentAmount;
                            logger.Info($"Offer found for this snapshot Amount #{filterView.Amount} MaxApproval #{filterView.MaxApproval} Payout #{filterView.Payout} PaymentAmount #{filterView.PaymentAmount} ");
                        }
                        else
                            logger.Info($"No offers found for this snapshot");

                        // status management source values
                        if (applicationStatus != null)
                        {
                            filterView.StatusCode = applicationStatus.Code;
                            filterView.StatusName = applicationStatus.Name;
                            filterView.StatusLabel = applicationStatus.Label;

                            filterView.ActiveOn = applicationStatus.ActiveOn;
                            
                            logger.Info($"Application status found for this snapshot #{filterView.StatusCode} #{filterView.StatusName}");
                        }
                        else
                            logger.Info($"No application status found for this snapshot");

                        filterView.Tags = GetTags(filterView.ApplicationId, decisionEngine, configuration, logger);
                        repository.AddOrUpdate(filterView);
                        logger.Info($"New snapshot added to application {applicationNumber}");
                    }
                    else
                        logger.Warn($"Application #{applicationNumber} could not be found while processing subscription #{@event.Name}");
                }
                catch (Exception ex) { logger.Error($"Unhadled exception while listening event {@event.Name}", ex); }
            };
        }

        private static IEnumerable<string> GetTags(string applicationId, IDecisionEngineService decisionEngine, Configuration configuration, ILogger logger)
        {
            // tag creation
            var appTags = new List<string>();
            configuration.Tags?.ToList().ForEach(x =>
            {
                var tagName = x.Key;
                var rule = x.Value;

                try
                {
                    var hasTag = decisionEngine.Execute<dynamic, bool>(rule.Name, rule.Version, new { appNumber = applicationId });
                    if (hasTag.Output)
                    {
                        appTags.Add(tagName);
                        logger.Info($"New tag #{tagName} added to application #{applicationId} based on rule #{rule.Name}");
                    }
                }
                catch (Exception ex)
                {
                    logger.Error($"Unhandled exception while processing tag #{tagName} to application #{applicationId} using the rule #{rule.Name}", ex);
                }
            });
            return appTags;
        }

        private static double GetOfferPayout(IOfferDefinition definition)
        {
            if (definition == null) return 0;

            //formula: Payout = Amount - SumOfFees
            var selectedOffer = definition.Offers.FirstOrDefault(o => o.Selected);
            var amount = selectedOffer.OfferAmount;
            var totalPercentFees = selectedOffer.Fees?.Where(f => f.Type == FeeType.Percent).Sum(f => f.Amount);
            var totalFixedFees = selectedOffer.Fees?.Where(f => f.Type == FeeType.Fixed).Sum(f => f.Amount);

            // remove all fixed fees from amount
            var payout = amount - totalFixedFees;

            // remove all percent fees from the residual value
            payout = payout - (amount * totalPercentFees / 100);
            return payout ?? 0;
        }
    }
}
﻿using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Applications.Filters
{
    public static class ApplicationFilterListenerExtensions
    {
        public static void UseApplicationFilterListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IApplicationFilterListener>().Start();
        }
    }
}
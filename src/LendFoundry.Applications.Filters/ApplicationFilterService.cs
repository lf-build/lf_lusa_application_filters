﻿using LendFoundry.Application;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Applications.Filters
{
    public class ApplicationFilterService : IApplicationFilterService
    {
        public ApplicationFilterService
        (
            IFilterViewRepository repository,
            ILogger logger,
            ITenantTime tenantTime,
            Configuration configuration,
            ITokenReader tokenReader,
            ITokenHandler tokenParser,
            IIdentityService identityService
        )
        {
            if (repository == null) throw new ArgumentException($"{nameof(repository)} is mandatory");
            if (logger == null) throw new ArgumentException($"{nameof(logger)} is mandatory");
            if (tenantTime == null) throw new ArgumentException($"{nameof(tenantTime)} is mandatory");
            if (configuration == null) throw new ArgumentException($"{nameof(configuration)} is mandatory");
            if (tokenReader == null) throw new ArgumentException($"{nameof(tokenReader)} is mandatory");
            if (tokenParser == null) throw new ArgumentException($"{nameof(tokenParser)} is mandatory");
            if (identityService == null) throw new ArgumentException($"{nameof(identityService)} is mandatory");

            Repository = repository;
            Logger = logger;
            TenantTime = tenantTime;
            Configuration = configuration;
            TokenReader = tokenReader;
            TokenParser = tokenParser;
            IdentityService = identityService;
        }

        private ITokenReader TokenReader { get; }

        private ITokenHandler TokenParser { get; }

        private Configuration Configuration { get; }

        private IFilterViewRepository Repository { get; }

        private ILogger Logger { get; }

        private ITenantTime TenantTime { get; }

        private IIdentityService IdentityService { get; }

        public IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId)
        {
            Validade(sourceType, sourceId);

            return Repository.GetAllBySource(sourceType, sourceId);
        }

        public IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validade(sourceType, sourceId);

            if (statuses == null || !statuses.Any())
                throw new ArgumentException($"{nameof(statuses)} is mandatory");

            return Repository.GetBySourceAndStatus(sourceType, sourceId, statuses);
        }

        public Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId)
        {
            try
            {
                Validade(sourceType, sourceId);

                var firstDayOfMonth = new DateTimeOffset(new DateTime(TenantTime.Now.Year, TenantTime.Now.Month, 1, 0, 0, 0));
                return Repository.GetTotalSubmittedForCurrentMonth(sourceType, sourceId, firstDayOfMonth);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetCountByMonth({sourceType}, {sourceId}) raised an error: {ex.Message}\n");
                throw;
            }
        }

        public Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId)
        {
            try
            {
                Validade(sourceType, sourceId);

                var firstDayOfYear = new DateTimeOffset(new DateTime(TenantTime.Now.Year, 1, 1, 0, 0, 0));
                return Repository.GetTotalSubmittedForCurrentYear(sourceType, sourceId, firstDayOfYear);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetCountByYear({sourceType}, {sourceId}) raised an error: {ex.Message}\n");
                throw;
            }
        }

        private void Validade(string sourceType, string sourceId)
        {
            if (string.IsNullOrWhiteSpace(sourceType))
                throw new InvalidArgumentException($"{nameof(sourceType)} is mandatory", nameof(sourceType));

            if (string.IsNullOrWhiteSpace(sourceId))
                throw new InvalidArgumentException($"{nameof(sourceId)} is mandatory", nameof(sourceId));

            ValidateEntityType(sourceType);
        }

        public Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name)
        {
            try
            {
                Validade(sourceType, sourceId);

                if (string.IsNullOrWhiteSpace(name))
                    throw new InvalidArgumentException($"{nameof(name)} is mandatory");

                return Repository.GetByName(sourceType, sourceId, name);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetByName({sourceType}, {sourceId}, {name}) raised an error : {ex.Message}\n");
                throw;
            }
        }

        public Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            try
            {
                Validade(sourceType, sourceId);

                if (statuses == null || !statuses.Any())
                    throw new ArgumentException($"{nameof(statuses)} is mandatory");

                return Repository.GetByStatus(sourceType, sourceId, statuses);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetByStatus({sourceType}, {sourceId}, {statuses}) raised an error : {ex.Message}\n");
                throw;
            }
        }

        public Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            try
            {
                Validade(sourceType, sourceId);

                if (statuses == null || !statuses.Any())
                    throw new ArgumentException($"{nameof(statuses)} is mandatory");

                return Repository.GetCountByStatus(sourceType, sourceId, statuses);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetCountByStatus({sourceType}, {sourceId}, {statuses}) raised an error : {ex.Message}\n");
                throw;
            }
        }

        public Task<IEnumerable<IFilterView>> GetByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            try
            {
                Validade(sourceType, sourceId);

                if (tags == null || !tags.Any())
                    throw new ArgumentException($"{nameof(tags)} is mandatory");

                return Repository.GetByTag(sourceType, sourceId, tags);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetByTag({sourceType}, {sourceId}, {tags}) raised an error : {ex.Message}\n");
                throw;
            }
        }

        public Task<int> GetCountByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            try
            {
                Validade(sourceType, sourceId);

                if (tags == null || !tags.Any())
                    throw new ArgumentException($"{nameof(tags)} is mandatory");

                return Repository.GetCountByTag(sourceType, sourceId, tags);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetCountByTag({sourceType}, {sourceId}, {tags}) raised an error : {ex.Message}\n");
                throw;
            }
        }

        public double GetTotalAmountApprovedBySource(string sourceType, string sourceId)
        {
            try
            {
                Validade(sourceType, sourceId);

                if (Configuration?.ApprovedStatuses == null || Configuration.ApprovedStatuses.Any() == false)
                    throw new ArgumentException($"{nameof(Configuration.ApprovedStatuses)} is mandatory in configuration");

                return
                    Repository.GetTotalAmountApprovedBySourceAndStatus(sourceType, sourceId, Configuration.ApprovedStatuses);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetTotalAmountApprovedBySource({sourceType}, {sourceId}) raised an error : {ex.Message}\n");
                throw;
            }
        }

        public Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            try
            {
                Validade(sourceType, sourceId);

                if (string.IsNullOrWhiteSpace(applicationNumber))
                    throw new InvalidArgumentException($"{nameof(applicationNumber)} is mandatory", nameof(applicationNumber));

                var result = Repository.GetByApplicationNumber(sourceType, sourceId, applicationNumber);
                if (result.Result == null)
                    throw new NotFoundException($"The snapshot could not be found with SourceType:{sourceType}, SourceId:{sourceId}, ApplicationNumber:{applicationNumber}");

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetByApplicationNumber({sourceType}, {sourceId}, {applicationNumber}) raised an error : {ex.Message}\n");
                throw;
            }
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications()
        {
            var todayDate = TenantTime.Today;
            if (Configuration?.StatusesNotExpired == null || !Configuration.StatusesNotExpired.Any() )
                throw new ArgumentException($"{nameof(Configuration.StatusesNotExpired)} is mandatory in configuration");
            return await Repository.GetAllExpiredApplications(todayDate, Configuration.StatusesNotExpired);
        }

        private void ValidateEntityType(string sourceType)
        {
            if (sourceType.Equals(SourceType.Undefined.ToString(), StringComparison.InvariantCultureIgnoreCase))
                throw new InvalidArgumentException($"Source {nameof(sourceType)} is not valid", EnumExtensions.GetEnumNames<SourceType>());
        }

        public async Task<IEnumerable<IFilterViewUnassigned>> GetUnassignedByStatus(IEnumerable<string> statuses)
        {
            if (statuses == null || !statuses.Any())
                throw new InvalidArgumentException($"{nameof(statuses)} is mandatory", nameof(statuses));

            var roles = await GetUserRoles();

            return await Repository.GetUnassignedByStatus(roles, statuses);
        }

        public async Task<IEnumerable<IFilterViewUnassigned>> GetUnassignedByTags(IEnumerable<string> tags)
        {
            if (tags == null || !tags.Any())
                throw new InvalidArgumentException($"{nameof(tags)} is mandatory", nameof(tags));

            var roles = await GetUserRoles();

            return await Repository.GetUnassignedByTags(roles, tags);
        }

        public async Task<IEnumerable<IFilterViewUnassigned>> GetAllUnassigned()
        {
            var roles = await GetUserRoles();

            return await Repository.GetAllUnassigned(roles);
        }

        private async Task<IEnumerable<string>> GetUserRoles()
        {
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            var roles = await IdentityService.GetUserRoles(userName);
            if (roles == null || roles.Any() == false)
                throw new NotFoundException("Could not be found the roles from current user");

            return roles;
        }

        public Task<IEnumerable<IFilterView>> GetAllAssignedApplications()
        {            
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            return Repository.GetAllAssignedApplications(userName);
        }

        public Task<IEnumerable<IFilterView>> GetAll()
        {
            return Repository.GetAll();
        }

        public Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses)
        {
          
            if (statuses==null || !statuses.Any())
                throw new ArgumentException($"{nameof(statuses)} is mandatory");
            var statusList = statuses as IList<string> ?? statuses.ToList();
            return Repository.GetByStatus(statusList);
        }

        public Task<IEnumerable<IFilterView>> GetByTags(IEnumerable<string> tags)
        {
          
            if (tags==null || !tags.Any())
                throw new ArgumentException($"{nameof(tags)} is mandatory");
            var tagList = tags as IList<string> ?? tags.ToList();
            return Repository.GetByTags(tagList);
        }

        public Task<IEnumerable<IFilterView>> GetAssignedByStatus(IEnumerable<string> statuses)
        {
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");
          
            if (statuses == null || !statuses.Any())
                throw new InvalidArgumentException($"{nameof(statuses)} is mandatory", nameof(statuses));
            var statusesList = statuses as IList<string> ?? statuses.ToList();
            return Repository.GetAssignedByStatus(userName,statusesList);
        }

        public Task<IEnumerable<IFilterView>> GetAssignedByTags(IEnumerable<string> tags)
        {
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            var tagsList = tags as string[] ?? tags.ToArray();
            if (tagsList == null || !tagsList.Any())
                throw new InvalidArgumentException($"{nameof(tags)} is mandatory", nameof(tags));

            return Repository.GetAssignedByTags(userName,tagsList);
        }

        private string GetTokenUserName()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            return token.Subject;
        }

        public Task<int> GetTotalSubmittedForCurrentMonthAndStatus(string sourceType, string sourceId)
        {
            try
            {
                Validade(sourceType, sourceId);

                var firstDayOfMonth = new DateTimeOffset(new DateTime(TenantTime.Now.Year, TenantTime.Now.Month, 1, 0, 0, 0));
                return Repository.GetTotalSubmittedForCurrentMonthAndStatus(sourceType, sourceId, firstDayOfMonth, Configuration.SubmittedStatuses);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetCountByMonth({sourceType}, {sourceId}) raised an error: {ex.Message}\n");
                throw;
            }
        }

        public Task<int> GetTotalSubmittedForCurrentYearAndStatus(string sourceType, string sourceId)
        {
            try
            {
                Validade(sourceType, sourceId);

                var firstDayOfYear = new DateTimeOffset(new DateTime(TenantTime.Now.Year, 1, 1, 0, 0, 0));
                return Repository.GetTotalSubmittedForCurrentYearAndStatus(sourceType, sourceId, firstDayOfYear, Configuration.SubmittedStatuses);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetCountByYear({sourceType}, {sourceId}) raised an error: {ex.Message}\n");
                throw;
            }
        }

      
    }
}
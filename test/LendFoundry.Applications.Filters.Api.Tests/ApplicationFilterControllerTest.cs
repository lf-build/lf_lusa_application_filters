﻿using LendFoundry.Applications.Filters.Api.Controllers;
using LendFoundry.Applications.Filters.Common.Tests.Fakes;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using Microsoft.AspNet.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Applications.Filters.Tests
{
    public partial class ApplicationFilterControllerTests
    {
        private ApiController ApplicationFilterController { get; }

        private Configuration FakeConfiguration { get; } = Mock.Of<Configuration>();

        private Mock<ITenantTime> TenantTime { get; } = new Mock<ITenantTime>();

        public ApplicationFilterControllerTests()
        {
            TenantTime.Setup(x => x.TimeZone).Returns(TimeZoneInfo.Utc);
            TenantTime.Setup(x => x.Now).Returns(DateTime.Now);
            TenantTime.Setup(x => x.Today).Returns(DateTime.Now);

            var repository = new FakeRepository(TenantTime.Object);

            repository.Add(new FilterView
            {
                Id = "1",
                ApplicationId = "LCUSA-0000009",
                Applicant = "CARLETTA G DCZZH",
                Amount = 0,
                MaxApproval = 0,
                PaymentAmount = 0,
                Payout = 0,
                SourceId = "f2534e3605f548a6bde6b9084a4f57df",
                SourceType = "Broker",
                StatusCode = "100.01",
                StatusName = "New",
                Submitted = new DateTime(2010, 01, 01),
                TenantId = "TenantId1"
            });
            repository.Add(new FilterView
            {
                Id = "2",
                ApplicationId = "LCUSA-0000002",
                Applicant = "CARLETTA G DCZZH",
                Amount = 0,
                MaxApproval = 0,
                PaymentAmount = 0,
                Payout = 0,
                SourceId = "df534e3605f548a6bde6b9084a4f57df",
                SourceType = "Merchant",
                StatusCode = "100.01",
                StatusName = "New",
                Submitted = new DateTime(2010, 01, 01),
                TenantId = "TenantId1",
                Tags = new List<string> { "Tag1", "Tag2" }
            });
            repository.Add(new FilterView
            {
                Id = "3",
                ApplicationId = "LCUSA-0000006",
                Applicant = "CARLETTA G DCZZH",
                Amount = 0,
                MaxApproval = 0,
                PaymentAmount = 0,
                Payout = 0,
                SourceId = "a2534e3605f548a6bde6b9084a4f57df",
                SourceType = "Merchant",
                StatusCode = "100.01",
                StatusName = "New",
                Submitted = new DateTime(2010, 01, 01),
                TenantId = "TenantId1"
            });
            repository.Add(new FilterView
            {
                Id = "4",
                ApplicationId = "LCUSA-0000010",
                Applicant = "CARLETTA G DCZZH",
                Amount = 0,
                MaxApproval = 0,
                PaymentAmount = 0,
                Payout = 0,
                SourceId = "f2534e3605f548a6bde6b9084a4f57df",
                SourceType = "Broker",
                StatusCode = "100.02",
                StatusName = "Step",
                Submitted = new DateTime(2010, 01, 01),
                TenantId = "TenantId1"
            });

            var Service = new ApplicationFilterService
                (
                    repository, 
                    Mock.Of<ILogger>(), 
                    TenantTime.Object, 
                    FakeConfiguration, 
                    Mock.Of<ITokenReader>(), 
                    Mock.Of<ITokenHandler>(),
                    Mock.Of<IIdentityService>()
                );

            ApplicationFilterController = new ApiController(Service, Mock.Of<ILogger>());
        }

        [Fact]
        public void GetAllBySource_WithNullSourceType_ThrowsArgumentExceptionResult()
        {
            var response = (ErrorResult)ApplicationFilterController.GetAllBySource(null, "df534e3605f548a6bde6b9084a4f57df");
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public void GetAllBySource_WithNullSourceId_ThrowsArgumentExceptionResult()
        {
            var response = (ErrorResult)ApplicationFilterController.GetAllBySource("Merchant", null);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public void GetAllBySource_WithValidSourceTypeAndSourceId_ReturnsObjectResult()
        {
            var response = (HttpOkObjectResult)ApplicationFilterController.GetAllBySource("Merchant", "df534e3605f548a6bde6b9084a4f57df");

            IEnumerable<IFilterView> resultData = response.Value as IEnumerable<IFilterView>;
            Assert.Equal(response.StatusCode, 200);
            Assert.NotNull(resultData);
            Assert.NotEmpty(resultData);

            var firstResultData = resultData.FirstOrDefault();
            Assert.Equal(firstResultData.Id, "2");
            Assert.Equal(firstResultData.ApplicationId, "LCUSA-0000002");
            Assert.Equal(firstResultData.Applicant, "CARLETTA G DCZZH");
            Assert.Equal(firstResultData.Amount, 0);
            Assert.Equal(firstResultData.MaxApproval, 0);
            Assert.Equal(firstResultData.PaymentAmount, 0);
            Assert.Equal(firstResultData.Payout, 0);
            Assert.Equal(firstResultData.SourceId, "df534e3605f548a6bde6b9084a4f57df");
            Assert.Equal(firstResultData.SourceType, "Merchant");
            Assert.Equal(firstResultData.StatusCode, "100.01");
            Assert.Equal(firstResultData.StatusName, "New");
            Assert.Equal(firstResultData.Submitted, new DateTime(2010, 01, 01));
            Assert.Equal(firstResultData.TenantId, "TenantId1");
        }

        [Fact]
        public async Task GetTotalSubmittedForCurrentMonth_WithNullSourceId_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetTotalSubmittedForCurrentMonth("Merchant", null);
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetTotalSubmittedForCurrentMonth_WithNullSourceType_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetTotalSubmittedForCurrentMonth(null, "df534e3605f548a6bde6b9084a4f57df");
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetTotalSubmittedForCurrentMonth_WithValidSourceTypeAndSourceId_ReturnsObjectResult()
        {

            var response = await ApplicationFilterController.GetTotalSubmittedForCurrentMonth("Merchant", "df534e3605f548a6bde6b9084a4f57df");
            Assert.NotNull(response);
            var result = (HttpOkObjectResult)response;
            Assert.Equal(result.StatusCode, 200);
            Assert.Equal(result.Value, 0);

        }

        [Fact]
        public async Task GetTotalSubmittedForCurrentYear_WithNullSourceId_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetTotalSubmittedForCurrentYear("Merchant", null);
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetTotalSubmittedForCurrentYear_WithNullSourceType_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetTotalSubmittedForCurrentYear(null, "df534e3605f548a6bde6b9084a4f57df");
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetTotalSubmittedForCurrentYear_WithValidSourceTypeAndSourceId_ReturnsObjectResult()
        {
            var response = await ApplicationFilterController.GetTotalSubmittedForCurrentYear("Merchant", "df534e3605f548a6bde6b9084a4f57df");
            Assert.NotNull(response);
            var result = (HttpOkObjectResult)response;
            Assert.Equal(result.StatusCode, 200);
            Assert.Equal(result.Value, 0);

        }

        [Fact]
        public async Task GetByStatus_WithNullSourceId_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetByStatus("Merchant", null, "100.01");
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetByStatus_WithNullSourceType_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetByStatus(null, "df534e3605f548a6bde6b9084a4f57df", "100.01");
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetByStatus_WithNullStatuses_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetByStatus("Merchant", "df534e3605f548a6bde6b9084a4f57df", null);
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetByStatus_WithValidSourceTypeSourceIdAndStatuses_ReturnsObjectResult()
        {
            var response = await ApplicationFilterController.GetByStatus("Merchant", "df534e3605f548a6bde6b9084a4f57df", "100.01");
            Assert.NotNull(response);
            var result = (HttpOkObjectResult)response;
            Assert.Equal(result.StatusCode, 200);
            var firstResultData = (result.Value as IEnumerable<IFilterView>).FirstOrDefault();
            Assert.Equal(firstResultData.Id, "2");
            Assert.Equal(firstResultData.ApplicationId, "LCUSA-0000002");
            Assert.Equal(firstResultData.Applicant, "CARLETTA G DCZZH");
            Assert.Equal(firstResultData.Amount, 0);
            Assert.Equal(firstResultData.MaxApproval, 0);
            Assert.Equal(firstResultData.PaymentAmount, 0);
            Assert.Equal(firstResultData.Payout, 0);
            Assert.Equal(firstResultData.SourceId, "df534e3605f548a6bde6b9084a4f57df");
            Assert.Equal(firstResultData.SourceType, "Merchant");
            Assert.Equal(firstResultData.StatusCode, "100.01");
            Assert.Equal(firstResultData.StatusName, "New");
            Assert.Equal(firstResultData.Submitted, new DateTime(2010, 01, 01));
            Assert.Equal(firstResultData.TenantId, "TenantId1");

        }

        [Fact]
        public async Task GetCountByStatus_WithNullSourceId_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetCountByStatus("Merchant", null, "100.01");
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetCountByStatus_WithNullSourceType_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetCountByStatus(null, "df534e3605f548a6bde6b9084a4f57df", "100.01");
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetCountByStatus_WithNullStatuses_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetCountByStatus("Merchant", "df534e3605f548a6bde6b9084a4f57df", null);
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetCountByStatus_WithValidSourceTypeSourceIdAndStatuses_ReturnsObjectResult()
        {
            var response = await ApplicationFilterController.GetCountByStatus("Merchant", "df534e3605f548a6bde6b9084a4f57df", "100.01");
            Assert.NotNull(response);
            var result = (HttpOkObjectResult)response;
            Assert.Equal(result.StatusCode, 200);

            Assert.Equal(result.Value, 1);


        }

        [Fact]
        public async Task GetByTag_WithNullSourceId_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetByTag("Merchant", null, "Tag1");
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetByTag_WithNullSourceType_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetByTag(null, "df534e3605f548a6bde6b9084a4f57df", "Tag1");
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetByTag_WithNullTags_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetByTag("Merchant", "df534e3605f548a6bde6b9084a4f57df", null);
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetByTag_WithValidSourceTypeSourceIdAndTags_ReturnsObjectResult()
        {
            var response = await ApplicationFilterController.GetByTag("Merchant", "df534e3605f548a6bde6b9084a4f57df", "Tag1");
            Assert.NotNull(response);
            var result = (HttpOkObjectResult)response;
            Assert.Equal(result.StatusCode, 200);

            var firstResultData = (result.Value as IEnumerable<IFilterView>).FirstOrDefault();
            Assert.Equal(firstResultData.Id, "2");
            Assert.Equal(firstResultData.ApplicationId, "LCUSA-0000002");
            Assert.Equal(firstResultData.Applicant, "CARLETTA G DCZZH");
            Assert.Equal(firstResultData.Amount, 0);
            Assert.Equal(firstResultData.MaxApproval, 0);
            Assert.Equal(firstResultData.PaymentAmount, 0);
            Assert.Equal(firstResultData.Payout, 0);
            Assert.Equal(firstResultData.SourceId, "df534e3605f548a6bde6b9084a4f57df");
            Assert.Equal(firstResultData.SourceType, "Merchant");
            Assert.Equal(firstResultData.StatusCode, "100.01");
            Assert.Equal(firstResultData.StatusName, "New");
            Assert.Equal(firstResultData.Submitted, new DateTime(2010, 01, 01));
            Assert.Equal(firstResultData.TenantId, "TenantId1");
            Assert.NotNull(firstResultData.Tags);
            Assert.Equal(firstResultData.Tags.Count(), 2);

        }

        [Fact]
        public async Task GetCountByTag_WithNullSourceId_ThrowsArgumentExceptionResult()

        {
            var response = await ApplicationFilterController.GetCountByTag("Merchant", null, "Tag1");
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetCountByTag_WithNullSourceType_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetCountByTag(null, "df534e3605f548a6bde6b9084a4f57df", "Tag1");
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetCountByTag_WithNullTags_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetCountByTag("Merchant", "df534e3605f548a6bde6b9084a4f57df", null);
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetCountByTag_WithValidSourceTypeSourceIdAndTags_ReturnsObjectResult()
        {
            var response = await ApplicationFilterController.GetCountByTag("Merchant", "df534e3605f548a6bde6b9084a4f57df", "Tag1/Tag2");
            Assert.NotNull(response);
            var result = (HttpOkObjectResult)response;
            Assert.Equal(result.StatusCode, 200);

            Assert.Equal(result.Value, 2);


        }

        [Fact]
        public async Task GetByName_WithNullSourceId_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetByName("Merchant", null, "CARLETTA G DCZZH");
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetByName_WithNullSourceType_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetByName(null, "df534e3605f548a6bde6b9084a4f57df", "CARLETTA G DCZZH");
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetByName_WithNullName_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetByName("Merchant", "df534e3605f548a6bde6b9084a4f57df", null);
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetByName_WithValidSourceTypeSourceIdAndName_ReturnsObjectResult()
        {
            var response = await ApplicationFilterController.GetByName("Merchant", "df534e3605f548a6bde6b9084a4f57df", "CARLETTA G DCZZH");
            Assert.NotNull(response);
            var result = (HttpOkObjectResult)response;
            Assert.Equal(result.StatusCode, 200);

            var firstResultData = (result.Value as IEnumerable<IFilterView>).FirstOrDefault();
            Assert.Equal(firstResultData.Id, "2");
            Assert.Equal(firstResultData.ApplicationId, "LCUSA-0000002");
            Assert.Equal(firstResultData.Applicant, "CARLETTA G DCZZH");
            Assert.Equal(firstResultData.Amount, 0);
            Assert.Equal(firstResultData.MaxApproval, 0);
            Assert.Equal(firstResultData.PaymentAmount, 0);
            Assert.Equal(firstResultData.Payout, 0);
            Assert.Equal(firstResultData.SourceId, "df534e3605f548a6bde6b9084a4f57df");
            Assert.Equal(firstResultData.SourceType, "Merchant");
            Assert.Equal(firstResultData.StatusCode, "100.01");
            Assert.Equal(firstResultData.StatusName, "New");
            Assert.Equal(firstResultData.Submitted, new DateTime(2010, 01, 01));
            Assert.Equal(firstResultData.TenantId, "TenantId1");
            Assert.NotNull(firstResultData.Tags);
            Assert.Equal(firstResultData.Tags.Count(), 2);

        }

        [Fact]
        public void GetTotalAmountApprovedBySource_WithNullSourceId_ThrowsArgumentExceptionResult()
        {
            var response = ApplicationFilterController.GetTotalAmountApprovedBySource("Merchant", null);
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public void GetTotalAmountApprovedBySource_WithNullSourceType_ThrowsArgumentExceptionResult()
        {
            var response = ApplicationFilterController.GetTotalAmountApprovedBySource(null, "df534e3605f548a6bde6b9084a4f57df");
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public void GetTotalAmountApprovedBySource_WithNullApprovedConfigurationStatuses_ThrowsArgumentExceptionResult()
        {
            var response = ApplicationFilterController.GetTotalAmountApprovedBySource("Merchant", "df534e3605f548a6bde6b9084a4f57df");
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);


        }

        [Fact]
        public void GetTotalAmountApprovedBySource_WithValidSourceTypeAndSourceId_ReturnsObjectResult()
        {
            FakeConfiguration.ApprovedStatuses = new string[] { "100.01" };
            var response = ApplicationFilterController.GetTotalAmountApprovedBySource("Merchant", "df534e3605f548a6bde6b9084a4f57df");
            Assert.NotNull(response);
            var result = (HttpOkObjectResult)response;
            Assert.Equal(result.StatusCode, 200);


            Assert.Equal(result.Value, (double)0);


        }

        [Fact]
        public async Task GetByApplicationNumber_WithNullSourceId_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetByApplicationNumber("Merchant", null, "LCUSA-0000002");
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetByApplicationNumber_WithNullSourceType_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetByApplicationNumber(null, "df534e3605f548a6bde6b9084a4f57df", "LCUSA-0000002");
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetByApplicationNumber_WithNullApplicationNumber_ThrowsArgumentExceptionResult()
        {
            var response = await ApplicationFilterController.GetByApplicationNumber("Merchant", "df534e3605f548a6bde6b9084a4f57df", null);
            Assert.NotNull(response);
            var result = (ErrorResult)response;
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async Task GetByApplicationNumber_WithValidSourceTypeSourceIdAndApplicationNumber_ReturnsObjectResult()
        {
            var response = await ApplicationFilterController.GetByApplicationNumber("Merchant", "df534e3605f548a6bde6b9084a4f57df", "LCUSA-0000002");
            Assert.NotNull(response);
            var result = (HttpOkObjectResult)response;
            Assert.Equal(result.StatusCode, 200);

            var firstResultData = (result.Value as IFilterView);
            Assert.Equal(firstResultData.Id, "2");
            Assert.Equal(firstResultData.ApplicationId, "LCUSA-0000002");
            Assert.Equal(firstResultData.Applicant, "CARLETTA G DCZZH");
            Assert.Equal(firstResultData.Amount, 0);
            Assert.Equal(firstResultData.MaxApproval, 0);
            Assert.Equal(firstResultData.PaymentAmount, 0);
            Assert.Equal(firstResultData.Payout, 0);
            Assert.Equal(firstResultData.SourceId, "df534e3605f548a6bde6b9084a4f57df");
            Assert.Equal(firstResultData.SourceType, "Merchant");
            Assert.Equal(firstResultData.StatusCode, "100.01");
            Assert.Equal(firstResultData.StatusName, "New");
            Assert.Equal(firstResultData.Submitted, new DateTime(2010, 01, 01));
            Assert.Equal(firstResultData.TenantId, "TenantId1");
            Assert.NotNull(firstResultData.Tags);
            Assert.Equal(firstResultData.Tags.Count(), 2);

        }

        [Fact]
        public async Task GetAllExpiredApplications_ReturnsObjectResult()
        {
            FakeConfiguration.StatusesNotExpired = new string[] { "400.03", "500.02", "500.03", "500.05", "500.06", "600.01", "600.02", "600.03", "600.04" };
            var response = await ApplicationFilterController.GetAllExpiredApplications();
            Assert.NotNull(response);
            var result = (HttpOkObjectResult)response;
            Assert.Equal(result.StatusCode, 200);

            var firstResultData = (result.Value as IEnumerable<IFilterView>).FirstOrDefault();
            Assert.Equal(firstResultData.Id, "1");
            Assert.Equal(firstResultData.ApplicationId, "LCUSA-0000009");
            Assert.Equal(firstResultData.Applicant, "CARLETTA G DCZZH");
            Assert.Equal(firstResultData.Amount, 0);
            Assert.Equal(firstResultData.MaxApproval, 0);
            Assert.Equal(firstResultData.PaymentAmount, 0);
            Assert.Equal(firstResultData.Payout, 0);
            Assert.Equal(firstResultData.SourceId, "f2534e3605f548a6bde6b9084a4f57df");
            Assert.Equal(firstResultData.SourceType, "Broker");
            Assert.Equal(firstResultData.StatusCode, "100.01");
            Assert.Equal(firstResultData.StatusName, "New");
            Assert.Equal(firstResultData.Submitted, new DateTime(2010, 01, 01));
            Assert.Equal(firstResultData.ExpirationDate, new DateTime(2010, 01, 31));
            Assert.Equal(firstResultData.TenantId, "TenantId1");

        }
    }
}
﻿using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using Moq;
using System;
using Xunit;

namespace LendFoundry.Applications.Filters.Api.Tests
{
    public class StartupTests
    {
        [Fact]
        public void ConfigureServices()
        {
            var startup = new Startup();
            var fakeServiceCollection = new ServiceCollection();
            startup.ConfigureServices(fakeServiceCollection);

            Assert.NotNull(fakeServiceCollection);
            Assert.NotEmpty(fakeServiceCollection);
        }

        [Fact]
        public void Configure()
        {
            Assert.Throws<NullReferenceException>(() => 
            {
                var startup = new Startup();
                var app = new Mock<IApplicationBuilder>();
                var env = new Mock<IHostingEnvironment>();

                env.Setup(x => x.EnvironmentName)
                    .Returns("Development");

                var currentPath = Environment.CurrentDirectory;
                var webRootFileProvider = new Microsoft.AspNet.FileProviders.PhysicalFileProvider(currentPath);

                env.Setup(x => x.WebRootFileProvider)
                    .Returns(webRootFileProvider);

                env.Setup(x => x.WebRootPath)
                    .Returns(currentPath);

                startup.Configure(app.Object, env.Object);
            });
        }
    }
}
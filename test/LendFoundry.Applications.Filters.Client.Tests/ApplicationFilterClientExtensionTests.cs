﻿//using LendFoundry.Applications.Filters.Common.Tests;
//using LendFoundry.Foundation.Logging;
//using LendFoundry.Security.Tokens;
//using Microsoft.Framework.DependencyInjection;
//using Moq;
//using System.Linq;
//using Xunit;

//namespace LendFoundry.Applications.Filters.Client.Tests
//{
//    public class ApplicationFilterClientExtensionTests : InMemoryObjects
//    {
//        public ApplicationFilterClientExtensionTests()
//        {
//            HttpContextAccessor = new Mock<Microsoft.AspNet.Http.IHttpContextAccessor>();
//            Logger = new Mock<ILogger>();
//            TokenReader = new Mock<ITokenReader>();

//            ServiceCollection = new ServiceCollection();

//            ServiceCollection.AddSingleton(x => HttpContextAccessor.Object);
//            ServiceCollection.AddSingleton(x => Logger.Object);
//            ServiceCollection.AddSingleton(x => TokenReader.Object);

//            ServiceProvider = ServiceCollection.BuildServiceProvider();
//        }

//        private System.IServiceProvider ServiceProvider { get; }

//        [Fact]
//        public void Should_Be_Able_To_AddScheduLog()
//        {
//            var serviceCollection = ApplicationFilterClientExtensions.AddApplicationFilters
//            (
//                ServiceCollection,
//                Endpoint,
//                Port
//            );

//            Assert.NotNull(serviceCollection);
//            Assert.NotEmpty(serviceCollection);
//            Assert.Equal(5, serviceCollection.Count);
//            Assert.Equal(1, serviceCollection.Count(x => x.ServiceType == typeof(IApplicationFilterClientFactory)));
//            Assert.Equal(1, serviceCollection.Count(x => x.ServiceType == typeof(IApplicationFilterService)));
//        }
//    }
//}
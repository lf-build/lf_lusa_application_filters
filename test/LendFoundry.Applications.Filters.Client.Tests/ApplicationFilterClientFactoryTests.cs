﻿//using LendFoundry.Applications.Filters.Common.Tests;
//using LendFoundry.Security.Tokens;
//using Microsoft.AspNet.Http;
//using Microsoft.Framework.DependencyInjection;
//using Moq;
//using Xunit;
//using ILogger = LendFoundry.Foundation.Logging.ILogger;

//namespace LendFoundry.Applications.Filters.Client.Tests
//{
//    public class ApplicationFilterClientFactoryTests : InMemoryObjects
//    {
//        public ApplicationFilterClientFactoryTests()
//        {
//            HttpContextAccessor = new Mock<IHttpContextAccessor>();
//            Logger = new Mock<ILogger>();
//            TokenReader = new Mock<ITokenReader>();

//            ServiceCollection = new ServiceCollection();

//            ServiceCollection.AddSingleton(x => HttpContextAccessor.Object);
//            ServiceCollection.AddSingleton(x => Logger.Object);
//            ServiceCollection.AddSingleton(x => TokenReader.Object);

//            ServiceProvider = ServiceCollection.BuildServiceProvider();
//        }

//        private System.IServiceProvider ServiceProvider { get; }

//        [Fact]
//        public void Should_Be_Able_To_Create_ApplicationFilterService()
//        {
//            var factory = new ApplicationFilterClientFactory(ServiceProvider, Endpoint, Port);
//            var client = factory.Create(TokenReader.Object);

//            Assert.NotNull(client);
//            Assert.IsType<ApplicationFilterClient>(client);
//        }
//    }
//}
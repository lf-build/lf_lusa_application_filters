﻿//using LendFoundry.Applications.Filters.Common.Tests;
//using LendFoundry.Applications.Filters.Common.Tests.Fakes;
//using LendFoundry.Foundation.Services;
//using Moq;
//using RestSharp;
//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using Xunit;

//namespace LendFoundry.Applications.Filters.Client.Tests
//{
//    public class ApplicationFilterClientTests : InMemoryObjects
//    {
//        public ApplicationFilterClientTests()
//        {
//            ServiceClient = new Mock<IServiceClient>();
//            Client = new ApplicationFilterClient(ServiceClient.Object);

//            MockServiceClient = new Mock<IServiceClient>();
//            ApplicationFilterClient = new ApplicationFilterClient(MockServiceClient.Object);
//        }

//        private IRestRequest Request { get; set; }

//        private Mock<IServiceClient> ServiceClient { get; }

//        private IApplicationFilterService Client { get; set; }

//        private ApplicationFilterClient ApplicationFilterClient { get; }

//        private Mock<IServiceClient> MockServiceClient { get; }

//        private List<FilterView> DummyData { get; } = new List<FilterView>
//            {
//               new FilterView
//                {
//                  Id = "1",
//                  ApplicationId="LCUSA-0000009",
//                  Applicant = "CARLETTA G DCZZH",
//                  Amount = 0,
//                  MaxApproval=0,
//                  PaymentAmount=0,
//                  Payout=0,
//                  SourceId="f2534e3605f548a6bde6b9084a4f57df",
//                  SourceType="Broker",
//                  StatusCode="100.01",
//                  StatusName = "New",
//                  Submitted = new DateTime(2010,01,01),
//                  TenantId = "TenantId1",
//                  ExpirationDate = new DateTime(2010,01,01).AddDays(30),
//                  Tags = new string[] { "tag1" }
//                },
//                 new FilterView
//                {
//                  Id = "2",
//                  ApplicationId="LCUSA-0000002",
//                  Applicant = "CARLETTA G DCZZH",
//                  Amount = 0,
//                  MaxApproval=0,
//                  PaymentAmount=0,
//                  Payout=0,
//                  SourceId="df534e3605f548a6bde6b9084a4f57df",
//                  SourceType="Merchant",
//                  StatusCode="100.01",
//                  StatusName = "New",
//                  Submitted = new DateTime(2010,01,01),
//                  TenantId = "TenantId1",
//                   ExpirationDate = new DateTime(2010,01,01).AddDays(30),
//                    Tags = new string[] { "tag1" }
//                },
//                 new FilterView
//                {
//                  Id = "3",
//                  ApplicationId="LCUSA-0000006",
//                  Applicant = "CARLETTA G DCZZH",
//                  Amount = 0,
//                  MaxApproval=0,
//                  PaymentAmount=0,
//                  Payout=0,
//                  SourceId="a2534e3605f548a6bde6b9084a4f57df",
//                  SourceType="Merchant",
//                  StatusCode="100.01",
//                  StatusName = "New",
//                  Submitted = new DateTime(2010,01,01),
//                  TenantId = "TenantId1",
//                   ExpirationDate = new DateTime(2010,01,01).AddDays(30),
//                    Tags = new string[] { "tag1" }
//                },
//                 new FilterView
//                {
//                  Id = "4",
//                  ApplicationId="LCUSA-0000010",
//                  Applicant = "CARLETTA G DCZZH",
//                  Amount = 0,
//                  MaxApproval=0,
//                  PaymentAmount=0,
//                  Payout=0,
//                  SourceId="f2534e3605f548a6bde6b9084a4f57df",
//                  SourceType="Broker",
//                  StatusCode="100.02",
//                  StatusName = "Step",
//                  Submitted = new DateTime(2010,01,01),
//                  TenantId = "TenantId1",
//                   ExpirationDate = new DateTime(2010,01,01).AddDays(30),
//                    Tags = new string[] { "tag1" }
//                }
//            };

//        private ApplicationFilterClient GetClient(IServiceClient client = null)
//        {
//            IServiceClient inMemoryServiceClient = ServiceClient.Object;
//            if (client == null)
//                client = new FakeServiceClient();

//            return new ApplicationFilterClient(client);
//        }


//        [Fact]
//        public async void GetTotalSubmittedForCurrentMonthExecutionOk()
//        {
//            // arrange
//            var fakeServiceClient = new FakeServiceClient();
//            var fakeTotal = 10;
//            fakeServiceClient.InMemoryFake = fakeTotal;

//            // action
//            var result = await GetClient(fakeServiceClient).GetTotalSubmittedForCurrentMonth("SourceType", "SourceId");

//            // assert
//            Assert.NotNull(result);
//            Assert.Equal(10, result);
//        }

//        [Fact]
//        public void GetTotalSubmittedForCurrentMonthExecutionOkButIfNoData()
//        {
//            // arrange
//            var fakeServiceClient = new FakeServiceClient();
//            fakeServiceClient.InMemoryFake = null;

//            // action
//            var result = GetClient(fakeServiceClient).GetTotalSubmittedForCurrentMonth("SourceType", "SourceId");

//            // assert
//            Assert.NotNull(result);
//            Assert.Equal(0, result.Result);
//        }

//        [Fact]
//        public void GetTotalSubmittedForCurrentMonthWhenInvalidSourceType()
//        {
//            Assert.ThrowsAsync<InvalidArgumentException>(async () => 
//            {
//                await GetClient().GetTotalSubmittedForCurrentMonth(null, "SourceId");
//            });
//        }

//        [Fact]
//        public void GetTotalSubmittedForCurrentMonthWhenInvalidSourceId()
//        {
//            Assert.ThrowsAsync<InvalidArgumentException>(async () => 
//            {
//                await GetClient().GetTotalSubmittedForCurrentMonth("SourceType", string.Empty);
//            });
//        }


//        [Fact]
//        public async void GetTotalSubmittedForCurrentYearExecutionOk()
//        {
//            // arrange
//            var fakeServiceClient = new FakeServiceClient();
//            var fakeTotal = 10;
//            fakeServiceClient.InMemoryFake = fakeTotal;

//            // action
//            var result = await GetClient(fakeServiceClient).GetTotalSubmittedForCurrentYear("SourceType", "SourceId");

//            // assert
//            Assert.NotNull(result);
//            Assert.Equal(10, result);
//        }

//        [Fact]
//        public void GetTotalSubmittedForCurrentYearExecutionOkButIfNoData()
//        {
//            // arrange
//            var fakeServiceClient = new FakeServiceClient();
//            fakeServiceClient.InMemoryFake = null;

//            // action
//            var result = GetClient(fakeServiceClient).GetTotalSubmittedForCurrentYear("SourceType", "SourceId");

//            // assert
//            Assert.NotNull(result);
//            Assert.Equal(0, result.Result);
//        }

//        [Fact]
//        public void GetTotalSubmittedForCurrentYearWhenInvalidSourceType()
//        {
//            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
//            {
//                await GetClient().GetTotalSubmittedForCurrentYear(null, "SourceId");
//            });
//        }

//        [Fact]
//        public void GetTotalSubmittedForCurrentYearWhenInvalidSourceId()
//        {
//            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
//            {
//                await GetClient().GetTotalSubmittedForCurrentYear("SourceType", string.Empty);
//            });
//        }

//        [Fact]
//        public void GetByName()
//        {
//            var fakeServiceClient = new FakeServiceClient()
//            {
//                InMemoryFake = "test"
//            };

//            var result = GetClient(fakeServiceClient).GetByName("SourceType", "SourceId", "name");

//            Assert.NotNull(result);
//        }

//        [Fact]
//        public void GetByNameWhenInvalidSourceType()
//        {
//            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
//            {
//                await GetClient().GetByName(null, "SourceId", "name");
//            });
//        }

//        [Fact]
//        public void GetByNameWhenInvalidSourceId()
//        {
//            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
//            {
//                await GetClient().GetByName("SourceType", null, "name");
//            });
//        }

//        [Fact]
//        public void GetByNameWhenInvalidName()
//        {
//            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
//            {
//                await GetClient().GetByName("SourceType", "SourceId", null);
//            });
//        }


//        [Fact]
//        public async void GetByApplicationNumberExecutionOk()
//        {
//            // arrange
//            var fakeServiceClient = new FakeServiceClient();
//            fakeServiceClient.InMemoryFake = InMemoryFilterView;

//            // action
//            var result = await GetClient(fakeServiceClient).GetByApplicationNumber("SourceType", "SourceId", "ApplicationId1");

//            // assert
//            Assert.NotNull(result);
//            Assert.Equal("ApplicationId1", result.ApplicationId);
//        }

//        [Fact]
//        public void GetByApplicationNumberExecutionOkButIfNoData()
//        {
//            // arrange
//            var fakeServiceClient = new FakeServiceClient();
//            fakeServiceClient.InMemoryFake = InMemoryFilterView;

//            // action
//            var result = GetClient(fakeServiceClient).GetByApplicationNumber("SourceType", "SourceId", "ApplicationId");

//            // assert
//            Assert.NotNull(result);
//        }

//        [Fact]
//        public void GetByApplicationNumberWhenInvalidSourceType()
//        {
//            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
//            {
//                await GetClient().GetByApplicationNumber(null, "SourceId", "ApplicationNumber");
//            });
//        }

//        [Fact]
//        public void GetByApplicationNumberWhenInvalidSourceId()
//        {
//            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
//            {
//                await GetClient().GetByApplicationNumber("SourceType", string.Empty, "ApplicationNumber");
//            });
//        }

//        [Fact]
//        public void GetByApplicationNumberWhenInvalidApplicationNumber()
//        {
//            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
//            {
//                await GetClient().GetByApplicationNumber("SourceType", "SourceId", string.Empty);
//            });
//        }

//        [Fact]
//        public void GetAllBySource()
//        {
//            MockServiceClient.Setup(s => s.Execute<List<FilterView>>(It.IsAny<IRestRequest>()))
//                .Callback<IRestRequest>(r => Request = r)
//                .Returns(
//                    () =>
//                        Task.FromResult(DummyData).Result
//                        );

//            var result = ApplicationFilterClient.GetAllBySource("SourceType3", "SourceId3");
//            Assert.Equal("/{sourceType}/{sourceId}/all", Request.Resource);
//            Assert.Equal(Method.GET, Request.Method);
//            Assert.NotNull(result);
//        }

//        [Fact]
//        public void GetBySourceAndStatus()
//        {
//            MockServiceClient.Setup(s => s.Execute<List<FilterView>>(It.IsAny<IRestRequest>()))
//                .Callback<IRestRequest>(r => Request = r)
//                .Returns(
//                    () =>
//                        Task.FromResult(DummyData).Result
//                        );

//            var result = ApplicationFilterClient.GetBySourceAndStatus("SourceType3", "SourceId3", new[] { "R" });
//            Assert.Equal("/{sourceType}/{sourceId}/status/{statuses}", Request.Resource);
//            Assert.Equal(Method.GET, Request.Method);
//            Assert.NotNull(result);
//        }

//        [Fact]
//        public async Task GetAllExpiredApplications()
//        {

//            MockServiceClient.Setup(s => s.ExecuteAsync<List<FilterView>>(It.IsAny<IRestRequest>()))
//               .Callback<IRestRequest>(r => Request = r)
//               .Returns(Task.FromResult(DummyData));


//            var result = await ApplicationFilterClient.GetAllExpiredApplications();
//            Assert.Equal("/expired/all", Request.Resource);
//            Assert.Equal(Method.GET, Request.Method);
//            Assert.NotNull(result);
//        }

//        [Fact]
//        public async Task GetAssignedByStatusWhenExecutionOk()
//        {
//            MockServiceClient.Setup(s => s.ExecuteAsync<List<FilterView>>(It.IsAny<IRestRequest>()))
//                .Callback<IRestRequest>(r => Request = r)
//                .Returns(
//                    () =>
//                        Task.FromResult(DummyData)
//                        );



//            var result = await ApplicationFilterClient.GetAssignedByStatus(new string[] { "100.01" });
//            Assert.Equal("/assigned/status/{statuses}", Request.Resource);
//            Assert.Equal(Method.GET, Request.Method);
//            Assert.NotNull(result);
//            Assert.NotEmpty(result);
           
//        }

//        [Fact]
//        public async void GetAssignedByTagsWhenExecutionOk()
//        {
//            MockServiceClient.Setup(s => s.ExecuteAsync<List<FilterView>>(It.IsAny<IRestRequest>()))
//                 .Callback<IRestRequest>(r => Request = r)
//                 .Returns(
//                     () =>
//                         Task.FromResult(DummyData)
//                         );



//            var result = await ApplicationFilterClient.GetAssignedByTags(new string[] { "tag1" });
//            Assert.Equal("/assigned/tags/{tags}", Request.Resource);
//            Assert.Equal(Method.GET, Request.Method);
//            Assert.NotNull(result);
//            Assert.NotEmpty(result);

//        }
//        [Fact]
//        public async void GetAllWhenExecutionOk()
//        {
//            MockServiceClient.Setup(s => s.ExecuteAsync<List<FilterView>>(It.IsAny<IRestRequest>()))
//                 .Callback<IRestRequest>(r => Request = r)
//                 .Returns(
//                     () =>
//                         Task.FromResult(DummyData)
//                         );



//            var result = await ApplicationFilterClient.GetAll();
//            Assert.Equal("/all", Request.Resource);
//            Assert.Equal(Method.GET, Request.Method);
//            Assert.NotNull(result);
//            Assert.NotEmpty(result);

//        }
//        [Fact]
//        public async void GetByStatusWhenExecutionOk()
//        {
//            MockServiceClient.Setup(s => s.ExecuteAsync<List<FilterView>>(It.IsAny<IRestRequest>()))
//                 .Callback<IRestRequest>(r => Request = r)
//                 .Returns(
//                     () =>
//                         Task.FromResult(DummyData)
//                         );



//            var result = await ApplicationFilterClient.GetByStatus(new string[] { "100.01"});
//            Assert.Equal("/status/{statuses}", Request.Resource);
//            Assert.Equal(Method.GET, Request.Method);
//            Assert.NotNull(result);
//            Assert.NotEmpty(result);

//        }
//        [Fact]
//        public async void GetByTagsWhenExecutionOk()
//        {
//            MockServiceClient.Setup(s => s.ExecuteAsync<List<FilterView>>(It.IsAny<IRestRequest>()))
//                 .Callback<IRestRequest>(r => Request = r)
//                 .Returns(
//                     () =>
//                         Task.FromResult(DummyData)
//                         );



//            var result = await ApplicationFilterClient.GetByTags(new string[] { "tag1" });
//            Assert.Equal("/tags/{tags}", Request.Resource);
//            Assert.Equal(Method.GET, Request.Method);
//            Assert.NotNull(result);
//            Assert.NotEmpty(result);

//        }
//    }
//}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.EventHub.Client;
using LendFoundry.Security.Tokens;
using Moq;

namespace LendFoundry.Applications.Filters.Common.Tests.Fakes
{
    public class FakeEventHub : IEventHubClient
    {
        Action<EventInfo> Handler = default(Action<EventInfo>);

        public EventInfo EventInfo { get; set; }

        public void On(string eventName, Action<EventInfo> handler)
        {
            Handler = handler;
        }

        public Task<bool> Publish<T>(T @event)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Publish<T>(string eventName, T @event)
        {
            throw new NotImplementedException();
        }

        public void PublishBatchWithInterval<T>(List<T> events, int interval = 100)
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            Handler.Invoke(EventInfo);
        }

        public void StartAsync()
        {
            Handler.Invoke(EventInfo);
        }
    }

    public class FakeEventHubFactory : IEventHubClientFactory
    {
        public FakeEventHub EventHub { get; set; }

        public IEventHubClient Create(ITokenReader tokenReader)
        {
            return EventHub;
        }
    }
}
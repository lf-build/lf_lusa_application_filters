﻿using LendFoundry.EventHub.Client;
using Moq;

namespace LendFoundry.Applications.Filters.Common.Tests.Fakes
{
    public class FakeEventHubClient
    {
        public Mock<IEventHubClient> Event { get; }

        public FakeEventHubClient()
        {
            Event = new Mock<IEventHubClient>();
        }
    }
}
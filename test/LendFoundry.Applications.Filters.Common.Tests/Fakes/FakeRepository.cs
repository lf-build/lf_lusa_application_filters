﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LendFoundry.Applications.Filters.Common.Tests.Fakes
{
    public class FakeRepository : IFilterViewRepository
    {
        public FakeRepository() { }

        public FakeRepository(ITenantTime tenantTime)
        {

            if (tenantTime != null)
                TenantTime = tenantTime;
        }

        protected ITenantTime TenantTime { get; set; }

        public List<IFilterView> InMemoryData { get; set; } = new List<IFilterView>();

        public void Add(IFilterView item)
        {
            // item.Id = Guid.NewGuid().ToString("N");
            item.ExpirationDate = item.Submitted.AddDays(30);
            InMemoryData.Add(item);
        }

        public void AddOrUpdate(IFilterView view)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IFilterView>> All(Expression<Func<IFilterView, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            return Task.Run(() => InMemoryData
                .AsQueryable()
                .Where(query)
                .AsEnumerable());
        }

        public int Count(Expression<Func<IFilterView, bool>> query)
        {
            return InMemoryData
                .AsQueryable()
                .Count(query);
        }

        public Task<IFilterView> Get(string id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IFilterView>> GetAssignedByTags(string userName, string[] tags)
        {
            return Task.FromResult<IEnumerable<IFilterView>>
            (
                OrderIt(from db in InMemoryData.Where(x => x.Assignees.Values.Contains(userName)).ToList()
                        from dbTags in db.Tags
                        where (from tag in tags select tag).Contains(dbTags)
                        select db).Distinct()
            );
        }

        public Task<IEnumerable<IFilterView>> GetAll()
        {
            return Task.FromResult<IEnumerable<IFilterView>>(OrderIt(InMemoryData));
        }



        public IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId)
        {
            var result = InMemoryData.Where(x => x.SourceType == sourceType && x.SourceId == sourceId);
            if (result.Count() > 0)
                result = OrderByStatusCodeThenByDate(result);
            return result.ToList();
        }

        public IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            var result = InMemoryData.Where(x => x.SourceType == sourceType && x.SourceId == sourceId && statuses.Contains(x.StatusCode));
            if (result.Count() > 0)
                result = OrderByStatusCodeThenByDate(result);
            return result.ToList();
        }


        public Task<IEnumerable<IFilterView>> GetByTags(IList<string> tags)
        {
            return Task.FromResult<IEnumerable<IFilterView>>
           (
               OrderIt(from db in InMemoryData.ToList()
                       from dbTags in db.Tags
                       where (from tag in tags select tag).Contains(dbTags)
                       select db).Distinct()
           );
        }

        public Task<IEnumerable<IFilterView>> GetAssignedByStatus(string userName, IList<string> statuses)
        {

            return Task.FromResult<IEnumerable<IFilterView>>
            (
                OrderIt(InMemoryData.Where(x => statuses.Contains(x.StatusCode) && x.Assignees.Values.Contains(userName)))
            );
        }

        public Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId, DateTimeOffset currentMonth)
        {
            return Task.FromResult(InMemoryData.Count
                (
                    app => app.SourceType.ToLower() == sourceType.ToLower() &&
                    app.SourceId == sourceId &&
                    app.Submitted >= currentMonth
                ));
        }

        public Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId, DateTimeOffset currentYear)
        {
            return Task.FromResult(InMemoryData.Count
                (
                    app => app.SourceType.ToLower() == sourceType.ToLower() &&
                    app.SourceId == sourceId &&
                    app.Submitted >= currentYear
                ));
        }

        public void Remove(IFilterView item)
        {
            throw new NotImplementedException();
        }

        public void Update(IFilterView item)
        {
            throw new NotImplementedException();
        }

        private IQueryable<IFilterView> OrderByStatusCodeThenByDate(IEnumerable<IFilterView> filterView)
        {
            return filterView.OrderBy(x => x.StatusCode).ThenBy(x => x.Submitted).AsQueryable();
        }

        public Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name)
        {
            return Task.Run(() =>
            {
                return InMemoryData.Where(a => a.SourceType.ToLower() == sourceType.ToLower()
                                            && a.SourceId == sourceId
                                            && a.Applicant.ToLower().Contains(name.ToLower()));
            });
        }

        public Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {

            return Task.Run(() =>
            {
                return InMemoryData.Where(a =>
                    a.SourceType.ToLower() == sourceType.ToLower() &&
                    a.SourceId == sourceId &&
                    statuses.Contains(a.StatusCode));
            });
        }

        public Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return Task.Run(() =>
            {
                return InMemoryData.Where(a =>
                    a.SourceType.ToLower() == sourceType.ToLower() &&
                    a.SourceId == sourceId &&
                    statuses.Contains(a.StatusCode)).Count();
            });
        }

        public Task<IEnumerable<IFilterView>> GetByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            return Task.Run(() =>
            {
                var db = InMemoryData.Where(a =>
                   a.SourceType.ToLower() == sourceType.ToLower() &&
                   a.SourceId == sourceId);
                return from a in db
                       from b in a.Tags
                       from c in tags
                       where b == c
                       select a;
            });
        }

        public Task<int> GetCountByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            return Task.Run(() =>
            {
                var db = InMemoryData.Where(a =>
                   a.SourceType.ToLower() == sourceType.ToLower() &&
                   a.SourceId == sourceId);
                return (from a in db
                        from b in a.Tags
                        from c in tags
                        where b == c
                        select a).Count();
            });
        }

        public double GetTotalAmountApprovedBySourceAndStatus(string sourceType, string sourceId, string[] statuses)
        {
            return
                InMemoryData.Where(
                        x => x.SourceType == sourceType && x.SourceId == sourceId && statuses.Contains(x.StatusCode))
                    .Sum(x => x.Amount);
        }

        public Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            return Task.Run(() =>
            {
                return InMemoryData.Where(a =>
                        a.SourceType.ToLower() == sourceType.ToLower() &&
                        a.SourceId == sourceId &&
                        a.ApplicationId == applicationNumber).FirstOrDefault();
            });
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications(DateTimeOffset todayDate, string[] verifiedStatuses)
        {

            todayDate = TenantTime.Today;
            return await Task.Run(() => InMemoryData.Where(p => p.ExpirationDate < todayDate && !verifiedStatuses.Contains(p.StatusCode)).ToList());

        }

        public Task<IEnumerable<IFilterViewUnassigned>> GetUnassignedByStatus(IEnumerable<string> roles, IEnumerable<string> statuses)
        {
            return Task.Run(() =>
            {
                var filteredByStatus = InMemoryData.Where(x => statuses.Contains(x.StatusCode));

                var unassignes = filteredByStatus
                .Where(i => i.Assignees != null && i.Assignees.Any())
                .Where(i => i.Assignees.Keys.Any(a => roles.Contains(a)) == false)
                .Select(i => new FilterViewUnassigned(i))
                .AsEnumerable<IFilterViewUnassigned>().ToList();

                var nullOrEmptyAssignees = filteredByStatus
                .Where(i => i.Assignees == null || i.Assignees.Any() == false)
                .Select(i => new FilterViewUnassigned(i));

                unassignes.AddRange(nullOrEmptyAssignees);

                return unassignes.AsEnumerable();
            });
        }

        public Task<IEnumerable<IFilterViewUnassigned>> GetUnassignedByTags(IEnumerable<string> roles, IEnumerable<string> tags)
        {
            return Task.Run(() =>
            {
                var db = InMemoryData.Where(x => x.Tags != null);
                var filteredByTags = (
                                        from a in db
                                        from b in a.Tags
                                        from c in tags
                                        where b == c
                                        select a
                                     ).Distinct();

                var unassignes = filteredByTags
                .Where(i => i.Assignees != null && i.Assignees.Any())
                .Where(i => i.Assignees.Keys.Any(a => roles.Contains(a)) == false)
                .Select(i => new FilterViewUnassigned(i))
                .AsEnumerable<IFilterViewUnassigned>().ToList();

                var nullOrEmptyAssignees = filteredByTags
                .Where(i => i.Assignees == null || i.Assignees.Any() == false)
                .Select(i => new FilterViewUnassigned(i));

                unassignes.AddRange(nullOrEmptyAssignees);

                return unassignes.AsEnumerable();
            });
        }

        public Task<IEnumerable<IFilterViewUnassigned>> GetAllUnassigned(IEnumerable<string> roles)
        {
            return Task.Run(() =>
            {
                var unassignes = InMemoryData
                .Where(i => i.Assignees != null && i.Assignees.Any())
                .Where(i => i.Assignees.Keys.Any(a => roles.Contains(a)) == false)
                .Select(i => new FilterViewUnassigned(i))
                .AsEnumerable<IFilterViewUnassigned>().ToList();

                var nullOrEmptyAssignees = InMemoryData
                .Where(i => i.Assignees == null || i.Assignees.Any() == false)
                .Select(i => new FilterViewUnassigned(i));

                unassignes.AddRange(nullOrEmptyAssignees);

                return unassignes.AsEnumerable();
            });
        }

        public Task<IEnumerable<IFilterView>> GetAllAssignedApplications(string user)
        {
            return Task.Run(() =>
            {
                return InMemoryData.Where(x => x.Assignees.Values.Contains(user));
            });
        }



        private IQueryable<IFilterView> OrderIt(IEnumerable<IFilterView> filterView)
        {
            return filterView.OrderBy(x => x.StatusCode)
                             .ThenBy(x => x.Submitted).AsQueryable();
        }

        public Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses)
        {
            var result = InMemoryData.Where(x => statuses.Contains(x.StatusCode));
            if (result.Count() > 0)
                result = OrderByStatusCodeThenByDate(result);

            return Task.FromResult(result);
        }

        public Task<int> GetTotalSubmittedForCurrentMonthAndStatus(string sourceType, string sourceId, DateTimeOffset currentMonth, IEnumerable<string> validStatuses)
        {
            return Task.FromResult(InMemoryData.Count
             (
                 app => app.SourceType.ToLower() == sourceType.ToLower() &&
                 app.SourceId == sourceId &&
                 app.Submitted >= currentMonth
             ));
        }

        public Task<int> GetTotalSubmittedForCurrentYearAndStatus(string sourceType, string sourceId, DateTimeOffset currentYear, IEnumerable<string> validStatuses)
        {
            return Task.FromResult(InMemoryData.Count
             (
                 app => app.SourceType.ToLower() == sourceType.ToLower() &&
                 app.SourceId == sourceId &&
                 app.Submitted >= currentYear &&
                 validStatuses.Contains(app.StatusCode)
             ));
        }
    }
}
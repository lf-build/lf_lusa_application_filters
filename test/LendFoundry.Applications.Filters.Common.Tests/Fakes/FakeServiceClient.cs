﻿using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using System;
using System.Threading.Tasks;

namespace LendFoundry.Applications.Filters.Common.Tests.Fakes
{
    public class FakeServiceClient : IServiceClient
    {
        public FakeServiceClient()
        {
            Client = Mock.Of<IServiceClient>();
        }

        public FakeServiceClient(IServiceClient client) { Client = client; }

        private IServiceClient Client { get; }

        public object InMemoryFake { get; set; }

        public bool Execute(IRestRequest request)
        {
            throw new NotImplementedException();
        }

        public object Execute(IRestRequest request, Type type)
        {
            throw new NotImplementedException();
        }

        public T Execute<T>(IRestRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ExecuteAsync(IRestRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<object> ExecuteAsync(IRestRequest request, Type type)
        {
            return Task.Run(() => InMemoryFake);
        }

        public Task<T> ExecuteAsync<T>(IRestRequest request)
        {
            return Task.Run<T>(() => (T)InMemoryFake);
            //return new Task<T>(() => (T) InMemoryFake);
        }

        public IRestResponse ExecuteRequest(IRestRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<IRestResponse> ExecuteRequestAsync(IRestRequest request)
        {
            throw new NotImplementedException();
        }


    }
}
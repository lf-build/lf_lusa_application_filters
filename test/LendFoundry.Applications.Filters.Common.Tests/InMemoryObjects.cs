﻿using LendFoundry.Applications.Filters.Api.Controllers;
using LendFoundry.Applications.Filters.Common.Tests.Fakes;
using LendFoundry.AssignmentEngine;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Abstractions;
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Applications.Filters.Common.Tests
{
    public class InMemoryObjects
    {
        protected string TenantId { get; } = "my-tenant";

        protected Mock<IApplicationFilterService> Service { get; } = new Mock<IApplicationFilterService>();

        protected Mock<ITenantService> TenantService { get; set; }

        protected string Endpoint { get; } = "10.1.1.99";

        protected int Port { get; } = 5000;

        protected string BearerToken { get; } = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM";

        protected Mock<Microsoft.AspNet.Http.IHttpContextAccessor> HttpContextAccessor { get; set; } = new Mock<Microsoft.AspNet.Http.IHttpContextAccessor>();

        protected Mock<ILogger> Logger { get; set; } = new Mock<ILogger>();

        protected Mock<ITokenReader> TokenReader { get; set; } = new Mock<ITokenReader>();

        protected Mock<ITokenHandler> TokenHandler { get; set; } = new Mock<ITokenHandler>();

        protected IServiceCollection ServiceCollection { get; set; } = new ServiceCollection();

        protected IServiceProvider InServiceProvider { get; set; }

        protected Mock<ITenantTime> TenantTime { get; set; } = new Mock<ITenantTime>();

        protected Mock<IEventHubClient> EventHubClient { get; set; } = new Mock<IEventHubClient>();

        protected Mock<IIdentityService> IdentityService { get; } = new Mock<IIdentityService>();

        protected Configuration FakeConfiguration { get; } = Mock.Of<Configuration>();

        protected IApplicationFilterService GetService
        (
            IFilterViewRepository repository = null,
            IEventHubClient eventhub = null,
            ILogger logger = null,
            ITenantTime tenantTime = null,
            Configuration configuration = null,
            ITokenReader tokenReader = null,
            ITokenHandler tokenHandler = null,
            IIdentityService identityService = null
        )
        {
            IFilterViewRepository inTimeRepository = repository;
            if (inTimeRepository == null)
                inTimeRepository = new FakeRepository();

            IEventHubClient inTimeEventhub = eventhub;
            if (inTimeEventhub == null)
                inTimeEventhub = new FakeEventHub();

            ILogger inTimeLogger = logger;
            if (inTimeLogger == null)
                inTimeLogger = Mock.Of<ILogger>();

            ITenantTime inTimeTenant = tenantTime;
            if (inTimeTenant == null)
                inTimeTenant = Mock.Of<ITenantTime>();

            Configuration inTimeConfiguration = configuration;
            if (inTimeConfiguration == null)
                inTimeConfiguration = Mock.Of<Configuration>();

            ITokenReader inTimeTokenReader = tokenReader;
            if (inTimeTokenReader == null)
                inTimeTokenReader = Mock.Of<ITokenReader>();

            ITokenHandler inTimeTokenHandler = tokenHandler;
            if (inTimeTokenHandler == null)
                inTimeTokenHandler = Mock.Of<ITokenHandler>();

            IIdentityService inIdentityService = identityService;
            if (inIdentityService == null)
                inIdentityService = Mock.Of<IIdentityService>();

            return new ApplicationFilterService
                (
                    inTimeRepository,
                    inTimeLogger,
                    inTimeTenant,
                    inTimeConfiguration,
                    inTimeTokenReader,
                    inTimeTokenHandler,
                    inIdentityService
                );
        }

        protected ApiController GetController
        (
            IApplicationFilterService service = null, 
            ILogger logger = null
        )
        {
            IApplicationFilterService inService = service;
            if (inService == null)
                inService = GetService();

            ILogger inLogger = logger;
            if (inLogger == null)
                inLogger = Mock.Of<ILogger>();

            return new ApiController(inService, inLogger);
        }

        protected IFilterView GetData()
        {
            return new FilterView
            {
                ApplicationId = "application001",
                Applicant = "applicant001",
                Amount = 40000,
                MaxApproval = 30000,
                PaymentAmount = 10000,
                Payout = 10000,
                SourceId = "source001",
                SourceType = "merchant",
                StatusCode = "R",
                StatusName = "Rejected"
            };
        }

        protected IFilterView InMemoryFilterView => new FilterView
        {
            Id = "1",
            ApplicationId = "ApplicationId1",
            Applicant = "Applicant1",
            Amount = 12000,
            MaxApproval = 10000,
            PaymentAmount = 5000,
            Payout = 5000,
            SourceId = "SourceId1",
            SourceType = "SourceType1",
            StatusCode = "A",
            StatusName = "Approved",
            Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
            TenantId = TenantId,
            ExpirationDate = new DateTime(DateTime.Now.Year - 2, DateTime.Now.Month, DateTime.Now.Day),
            Tags = new string[] { "tag1" }
        };

        protected IAssignment InMemoryAssignment => new Assignment
        {
            Id = "1",
            AssignedBy = "Eduardo",
            AssignedOn = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
            Assignee = "Eduardo",
            EntityId = "EntityId",
            EntityType = "Application",
            EscalationId = "EscalationId",
            Role = "Credit Specialist",
            TenantId = TenantId
        };

        protected IEnumerable<IAssignment> GetAssignments()
        {
            return new[] { InMemoryAssignment, InMemoryAssignment, InMemoryAssignment }
            .Select((item, idx) =>
            {
                idx++;
                item.Id = $"Assignment-00{idx}";

                if (idx == 2)
                    item.Role = "QA";

                if (idx == 3)
                {
                    item.Role = "VP of Sales";
                    item.Assignee = "Marcio";
                }

                return item;
            }).ToList();
        }

        protected IToken InMemoryToken => new Token
        {
            Subject = "Subject"
        };
    }

    public interface IServiceProvider : System.IServiceProvider
    {
        T GetService<T>() where T : class;

        T GetRequiredService<T>() where T : class;
    }
}
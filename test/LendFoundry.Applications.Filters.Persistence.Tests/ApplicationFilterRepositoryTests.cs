﻿using LendFoundry.AssignmentEngine;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Testing.Mongo;
using LendFoundry.Tenant.Client;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LendFoundry.Applications.Filters.Persistence.Tests
{
    public class ApplicationFilterRepositoryTests
    {
        [MongoFact]
        public void GetTotalSubmittedForCurrentMonth()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = GetRepository(config);
                var sourceType = GetData.SourceType;
                var sourceId = GetData.SourceId;
                var entries = new[]
                {
                    GetData, GetData, GetData,
                    GetData, GetData, GetData
                };
                entries.Select((entry, i) =>
                {
                    i++;
                    entry.Id = $"ApplicationId{i}";
                    if (i > 3)
                        entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(1).Day);
                    else
                        entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.AddDays(-31).Month, DateTime.Now.AddDays(-10).Day);

                    return entry;
                }).ToList().ForEach((filterView) =>
                {
                    repo.Add(filterView);
                });

                var firstDayOfMonth = new DateTimeOffset(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0));
                // action
                var result = repo.GetTotalSubmittedForCurrentMonth(sourceType, sourceId, firstDayOfMonth);

                // assert
                Assert.NotNull(result);
                Assert.Equal(3, result.Result);
            });
        }

        [MongoFact]
        public void GetTotalSubmittedForCurrentYear()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = GetRepository(config);
                var sourceType = GetData.SourceType;
                var sourceId = GetData.SourceId;
                var entries = new[]
                {
                    GetData, GetData, GetData,
                    GetData, GetData, GetData,
                    GetData, GetData, GetData,
                    GetData
                };
                entries.Select((entry, i) =>
                {
                    i++;
                    entry.Id = $"ApplicationId{i}";
                    if (i > 5)
                        entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(1).Day);
                    else
                        entry.Submitted = new DateTime(DateTime.Now.AddYears(-1).Year, DateTime.Now.Month, DateTime.Now.Day);

                    return entry;
                }).ToList().ForEach((filterView) =>
                {
                    repo.Add(filterView);
                });

                var firstDayOfYear = new DateTimeOffset(new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0));
                // action
                var result = repo.GetTotalSubmittedForCurrentMonth(sourceType, sourceId, firstDayOfYear);

                // assert
                Assert.NotNull(result);
                Assert.Equal(5, result.Result);
            });
        }

        [MongoFact]
        public void GetByStatus()
        {
            MongoTest.Run(config =>
            {
                var repo = GetRepository(config);
                var sourceType = GetData.SourceType;
                var sourceId = GetData.SourceId;
                var inputList = new[] { GetData, GetData, GetData, GetData, GetData };
                inputList.Select((item, idx) =>
                {
                    idx++;
                    item.Id = $"ApplicationId1{idx}";
                    item.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    item.StatusCode = "100.01";
                    return item;
                }).ToList().ForEach(item => repo.Add(item));

                inputList.Select((item, idx) =>
                {
                    idx++;
                    item.Id = $"ApplicationId2{idx}";
                    item.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    item.StatusCode = "100.02";
                    return item;
                }).ToList().ForEach(item => repo.Add(item));

                var data = repo.GetByStatus(sourceType, sourceId, new[] { "100.01" }).Result;
                Assert.NotNull(data);
                Assert.NotEmpty(data);
                Assert.True(data.ToList().TrueForAll(x => x.StatusCode == "100.01"));
            });
        }

        [MongoFact]
        public void GetCountByStatus()
        {
            MongoTest.Run(config =>
            {
                var repo = GetRepository(config);
                var sourceType = GetData.SourceType;
                var sourceId = GetData.SourceId;
                var inputList = new[] { GetData, GetData, GetData, GetData, GetData };
                inputList.Select((item, idx) =>
                {
                    idx++;
                    item.Id = $"ApplicationId1{idx}";
                    item.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    item.StatusCode = "100.01";
                    return item;
                }).ToList().ForEach(item => repo.Add(item));

                inputList.Select((item, idx) =>
                {
                    idx++;
                    item.Id = $"ApplicationId2{idx}";
                    item.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    item.StatusCode = "100.02";
                    return item;
                }).ToList().ForEach(item => repo.Add(item));

                var data = repo.GetCountByStatus(sourceType, sourceId, new[] { "100.01" }).Result;
                Assert.NotNull(data);
                Assert.Equal(inputList.Count(), data);
            });
        }

        [MongoFact]
        public void GetByTag()
        {
            MongoTest.Run(config =>
            {
                var repo = GetRepository(config);
                var sourceType = GetData.SourceType;
                var sourceId = GetData.SourceId;
                var inputList = new[] { GetData, GetData, GetData, GetData, GetData };
                inputList.Take(3).Select((item, idx) =>
                {
                    idx++;
                    item.Id = $"ApplicationId1{idx}";
                    item.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    item.Tags = new[] { "tag-a", "tag-b" };
                    return item;
                }).ToList().ForEach(item => repo.Add(item));

                inputList.Select((item, idx) =>
                {
                    idx++;
                    item.Id = $"ApplicationId2{idx}";
                    item.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    item.Tags = new[] { "tag-c", "tag-d" };
                    return item;
                }).ToList().ForEach(item => repo.Add(item));

                var data = repo.GetByTag(sourceType, sourceId, new[] { "tag-a" }).Result;
                Assert.NotNull(data);
                Assert.NotEmpty(data);
                Assert.Equal(3, data.Count());
            });
        }

        [MongoFact]
        public void GetCountByTag()
        {
            MongoTest.Run(config =>
            {
                var repo = GetRepository(config);
                var sourceType = GetData.SourceType;
                var sourceId = GetData.SourceId;
                var inputList = new[] { GetData, GetData, GetData, GetData, GetData };
                inputList.Take(2).Select((item, idx) =>
                {
                    idx++;
                    item.Id = $"ApplicationId1{idx}";
                    item.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    item.Tags = new[] { "tag-a", "tag-b" };
                    return item;
                }).ToList().ForEach(item => repo.Add(item));

                inputList.Select((item, idx) =>
                {
                    idx++;
                    item.Id = $"ApplicationId2{idx}";
                    item.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    item.Tags = new[] { "tag-c", "tag-d" };
                    return item;
                }).ToList().ForEach(item => repo.Add(item));

                var data = repo.GetCountByTag(sourceType, sourceId, new[] { "tag-b" }).Result;
                Assert.NotNull(data);
                Assert.Equal(2, data);

                data = repo.GetCountByTag(sourceType, sourceId, new[] { "tag-x" }).Result;
                Assert.NotNull(data);
                Assert.Equal(0, data);
            });
        }

        [MongoFact]
        public void GetByName()
        {
            MongoTest.Run(config =>
            {
                var fakeRepository = GetRepository(config);
                var sourceType = GetData.SourceType;
                var sourceId = GetData.SourceId;

                var inputList = new[] { GetData, GetData, GetData, GetData, GetData };
                inputList.Take(1).Select((item, idx) =>
                {
                    idx++;
                    item.Id = $"ApplicationId1{idx}";
                    item.Applicant = "name";
                    return item;
                }).ToList().ForEach(item => fakeRepository.Add(item));

                inputList.Select((item, idx) =>
                {
                    idx++;
                    item.Id = $"ApplicationId2{idx}";
                    item.Applicant = "test";
                    return item;
                }).ToList().ForEach(item => fakeRepository.Add(item));

                var data = fakeRepository.GetByName(sourceType, sourceId, "name").Result;
                Assert.NotNull(data);
                Assert.NotNull(data.Single());
            });
        }

        [MongoFact]
        public void GetByApplicationNumber()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = GetRepository(config);
                var sourceType = GetData.SourceType;
                var sourceId = GetData.SourceId;
                var entries = new[]
                {
                    GetData, GetData, GetData,
                    GetData, GetData, GetData,
                    GetData, GetData, GetData,
                    GetData
                };
                entries.Select((entry, i) =>
                {
                    i++;
                    entry.Id = $"ApplicationId{i}";
                    entry.ApplicationId = $"ApplicationId{i}";
                    entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(1).Day);

                    return entry;
                }).ToList().ForEach((filterView) =>
                {
                    repo.Add(filterView);
                });

                var firstDayOfYear = new DateTimeOffset(new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0));
                // action
                var result = repo.GetByApplicationNumber(sourceType, sourceId, "ApplicationId2");

                // assert
                Assert.NotNull(result);
                Assert.Equal("ApplicationId2", result.Result.ApplicationId);
            });
        }

        private string TenantId { get; } = "my-tenant";

        private IFilterViewRepository GetRepository(IMongoConfiguration config)
        {
            var tenantService = new Mock<ITenantService>();
            tenantService.Setup(s => s.Current).Returns(new TenantInfo { Id = TenantId });
            return new FilterViewRepository
            (
                configuration: config,
                tenantService: tenantService.Object
            );
        }

        private IFilterView GetData => new FilterView
        {
            Id = "1",
            ApplicationId = "ApplicationId1",
            Applicant = "Applicant1",
            Amount = 12000,
            MaxApproval = 10000,
            PaymentAmount = 5000,
            Payout = 5000,
            SourceId = "SourceId1",
            SourceType = "SourceType1",
            StatusCode = "A",
            StatusName = "Approved",
            Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
            TenantId = TenantId
        };

        protected IFilterView InMemoryFilterView => new FilterView
        {
            Id = "1",
            ApplicationId = "ApplicationId1",
            Applicant = "Applicant1",
            Amount = 12000,
            MaxApproval = 10000,
            PaymentAmount = 5000,
            Payout = 5000,
            SourceId = "SourceId1",
            SourceType = "SourceType1",
            StatusCode = "A",
            StatusName = "Approved",
            Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
            TenantId = TenantId,
            ExpirationDate = new DateTime(DateTime.Now.Year - 2, DateTime.Now.Month, DateTime.Now.Day),
            Tags = new string[] { "tag1" }
        };

        [MongoFact]
        public void GetAllUnassigned()
        {
            MongoTest.Run( async (config) =>
            {
                // arrange
                var repo = GetRepository(config);
                var filterViews = new[]
                {
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView
                };

                filterViews.Select((item, idx) =>
                {
                    idx++;
                    if (idx == 3)
                    {
                        return new FilterView
                        {
                            Id = $"ApplicationId2{idx}",
                            ApplicationId = $"ApplicationNumber00{idx}",
                            Applicant = $"Applicant00{idx}",
                            Ssn = "ABCD1234",
                            Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                            StatusCode = "100.02",
                            Amount = 12000,
                            MaxApproval = 10000,
                            PaymentAmount = 5000,
                            Payout = 5000,
                            SourceId = "SourceId1",
                            SourceType = "SourceType1",
                            Tags = new[] { "tag001", "tag002", "tag003" },
                            Assignees = new[] {
                                new Assignment
                                {
                                    Id = "1",
                                    AssignedBy = "Eduardo",
                                    AssignedOn = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                                    Assignee = "Eduardo",
                                    EntityId = "EntityId",
                                    EntityType = "Application",
                                    EscalationId = "EscalationId",
                                    Role = "QA",
                                    TenantId = TenantId
                                }
                            }.ToDictionary(x => x.Role, x => x.Assignee)
                        };
                    }

                    if (idx == 4)
                    {
                        return new FilterView
                        {
                            Id = $"ApplicationId2{idx}",
                            ApplicationId = $"ApplicationNumber00{idx}",
                            Applicant = $"Applicant00{idx}",
                            Ssn = "ABCD1234",
                            Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                            StatusCode = "100.02",
                            Amount = 12000,
                            MaxApproval = 10000,
                            PaymentAmount = 5000,
                            Payout = 5000,
                            SourceId = "SourceId1",
                            SourceType = "SourceType1",
                            Tags = new[] { "tag001", "tag002", "tag003" },
                            Assignees = null
                        };
                    }

                    if (idx == 5)
                    {
                        return new FilterView
                        {
                            Id = $"ApplicationId2{idx}",
                            ApplicationId = $"ApplicationNumber00{idx}",
                            Applicant = $"Applicant00{idx}",
                            Ssn = "ABCD1234",
                            Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                            StatusCode = "100.02",
                            Amount = 12000,
                            MaxApproval = 10000,
                            PaymentAmount = 5000,
                            Payout = 5000,
                            SourceId = "SourceId1",
                            SourceType = "SourceType1",
                            Tags = new[] { "tag001", "tag002", "tag003" },
                            Assignees = new Dictionary<string, string>()
                        };
                    }

                    return new FilterView
                    {
                        Id = $"ApplicationId2{idx}",
                        ApplicationId = $"ApplicationNumber00{idx}",
                        Applicant = $"Applicant00{idx}",
                        Ssn = "ABCD1234",
                        Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                        ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                        StatusCode = "100.02",
                        Amount = 12000,
                        MaxApproval = 10000,
                        PaymentAmount = 5000,
                        Payout = 5000,
                        SourceId = "SourceId1",
                        SourceType = "SourceType1",
                        Tags = new[] { "tag001", "tag002", "tag003" },
                        Assignees = new[] {
                            new Assignment
                            {
                                Id = "1",
                                AssignedBy = "Eduardo",
                                AssignedOn = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                                Assignee = "Eduardo",
                                EntityId = "EntityId",
                                EntityType = "Application",
                                EscalationId = "EscalationId",
                                Role = "Credit Specialist",
                                TenantId = TenantId
                            }
                        }.ToDictionary(x => x.Role, x => x.Assignee)
                    };
                }).ToList().ForEach(item => repo.Add(item));

                var roles = new[] { "QA", "VP of Sales" };
                
                // action
                var result = await repo.GetAllUnassigned(roles);
                var realResult = result.ToList();

                // assert
                Assert.NotEmpty(realResult);
                Assert.Equal(9, realResult.Count);
            });
        }

        [MongoFact]
        public void GetUnassignedByTags()
        {
            MongoTest.Run(async (config) =>
            {
                // arrange
                var repo = GetRepository(config);
                var filterViews = new[]
                {
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView
                };

                filterViews.Select((item, idx) =>
                {
                    idx++;
                    if (idx == 3)
                    {
                        return new FilterView
                        {
                            Id = $"ApplicationId2{idx}",
                            ApplicationId = $"ApplicationNumber00{idx}",
                            Applicant = $"Applicant00{idx}",
                            Ssn = "ABCD1234",
                            Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                            StatusCode = "100.02",
                            Amount = 12000,
                            MaxApproval = 10000,
                            PaymentAmount = 5000,
                            Payout = 5000,
                            SourceId = "SourceId1",
                            SourceType = "SourceType1",
                            Tags = new[] { "tag001", "tag002", "tag003" },
                            Assignees = new[] {
                                new Assignment
                                {
                                    Id = "1",
                                    AssignedBy = "Eduardo",
                                    AssignedOn = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                                    Assignee = "Eduardo",
                                    EntityId = "EntityId",
                                    EntityType = "Application",
                                    EscalationId = "EscalationId",
                                    Role = "QA",
                                    TenantId = TenantId
                                }
                            }.ToDictionary(x => x.Role, x => x.Assignee)
                        };
                    }

                    if (idx == 4)
                    {
                        return new FilterView
                        {
                            Id = $"ApplicationId2{idx}",
                            ApplicationId = $"ApplicationNumber00{idx}",
                            Applicant = $"Applicant00{idx}",
                            Ssn = "ABCD1234",
                            Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                            StatusCode = "100.02",
                            Amount = 12000,
                            MaxApproval = 10000,
                            PaymentAmount = 5000,
                            Payout = 5000,
                            SourceId = "SourceId1",
                            SourceType = "SourceType1",
                            Tags = new[] { "tag0011", "tag0022", "tag0033" },
                            Assignees = null
                        };
                    }

                    if (idx == 5)
                    {
                        return new FilterView
                        {
                            Id = $"ApplicationId2{idx}",
                            ApplicationId = $"ApplicationNumber00{idx}",
                            Applicant = $"Applicant00{idx}",
                            Ssn = "ABCD1234",
                            Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                            StatusCode = "100.02",
                            Amount = 12000,
                            MaxApproval = 10000,
                            PaymentAmount = 5000,
                            Payout = 5000,
                            SourceId = "SourceId1",
                            SourceType = "SourceType1",
                            Tags = new[] { "tag0011", "tag0022", "tag0033" },
                            Assignees = new Dictionary<string, string>()
                        };
                    }

                    return new FilterView
                    {
                        Id = $"ApplicationId2{idx}",
                        ApplicationId = $"ApplicationNumber00{idx}",
                        Applicant = $"Applicant00{idx}",
                        Ssn = "ABCD1234",
                        Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                        ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                        StatusCode = "100.02",
                        Amount = 12000,
                        MaxApproval = 10000,
                        PaymentAmount = 5000,
                        Payout = 5000,
                        SourceId = "SourceId1",
                        SourceType = "SourceType1",
                        Tags = new[] { "tag0011", "tag0022", "tag0033" },
                        Assignees = new[] {
                            new Assignment
                            {
                                Id = "1",
                                AssignedBy = "Eduardo",
                                AssignedOn = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                                Assignee = "Eduardo",
                                EntityId = "EntityId",
                                EntityType = "Application",
                                EscalationId = "EscalationId",
                                Role = "Credit Specialist",
                                TenantId = TenantId
                            }
                        }.ToDictionary(x => x.Role, x => x.Assignee)
                    };
                }).ToList().ForEach(item => repo.Add(item));

                var roles = new[] { "Credit Specialist", "VP of Sales" };
                var tags = new[] { "tag001", "tag003" };
                // action
                var result = await repo.GetUnassignedByTags(roles, tags);
                var realResult = result.ToList();

                // assert
                Assert.NotEmpty(realResult);
                Assert.Equal(1, realResult.Count);
            });
        }

        [MongoFact]
        public void GetUnassignedByStatus()
        {
            MongoTest.Run(async (config) =>
            {
                // arrange
                var repo = GetRepository(config);
                var filterViews = new[]
                {
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView
                };

                filterViews.Select((item, idx) =>
                {
                    idx++;
                    if (idx == 3)
                    {
                        return new FilterView
                        {
                            Id = $"ApplicationId2{idx}",
                            ApplicationId = $"ApplicationNumber00{idx}",
                            Applicant = $"Applicant00{idx}",
                            Ssn = "ABCD1234",
                            Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                            StatusCode = "100.02",
                            Amount = 12000,
                            MaxApproval = 10000,
                            PaymentAmount = 5000,
                            Payout = 5000,
                            SourceId = "SourceId1",
                            SourceType = "SourceType1",
                            Tags = new[] { "tag001", "tag002", "tag003" },
                            Assignees = new[] {
                                new Assignment
                                {
                                    Id = "1",
                                    AssignedBy = "Eduardo",
                                    AssignedOn = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                                    Assignee = "Eduardo",
                                    EntityId = "EntityId",
                                    EntityType = "Application",
                                    EscalationId = "EscalationId",
                                    Role = "QA",
                                    TenantId = TenantId
                                }
                            }.ToDictionary(x => x.Role, x => x.Assignee)
                        };
                    }

                    if (idx == 4)
                    {
                        return new FilterView
                        {
                            Id = $"ApplicationId2{idx}",
                            ApplicationId = $"ApplicationNumber00{idx}",
                            Applicant = $"Applicant00{idx}",
                            Ssn = "ABCD1234",
                            Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                            StatusCode = "100.03",
                            Amount = 12000,
                            MaxApproval = 10000,
                            PaymentAmount = 5000,
                            Payout = 5000,
                            SourceId = "SourceId1",
                            SourceType = "SourceType1",
                            Tags = new[] { "tag0011", "tag0022", "tag0033" },
                            Assignees = null
                        };
                    }

                    if (idx == 5)
                    {
                        return new FilterView
                        {
                            Id = $"ApplicationId2{idx}",
                            ApplicationId = $"ApplicationNumber00{idx}",
                            Applicant = $"Applicant00{idx}",
                            Ssn = "ABCD1234",
                            Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                            StatusCode = "100.01",
                            Amount = 12000,
                            MaxApproval = 10000,
                            PaymentAmount = 5000,
                            Payout = 5000,
                            SourceId = "SourceId1",
                            SourceType = "SourceType1",
                            Tags = new[] { "tag0011", "tag0022", "tag0033" },
                            Assignees = new Dictionary<string, string>()
                        };
                    }

                    return new FilterView
                    {
                        Id = $"ApplicationId2{idx}",
                        ApplicationId = $"ApplicationNumber00{idx}",
                        Applicant = $"Applicant00{idx}",
                        Ssn = "ABCD1234",
                        Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                        ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                        StatusCode = "100.04",
                        Amount = 12000,
                        MaxApproval = 10000,
                        PaymentAmount = 5000,
                        Payout = 5000,
                        SourceId = "SourceId1",
                        SourceType = "SourceType1",
                        Tags = new[] { "tag0011", "tag0022", "tag0033" },
                        Assignees = new[] {
                            new Assignment
                            {
                                Id = "1",
                                AssignedBy = "Eduardo",
                                AssignedOn = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                                Assignee = "Eduardo",
                                EntityId = "EntityId",
                                EntityType = "Application",
                                EscalationId = "EscalationId",
                                Role = "Credit Specialist",
                                TenantId = TenantId
                            }
                        }.ToDictionary(x => x.Role, x => x.Assignee)
                    };
                }).ToList().ForEach(item => repo.Add(item));

                var roles = new[] { "Credit Specialist", "VP of Sales" };
                var statuses = new[] { "100.02", "100.03" };
                // action
                var result = await repo.GetUnassignedByStatus(roles, statuses);
                var realResult = result.ToList();

                // assert
                Assert.NotEmpty(realResult);
                Assert.Equal(2, realResult.Count);
            });
        }
    }
}
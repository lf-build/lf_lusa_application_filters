﻿using LendFoundry.Application.Client;
using LendFoundry.Applications.Filters.Common.Tests;
using LendFoundry.AssignmentEngine.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.OfferEngine;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Moq;
using System;
using Xunit;

namespace LendFoundry.Applications.Filters.Tests
{
    public class ApplicationFilterListenerExtensionTests : InMemoryObjects
    {
        [Fact]
        public void UseApplicationFilterListener_InvalidOperation()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                var listener = new ApplicationFilterListener
                (
                    Mock.Of<IConfigurationServiceFactory<Configuration>>(),
                    Mock.Of<ITokenHandler>(),
                    Mock.Of<IEventHubClientFactory>(),
                    Mock.Of<IFilterViewRepositoryFactory>(),
                    Mock.Of<ILoggerFactory>(),
                    Mock.Of<ITenantServiceFactory>(),
                    Mock.Of<IApplicationServiceClientFactory>(),
                    Mock.Of<IOfferEngineServiceFactory>(),
                    Mock.Of<IStatusManagementServiceFactory>(),
                    Mock.Of<IDecisionEngineClientFactory>(),
                    Mock.Of<IAssignmentServiceClientFactory>()
                );

                var applicationBuilder = new Mock<IApplicationBuilder>();
                var serviceProvider = new Mock<Common.Tests.IServiceProvider>();

                serviceProvider.Setup(x => x.GetRequiredService<IApplicationFilterListener>())
                    .Returns(listener);

                applicationBuilder.Setup(x => x.ApplicationServices)
                    .Returns(serviceProvider.Object);

                ApplicationFilterListenerExtensions.UseApplicationFilterListener(applicationBuilder.Object);
            });
        }
    }
}
﻿using LendFoundry.Application;
using LendFoundry.Application.Client;
using LendFoundry.Applications.Filters.Common.Tests.Fakes;
using LendFoundry.AssignmentEngine.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.OfferEngine;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Tenant.Client;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LendFoundry.Applications.Filters.Tests
{
    public class ApplicationFilterListenerTest
    {
        [Fact]
        public void InitListenerWithNoConfiguration()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var config = Mock.Of<IConfigurationServiceFactory<Configuration>>();
                var token = Mock.Of<ITokenHandler>();
                var eventhub = Mock.Of<IEventHubClientFactory>();
                var repository = Mock.Of<IFilterViewRepositoryFactory>();
                var logger = Mock.Of<ILoggerFactory>();
                var tenant = Mock.Of<ITenantServiceFactory>();
                var application = Mock.Of<IApplicationServiceClientFactory>();
                var offer_engine = Mock.Of<IOfferEngineServiceFactory>();
                var status_manager = Mock.Of<IStatusManagementServiceFactory>();
                var decision_engine = Mock.Of<IDecisionEngineClientFactory>();
                var assignment = Mock.Of<IAssignmentServiceClientFactory>();
                new ApplicationFilterListener(null, token, eventhub, repository, logger, tenant, application, offer_engine, status_manager, decision_engine, assignment);
            });
        }

        [Fact]
        public void InitListenerWithNoToken()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var config = Mock.Of<IConfigurationServiceFactory<Configuration>>();
                var token = Mock.Of<ITokenHandler>();
                var eventhub = Mock.Of<IEventHubClientFactory>();
                var repository = Mock.Of<IFilterViewRepositoryFactory>();
                var logger = Mock.Of<ILoggerFactory>();
                var tenant = Mock.Of<ITenantServiceFactory>();
                var application = Mock.Of<IApplicationServiceClientFactory>();
                var offer_engine = Mock.Of<IOfferEngineServiceFactory>();
                var status_manager = Mock.Of<IStatusManagementServiceFactory>();
                var decision_engine = Mock.Of<IDecisionEngineClientFactory>();
                new ApplicationFilterListener(config, null, eventhub, repository, logger, tenant, application, offer_engine, status_manager, decision_engine, Mock.Of<IAssignmentServiceClientFactory>());
            });
        }

        [Fact]
        public void InitListenerWithNoEventhub()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var config = Mock.Of<IConfigurationServiceFactory<Configuration>>();
                var token = Mock.Of<ITokenHandler>();
                var eventhub = Mock.Of<IEventHubClientFactory>();
                var repository = Mock.Of<IFilterViewRepositoryFactory>();
                var logger = Mock.Of<ILoggerFactory>();
                var tenant = Mock.Of<ITenantServiceFactory>();
                var application = Mock.Of<IApplicationServiceClientFactory>();
                var offer_engine = Mock.Of<IOfferEngineServiceFactory>();
                var status_manager = Mock.Of<IStatusManagementServiceFactory>();
                var decision_engine = Mock.Of<IDecisionEngineClientFactory>();
                new ApplicationFilterListener(config, token, null, repository, logger, tenant, application, offer_engine, status_manager, decision_engine, Mock.Of<IAssignmentServiceClientFactory>());
            });
        }

        [Fact]
        public void InitListenerWithNoRepository()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var config = Mock.Of<IConfigurationServiceFactory<Configuration>>();
                var token = Mock.Of<ITokenHandler>();
                var eventhub = Mock.Of<IEventHubClientFactory>();
                var repository = Mock.Of<IFilterViewRepositoryFactory>();
                var logger = Mock.Of<ILoggerFactory>();
                var tenant = Mock.Of<ITenantServiceFactory>();
                var application = Mock.Of<IApplicationServiceClientFactory>();
                var offer_engine = Mock.Of<IOfferEngineServiceFactory>();
                var status_manager = Mock.Of<IStatusManagementServiceFactory>();
                var decision_engine = Mock.Of<IDecisionEngineClientFactory>();
                new ApplicationFilterListener(config, token, eventhub, null, logger, tenant, application, offer_engine, status_manager, decision_engine, Mock.Of<IAssignmentServiceClientFactory>());
            });
        }

        [Fact]
        public void InitListenerWithNoLogger()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var config = Mock.Of<IConfigurationServiceFactory<Configuration>>();
                var token = Mock.Of<ITokenHandler>();
                var eventhub = Mock.Of<IEventHubClientFactory>();
                var repository = Mock.Of<IFilterViewRepositoryFactory>();
                var logger = Mock.Of<ILoggerFactory>();
                var tenant = Mock.Of<ITenantServiceFactory>();
                var application = Mock.Of<IApplicationServiceClientFactory>();
                var offer_engine = Mock.Of<IOfferEngineServiceFactory>();
                var status_manager = Mock.Of<IStatusManagementServiceFactory>();
                var decision_engine = Mock.Of<IDecisionEngineClientFactory>();
                new ApplicationFilterListener(config, token, eventhub, repository, null, tenant, application, offer_engine, status_manager, decision_engine, Mock.Of<IAssignmentServiceClientFactory>());
            });
        }

        [Fact]
        public void InitListenerWithNoTenant()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var config = Mock.Of<IConfigurationServiceFactory<Configuration>>();
                var token = Mock.Of<ITokenHandler>();
                var eventhub = Mock.Of<IEventHubClientFactory>();
                var repository = Mock.Of<IFilterViewRepositoryFactory>();
                var logger = Mock.Of<ILoggerFactory>();
                var tenant = Mock.Of<ITenantServiceFactory>();
                var application = Mock.Of<IApplicationServiceClientFactory>();
                var offer_engine = Mock.Of<IOfferEngineServiceFactory>();
                var status_manager = Mock.Of<IStatusManagementServiceFactory>();
                var decision_engine = Mock.Of<IDecisionEngineClientFactory>();
                new ApplicationFilterListener(config, token, eventhub, repository, logger, null, application, offer_engine, status_manager, decision_engine, Mock.Of<IAssignmentServiceClientFactory>());
            });
        }

        [Fact]
        public void InitListenerWithNoApplication()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var config = Mock.Of<IConfigurationServiceFactory<Configuration>>();
                var token = Mock.Of<ITokenHandler>();
                var eventhub = Mock.Of<IEventHubClientFactory>();
                var repository = Mock.Of<IFilterViewRepositoryFactory>();
                var logger = Mock.Of<ILoggerFactory>();
                var tenant = Mock.Of<ITenantServiceFactory>();
                var application = Mock.Of<IApplicationServiceClientFactory>();
                var offer_engine = Mock.Of<IOfferEngineServiceFactory>();
                var status_manager = Mock.Of<IStatusManagementServiceFactory>();
                var decision_engine = Mock.Of<IDecisionEngineClientFactory>();
                new ApplicationFilterListener(config, token, eventhub, repository, logger, tenant, null, offer_engine, status_manager, decision_engine, Mock.Of<IAssignmentServiceClientFactory>());
            });
        }

        [Fact]
        public void InitListenerWithNoOfferEngine()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var config = Mock.Of<IConfigurationServiceFactory<Configuration>>();
                var token = Mock.Of<ITokenHandler>();
                var eventhub = Mock.Of<IEventHubClientFactory>();
                var repository = Mock.Of<IFilterViewRepositoryFactory>();
                var logger = Mock.Of<ILoggerFactory>();
                var tenant = Mock.Of<ITenantServiceFactory>();
                var application = Mock.Of<IApplicationServiceClientFactory>();
                var offer_engine = Mock.Of<IOfferEngineServiceFactory>();
                var status_manager = Mock.Of<IStatusManagementServiceFactory>();
                var decision_engine = Mock.Of<IDecisionEngineClientFactory>();
                new ApplicationFilterListener(config, token, eventhub, repository, logger, tenant, application, null, status_manager, decision_engine, Mock.Of<IAssignmentServiceClientFactory>());
            });
        }

        [Fact]
        public void InitListenerWithNoStatusManager()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var config = Mock.Of<IConfigurationServiceFactory<Configuration>>();
                var token = Mock.Of<ITokenHandler>();
                var eventhub = Mock.Of<IEventHubClientFactory>();
                var repository = Mock.Of<IFilterViewRepositoryFactory>();
                var logger = Mock.Of<ILoggerFactory>();
                var tenant = Mock.Of<ITenantServiceFactory>();
                var application = Mock.Of<IApplicationServiceClientFactory>();
                var offer_engine = Mock.Of<IOfferEngineServiceFactory>();
                var status_manager = Mock.Of<IStatusManagementServiceFactory>();
                var decision_engine = Mock.Of<IDecisionEngineClientFactory>();
                new ApplicationFilterListener(config, token, eventhub, repository, logger, tenant, application, offer_engine, null, decision_engine, Mock.Of<IAssignmentServiceClientFactory>());
            });
        }

        [Fact]
        public void InitListenerWithNoDecisionEngine()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var config = Mock.Of<IConfigurationServiceFactory<Configuration>>();
                var token = Mock.Of<ITokenHandler>();
                var eventhub = Mock.Of<IEventHubClientFactory>();
                var repository = Mock.Of<IFilterViewRepositoryFactory>();
                var logger = Mock.Of<ILoggerFactory>();
                var tenant = Mock.Of<ITenantServiceFactory>();
                var application = Mock.Of<IApplicationServiceClientFactory>();
                var offer_engine = Mock.Of<IOfferEngineServiceFactory>();
                var status_manager = Mock.Of<IStatusManagementServiceFactory>();
                var decision_engine = Mock.Of<IDecisionEngineClientFactory>();
                new ApplicationFilterListener(config, token, eventhub, repository, logger, tenant, application, offer_engine, status_manager, null, Mock.Of<IAssignmentServiceClientFactory>());
            });
        }

        [Fact]
        public void InitListenerWithAllDependenciesOk()
        {
            var config = Mock.Of<IConfigurationServiceFactory<Configuration>>();
            var token = Mock.Of<ITokenHandler>();
            var eventhub = Mock.Of<IEventHubClientFactory>();
            var repository = Mock.Of<IFilterViewRepositoryFactory>();
            var logger = Mock.Of<ILoggerFactory>();
            var tenant = Mock.Of<ITenantServiceFactory>();
            var application = Mock.Of<IApplicationServiceClientFactory>();
            var offer_engine = Mock.Of<IOfferEngineServiceFactory>();
            var status_manager = Mock.Of<IStatusManagementServiceFactory>();
            var decision_engine = Mock.Of<IDecisionEngineClientFactory>();

            var service = new ApplicationFilterListener(config, token, eventhub, repository, logger, tenant, application, offer_engine, status_manager, decision_engine, Mock.Of<IAssignmentServiceClientFactory>());
            Assert.NotNull(service);
            Assert.IsType<ApplicationFilterListener>(service);
        }

        [Fact]
        public void BasicListenerStartInitialization()
        {
            var logger = new Mock<ILogger>();
            logger.Setup(x => x.Info(It.IsAny<string>()));
            logger.Setup(x => x.Error(It.IsAny<string>()));
            var loggerFactory = new Mock<ILoggerFactory>();
            loggerFactory.Setup(x => x.Create(NullLogContext.Instance)).Returns(logger.Object);
            var service = GetListener(null, null, null, null, loggerFactory.Object);
            service.Start();

            Assert.NotNull(service);
            logger.Verify(x => x.Info($"Eventhub listener started"));
        }

        [Fact]
        public void StartMustToGetAllActiveTenantsToAttachEvents()
        {
            var logger = new Mock<ILogger>();
            logger.Setup(x => x.Info(It.IsAny<string>()));
            logger.Setup(x => x.Error(It.IsAny<string>()));
            var loggerFactory = new Mock<ILoggerFactory>();
            loggerFactory.Setup(x => x.Create(NullLogContext.Instance)).Returns(logger.Object);
            //---------
            var tenantList = new List<TenantInfo>
            {
                new TenantInfo { Id = "my-tenant-1" },
                new TenantInfo { Id = "my-tenant-2" }
            };
            var tenantFactory = new Mock<ITenantServiceFactory>();
            var tenant = new Mock<ITenantService>();
            tenant.Setup(x => x.GetActiveTenants()).Returns(tenantList);
            tenantFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(tenant.Object);

            var service = GetListener(null, null, null, null, loggerFactory.Object, tenantFactory.Object);
            service.Start();
            Assert.NotNull(service);
            logger.Verify(x => x.Info($"#{tenantList.Count} active tenant(s) found to be processed"));
        }

        [Fact]
        public void StartWhenNoApiConfigurationFoundInRunTime()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var tenantList = new List<TenantInfo>
                {
                    new TenantInfo { Id = "my-tenant-1" }
                };
                var tenantFactory = new Mock<ITenantServiceFactory>();
                var tenant = new Mock<ITenantService>();
                tenant.Setup(x => x.GetActiveTenants()).Returns(tenantList);
                tenantFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(tenant.Object);
                //---------
                var configFactory = new Mock<IConfigurationServiceFactory<Configuration>>();
                configFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>()).Get());
                var service = GetListener(configFactory.Object);
                service.Start();
            });
        }

        [Fact]
        public void AddSnapShotWhenNoApplicantsFound()
        {
            var logger = new Mock<ILogger>();
            logger.Setup(x => x.Warn(It.IsAny<string>()));
            var loggerFactory = new Mock<ILoggerFactory>();
            loggerFactory.Setup(x => x.Create(NullLogContext.Instance)).Returns(logger.Object);
            //---------
            var application = new Mock<IApplicationService>();
            application.Setup(x => x.GetByApplicationNumber(It.IsAny<string>())).Returns(new ApplicationResponse
            {
                Applicants = null
            });
            var applicationFactory = new Mock<IApplicationServiceClientFactory>();
            applicationFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(application.Object);
            //---------
            var hub = new FakeEventHub();
            hub.EventInfo = new EventInfo
            {
                Data = new { Application = new { ApplicationNumber = "APP0001" } }
            };
            var hubFactory = new FakeEventHubFactory() { EventHub = hub };
            //---------
            var service = GetListener(null, null, hubFactory, null, loggerFactory.Object, null, applicationFactory.Object);
            service.Start();
            //---------

            logger.Verify(x => x.Warn(It.Is<string>(z => z.Contains("No applicants found to application"))));
        }

        [Fact(Skip = "Due to error")]
        public void AddSnapShotWhenNoOfferFound()
        {
            var offer = new Mock<IOfferEngineService>();
            offer.Setup(x => x.GetByApplication(It.IsAny<string>()));
            var offerFactory = new Mock<IOfferEngineServiceFactory>();
            offerFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(offer.Object);
            //---------
            var status = new Mock<IEntityStatusService>();
            status.Setup(x => x.GetStatusByEntity(It.IsAny<string>(), It.IsAny<string>()));
            var statusFactory = new Mock<IStatusManagementServiceFactory>();
            statusFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(status.Object);
            //---------
            var logger = new Mock<ILogger>();
            logger.Setup(x => x.Warn(It.IsAny<string>()));
            var loggerFactory = new Mock<ILoggerFactory>();
            loggerFactory.Setup(x => x.Create(NullLogContext.Instance)).Returns(logger.Object);
            //---------
            var application = new Mock<IApplicationService>();
            application.Setup(x => x.GetByApplicationNumber(It.IsAny<string>())).Returns(new ApplicationResponse
            {
                ApplicationNumber = "APP0001",
                SubmittedDate = new TimeBucket(DateTime.Now),
                Source = new Source { Id = "Id", Type = SourceType.Broker },
                Applicants = new List<IApplicantRequest> {
                    new ApplicantRequest {
                        IsPrimary = true,
                        FirstName = "FirstName",
                        MiddleName = "MiddleName",
                        LastName = "LastName"
                    }
                }
            });
            var applicationFactory = new Mock<IApplicationServiceClientFactory>();
            applicationFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(application.Object);
            //---------
            var hub = new FakeEventHub();
            hub.EventInfo = new EventInfo
            {
                Data = new { Application = new { ApplicationNumber = "APP0001" } }
            };
            var hubFactory = new FakeEventHubFactory() { EventHub = hub };
            //---------
            var service = GetListener(null, null, hubFactory, null, loggerFactory.Object, null, applicationFactory.Object, offerFactory.Object, statusFactory.Object);
            service.Start();
            //---------

            logger.Verify(x => x.Info(It.Is<string>(z => z.Contains("No offers found for this snapshot"))));
        }

        [Fact(Skip = "Due to error")]
        public void AddSnapShotWhenOfferFound()
        {
            var offer = new Mock<IOfferEngineService>();
            offer.Setup(x => x.GetByApplication(It.IsAny<string>())).Returns(new OfferDefinition
            {
                MaxAmount = 2000,            
                Offers = new[] {
                    new Offer {
                        OfferAmount = 1000,
                        PaymentAmount = 100,
                        Selected = true
                    }
                }
            });
            var offerFactory = new Mock<IOfferEngineServiceFactory>();
            offerFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(offer.Object);
            //---------
            var status = new Mock<IEntityStatusService>();
            status.Setup(x => x.GetStatusByEntity(It.IsAny<string>(), It.IsAny<string>()));
            var statusFactory = new Mock<IStatusManagementServiceFactory>();
            statusFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(status.Object);
            //---------
            var logger = new Mock<ILogger>();
            logger.Setup(x => x.Warn(It.IsAny<string>()));
            var loggerFactory = new Mock<ILoggerFactory>();
            loggerFactory.Setup(x => x.Create(NullLogContext.Instance)).Returns(logger.Object);
            //---------
            var application = new Mock<IApplicationService>();
            application.Setup(x => x.GetByApplicationNumber(It.IsAny<string>())).Returns(new ApplicationResponse
            {
                ApplicationNumber = "APP0001",
                SubmittedDate = new TimeBucket(DateTime.Now),
                Source = new Source { Id = "Id", Type = SourceType.Broker },
                Applicants = new List<IApplicantRequest> {
                    new ApplicantRequest {
                        IsPrimary = true,
                        FirstName = "FirstName",
                        MiddleName = "MiddleName",
                        LastName = "LastName"
                    }
                }
            });
            var applicationFactory = new Mock<IApplicationServiceClientFactory>();
            applicationFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(application.Object);
            //---------
            var hub = new FakeEventHub();
            hub.EventInfo = new EventInfo
            {
                Data = new { Application = new { ApplicationNumber = "APP0001" } }
            };
            var hubFactory = new FakeEventHubFactory() { EventHub = hub };
            //---------
            var service = GetListener(null, null, hubFactory, null, loggerFactory.Object, null, applicationFactory.Object, offerFactory.Object, statusFactory.Object);
            service.Start();
            //---------

            logger.Verify(x => x.Info(It.Is<string>(z => z.Contains("Offer found for this snapshot Amount"))));
        }

        [Fact(Skip ="Due to error")]
        public void AddSnapShotWhenNoApplicationStatus()
        {
            var offer = new Mock<IOfferEngineService>();
            offer.Setup(x => x.GetByApplication(It.IsAny<string>()));
            var offerFactory = new Mock<IOfferEngineServiceFactory>();
            offerFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(offer.Object);
            //---------
            var status = new Mock<IEntityStatusService>();
            status.Setup(x => x.GetStatusByEntity(It.IsAny<string>(), It.IsAny<string>()));
            var statusFactory = new Mock<IStatusManagementServiceFactory>();
            statusFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(status.Object);
            //---------
            var logger = new Mock<ILogger>();
            logger.Setup(x => x.Warn(It.IsAny<string>()));
            var loggerFactory = new Mock<ILoggerFactory>();
            loggerFactory.Setup(x => x.Create(NullLogContext.Instance)).Returns(logger.Object);
            //---------
            var application = new Mock<IApplicationService>();
            application.Setup(x => x.GetByApplicationNumber(It.IsAny<string>())).Returns(new ApplicationResponse
            {
                ApplicationNumber = "APP0001",
                SubmittedDate = new TimeBucket(DateTime.Now),
                Source = new Source { Id = "Id", Type = SourceType.Broker },
                Applicants = new List<IApplicantRequest> {
                    new ApplicantRequest {
                        IsPrimary = true,
                        FirstName = "FirstName",
                        MiddleName = "MiddleName",
                        LastName = "LastName"
                    }
                }
            });
            var applicationFactory = new Mock<IApplicationServiceClientFactory>();
            applicationFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(application.Object);
            //---------
            var hub = new FakeEventHub();
            hub.EventInfo = new EventInfo
            {
                Data = new { Application = new { ApplicationNumber = "APP0001" } }
            };
            var hubFactory = new FakeEventHubFactory() { EventHub = hub };
            //---------
            var service = GetListener(null, null, hubFactory, null, loggerFactory.Object, null, applicationFactory.Object, offerFactory.Object, statusFactory.Object);
            service.Start();
            //---------

            logger.Verify(x => x.Info(It.Is<string>(z => z.Contains("No application status found for this snapshot"))));
        }

        private ApplicationFilterListener GetListener
        (
            IConfigurationServiceFactory<Configuration> apiConfigurationFactory = null,
            ITokenHandler tokenHandler = null,
            IEventHubClientFactory eventHubFactory = null,
            IFilterViewRepositoryFactory repositoryFactory = null,
            ILoggerFactory loggerFactory = null,
            ITenantServiceFactory tenantServiceFactory = null,
            IApplicationServiceClientFactory applicationServiceFactory = null,
            IOfferEngineServiceFactory offerEngineFactory = null,
            IStatusManagementServiceFactory statusManagementFactory = null,
            IDecisionEngineClientFactory decisionEngineFactory = null
        )
        {
            var emptyReader = It.IsAny<StaticTokenReader>();

            // configuration
            var inTimeApiConfiguration = apiConfigurationFactory;
            if (inTimeApiConfiguration == null)
            {
                var mockConfig = new Mock<IConfigurationServiceFactory<Configuration>>();
                mockConfig.Setup(x => x.Create(emptyReader).Get()).Returns(new Configuration
                {
                    Events = new List<EventMapping>
                    {
                        new EventMapping
                        {
                            ApplicationNumber = "{Data.Application.ApplicationNumber}",
                            Name = "ApplicationCreated"
                        }
                    }.ToArray()
                });
                inTimeApiConfiguration = mockConfig.Object;
            }

            //token
            var inTimeTokenHandler = tokenHandler;
            if (inTimeTokenHandler == null)
            {
                var mockTokenHandler = new Mock<ITokenHandler>();
                var token = new Mock<IToken>();
                token.Setup(x => x.Value).Returns("Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM");
                mockTokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>())).Returns(token.Object);
                inTimeTokenHandler = mockTokenHandler.Object;
            }

            //eventhub
            var inTimeEventhubFactory = eventHubFactory;
            if (inTimeEventhubFactory == null)
            {
                var eventHubClient = new Mock<IEventHubClient>();
                eventHubClient.Setup(x => x.StartAsync());
                eventHubClient.Setup(x => x.On(It.IsAny<string>(), It.IsAny<Action<EventInfo>>()));
                var mockHubFactory = new Mock<IEventHubClientFactory>();
                mockHubFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(eventHubClient.Object);
                inTimeEventhubFactory = mockHubFactory.Object;
            }

            //repository
            var inTimeRepositoryFactory = repositoryFactory;
            if (inTimeRepositoryFactory == null)
            {
                var repository = new Mock<IFilterViewRepository>();
                repository.Setup(x => x.AddOrUpdate(It.IsAny<IFilterView>()));
                var mockRepositoryFactory = new Mock<IFilterViewRepositoryFactory>();
                mockRepositoryFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(repository.Object);
                inTimeRepositoryFactory = mockRepositoryFactory.Object;
            }

            //logger            
            var inTimeLoggerFactory = loggerFactory;
            if (inTimeLoggerFactory == null)
            {
                var logger = new Mock<ILogger>();
                logger.Setup(x => x.Info(It.IsAny<string>()));
                logger.Setup(x => x.Error(It.IsAny<string>()));
                var mockLoggerFactory = new Mock<ILoggerFactory>();
                mockLoggerFactory.Setup(x => x.Create(NullLogContext.Instance)).Returns(logger.Object);
                inTimeLoggerFactory = mockLoggerFactory.Object;
            }

            //tenant
            var inTimeTenantFactory = tenantServiceFactory;
            if (inTimeTenantFactory == null)
            {
                var mockTenantServiceFactory = new Mock<ITenantServiceFactory>();
                var tenantService = new Mock<ITenantService>();
                tenantService.Setup(x => x.GetActiveTenants()).Returns(new List<TenantInfo>
                {
                    new TenantInfo { Id = "my-tenant" }
                });
                mockTenantServiceFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(tenantService.Object);
                inTimeTenantFactory = mockTenantServiceFactory.Object;
            }

            // application
            var inTimeApplicationFactory = applicationServiceFactory;
            if (inTimeApplicationFactory == null)
            {
                inTimeApplicationFactory = Mock.Of<IApplicationServiceClientFactory>();
            }

            // offer engine
            var inTimeOfferEngineFactory = offerEngineFactory;
            if (inTimeOfferEngineFactory == null)
            {
                inTimeOfferEngineFactory = Mock.Of<IOfferEngineServiceFactory>();
            }

            // status management
            var inTimeStatusManagementFactory = statusManagementFactory;
            if (inTimeStatusManagementFactory == null)
            {
                inTimeStatusManagementFactory = Mock.Of<IStatusManagementServiceFactory>();
            }

            // decision engine
            var inTimeDecisionEngineFactory = decisionEngineFactory;
            if (inTimeDecisionEngineFactory == null)
            {
                inTimeDecisionEngineFactory = Mock.Of<IDecisionEngineClientFactory>();
            }

            return new ApplicationFilterListener
            (
                inTimeApiConfiguration,
                inTimeTokenHandler,
                inTimeEventhubFactory,
                inTimeRepositoryFactory,
                inTimeLoggerFactory,
                inTimeTenantFactory,
                inTimeApplicationFactory,
                inTimeOfferEngineFactory,
                inTimeStatusManagementFactory,
                inTimeDecisionEngineFactory,
                Mock.Of<IAssignmentServiceClientFactory>()
            );
        }
    }
}
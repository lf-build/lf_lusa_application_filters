﻿using LendFoundry.Applications.Filters.Common.Tests;
using LendFoundry.Applications.Filters.Common.Tests.Fakes;
using LendFoundry.AssignmentEngine;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Applications.Filters.Tests
{
    public class ApplicationFilterServiceTest : InMemoryObjects
    {
        [Fact]
        public void GetAllBySourceWhenNullSourceType()
        {
            Assert.Throws<InvalidArgumentException>(() => GetService().GetAllBySource(null, "SourceId3"));
        }

        [Fact]
        public void GetAllBySourceWhenNullSourceId()
        {
            Assert.Throws<InvalidArgumentException>(() => GetService().GetAllBySource("SourceType3", null));
        }

        [Fact]
        public void GetAllBySourceWhenInvalidSourceType()
        {
            var results = GetService().GetAllBySource("invalid", "SourceId3");
            Assert.NotNull(results);
            Assert.Empty(results);
        }

        [Fact]
        public void GetAllBySourceWhenInvalidSourceId()
        {
            var results = GetService().GetAllBySource("SourceType3", "invalid");
            Assert.NotNull(results);
            Assert.Empty(results);
        }

        [Fact]
        public void GetAllBySourceWhenItHasData()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            repository.Add(filter);

            // creates a service and gets item to match
            var service = GetService(repository);
            var data = service.GetAllBySource(_sourceType, _sourceId);
            Assert.NotNull(data);
            Assert.NotEmpty(data);
            Assert.True(data.ToList().TrueForAll(x => x.SourceId == _sourceId));
            Assert.True(data.ToList().TrueForAll(x => x.SourceType == _sourceType));
            Assert.True(data.Count(x => x.StatusCode == "100.01") == 1);
            Assert.True(data.Count(x => x.StatusCode == "100.02") == 1);
        }

        [Fact]
        public void GetAllBySourceWhenItHasNoData()
        {
            var _sourceType = "merchant";
            var _sourceId = "merchant001";

            // creates a service and gets item to match
            var service = GetService();
            var data = service.GetAllBySource(_sourceType, _sourceId);
            Assert.NotNull(data);
            Assert.Empty(data);
        }

        [Fact]
        public void GetBySourceAndStatusWhenNullSourceType()
        {
            Assert.Throws<InvalidArgumentException>(() => GetService().GetBySourceAndStatus(null, "SourceId3", new[] { "R" }));
        }

        [Fact]
        public void GetBySourceAndStatusWhenNullSourceId()
        {
            Assert.Throws<InvalidArgumentException>(() => GetService().GetBySourceAndStatus("SourceType3", null, new[] { "R" }));
        }

        [Fact]
        public void GetBySourceAndStatusWhenNullStatusesList()
        {
            Assert.Throws<ArgumentException>(() => GetService().GetBySourceAndStatus("SourceType3", "SourceId3", null));
        }

        [Fact]
        public void GetBySourceAndStatusWhenInvalidSourceType()
        {
            var results = GetService().GetBySourceAndStatus("invalid", "invalid", new[] { "R" });
            Assert.NotNull(results);
            Assert.Empty(results);
        }

        [Fact]
        public void GetBySourceAndStatusWhenInvalidSourceId()
        {
            var results = GetService().GetBySourceAndStatus("SourceType3", "invalid", new[] { "R" });
            Assert.NotNull(results);
            Assert.Empty(results);
        }

        [Fact]
        public void GetBySourceAndStatusWhenInvalidStatusesList()
        {
            var results = GetService().GetBySourceAndStatus("SourceType3", "SourceId3", new[] { "" });
            Assert.NotNull(results);
            Assert.Empty(results);
        }

        [Fact]
        public void GetBySourceAndStatusWhenItHasData()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            repository.Add(filter);

            // try gets the object back
            var service = GetService(repository);
            var result = service.GetBySourceAndStatus(_sourceType, _sourceId, new[]
            {
                "100.01"
            });

            Assert.NotNull(result);
            Assert.NotEmpty(result);
            Assert.Equal(_sourceId, result.First().SourceId);
            Assert.Equal(_sourceType, result.First().SourceType);
            Assert.True(result.ToList().TrueForAll(x => x.StatusCode == "100.01"));
        }

        [Fact]
        public void GetBySourceAndStatusWhenItHasNoData()
        {
            var _sourceType = "merchant";
            var _sourceId = "merchant001";

            // try gets the object back
            var service = GetService();
            var result = service.GetBySourceAndStatus(_sourceType, _sourceId, new[]
            {
                "100.01"
            });

            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public void InitializeWithoutRepository()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new ApplicationFilterService
                (
                    null,
                    Mock.Of<ILogger>(),
                    Mock.Of<ITenantTime>(),
                    Mock.Of<Configuration>(),
                    Mock.Of<ITokenReader>(),
                    Mock.Of<ITokenHandler>(),
                    Mock.Of<IIdentityService>()
                );
            });
        }

        [Fact]
        public void InitializeWithoutLogger()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new ApplicationFilterService
                (
                    Mock.Of<IFilterViewRepository>(),
                    null,
                    Mock.Of<ITenantTime>(),
                    Mock.Of<Configuration>(),
                    Mock.Of<ITokenReader>(),
                    Mock.Of<ITokenHandler>(),
                    Mock.Of<IIdentityService>()
                );
            });
        }

        [Fact]
        public void InitializeWithoutTenantTime()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new ApplicationFilterService
                (
                    Mock.Of<IFilterViewRepository>(),
                    Mock.Of<ILogger>(),
                    null,
                    Mock.Of<Configuration>(),
                    Mock.Of<ITokenReader>(),
                    Mock.Of<ITokenHandler>(),
                    Mock.Of<IIdentityService>()
                );
            });
        }

        [Fact]
        public void InitializeWithoutConfiguration()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new ApplicationFilterService
                (
                    Mock.Of<IFilterViewRepository>(),
                    Mock.Of<ILogger>(),
                    Mock.Of<ITenantTime>(),
                    null,
                    Mock.Of<ITokenReader>(),
                    Mock.Of<ITokenHandler>(),
                    Mock.Of<IIdentityService>()
                );
            });
        }

        [Fact]
        public void InitializeWithoutTokenReader()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new ApplicationFilterService
                (
                    Mock.Of<IFilterViewRepository>(),
                    Mock.Of<ILogger>(),
                    Mock.Of<ITenantTime>(),
                    Mock.Of<Configuration>(),
                    null,
                    Mock.Of<ITokenHandler>(),
                    Mock.Of<IIdentityService>()
                );
            });
        }

        [Fact]
        public void InitializeWithoutTokenHandler()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new ApplicationFilterService
                (
                    Mock.Of<IFilterViewRepository>(),
                    Mock.Of<ILogger>(),
                    Mock.Of<ITenantTime>(),
                    Mock.Of<Configuration>(),
                    Mock.Of<ITokenReader>(),
                    null,
                    Mock.Of<IIdentityService>()
                );
            });
        }

        [Fact]
        public void InitializeWithoutIdentityService()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new ApplicationFilterService
                (
                    Mock.Of<IFilterViewRepository>(),
                    Mock.Of<ILogger>(),
                    Mock.Of<ITenantTime>(),
                    Mock.Of<Configuration>(),
                    Mock.Of<ITokenReader>(),
                    Mock.Of<ITokenHandler>(),
                    null
                );
            });
        }

        [Fact]
        public void GetTotalSubmittedForCurrentMonthExecutionOk()
        {
            // arrange
            var repo = new FakeRepository();
            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var entries = new[]
            {
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView
                };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                if (i > 3)
                    entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(1).Day);
                else
                    entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.AddDays(-31).Month, DateTime.Now.AddDays(-10).Day);

                return entry;
            }).ToList().ForEach((filterView) =>
            {
                repo.Add(filterView);
            });

            Logger.Setup(x => x.Error(It.IsAny<string>()));

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            var firstDayOfMonth = new DateTimeOffset(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0));

            // action
            var result = GetService(repo, null, Logger.Object, TenantTime.Object).GetTotalSubmittedForCurrentMonth(sourceType, sourceId);

            // assert
            Assert.NotNull(result);
            Assert.Equal(7, result.Result);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
            TenantTime.Verify(x => x.Now, Times.AtLeastOnce);
        }

        [Fact]
        public void GetTotalSubmittedForCurrentMonthExecutionOkButIfNoData()
        {
            // arrange
            var repo = new FakeRepository();
            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var entries = new[]
            {
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView
                };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.AddDays(-31).Month, DateTime.Now.AddDays(-10).Day);

                return entry;
            }).ToList().ForEach((filterView) =>
            {
                repo.Add(filterView);
            });

            Logger.Setup(x => x.Error(It.IsAny<string>()));

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            var firstDayOfMonth = new DateTimeOffset(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0));

            // action
            var result = GetService(repo, null, Logger.Object, TenantTime.Object).GetTotalSubmittedForCurrentMonth(sourceType, sourceId);

            // assert
            Assert.NotNull(result);
            Assert.Equal(0, result.Result);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
            TenantTime.Verify(x => x.Now, Times.AtLeastOnce);
        }

        [Fact]
        public void GetTotalSubmittedForCurrentMonthWhenInvalidSourceType()
        {
            Assert.ThrowsAsync<InvalidArgumentException>(() => GetService().GetTotalSubmittedForCurrentMonth(null, "SourceId3"));
        }

        [Fact]
        public void GetTotalSubmittedForCurrentMonthWhenInvalidSourceId()
        {
            Assert.ThrowsAsync<InvalidArgumentException>(() => GetService().GetTotalSubmittedForCurrentMonth("SourceType1", null));
        }

        [Fact]
        public void GetTotalSubmittedForCurrentYearExecutionOk()
        {
            // arrange
            var repo = new FakeRepository();
            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var entries = new[]
            {
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView
                };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                if (i > 5)
                    entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(1).Day);
                else
                    entry.Submitted = new DateTime(DateTime.Now.AddYears(-1).Year, DateTime.Now.Month, DateTime.Now.Day);

                return entry;
            }).ToList().ForEach((filterView) =>
            {
                repo.Add(filterView);
            });

            Logger.Setup(x => x.Error(It.IsAny<string>()));

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            var firstDayOfMonth = new DateTimeOffset(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0));

            // action
            var result = GetService(repo, null, Logger.Object, TenantTime.Object).GetTotalSubmittedForCurrentYear(sourceType, sourceId);

            // assert
            Assert.NotNull(result);
            Assert.Equal(5, result.Result);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
            TenantTime.Verify(x => x.Now, Times.AtLeastOnce);
        }

        [Fact]
        public void GetTotalSubmittedForCurrentYearExecutionOkButIfNoData()
        {
            // arrange
            var repo = new FakeRepository();
            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var entries = new[]
            {
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView
                };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                entry.Submitted = new DateTime(DateTime.Now.AddYears(-1).Year, DateTime.Now.Month, DateTime.Now.Day);

                return entry;
            }).ToList().ForEach((filterView) =>
            {
                repo.Add(filterView);
            });

            Logger.Setup(x => x.Error(It.IsAny<string>()));

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            var firstDayOfMonth = new DateTimeOffset(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0));

            // action
            var result = GetService(repo, null, Logger.Object, TenantTime.Object).GetTotalSubmittedForCurrentYear(sourceType, sourceId);

            // assert
            Assert.NotNull(result);
            Assert.Equal(0, result.Result);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
            TenantTime.Verify(x => x.Now, Times.AtLeastOnce);
        }

        [Fact]
        public void GetTotalSubmittedForCurrentYearWhenInvalidSourceType()
        {
            Assert.ThrowsAsync<InvalidArgumentException>(() => GetService().GetTotalSubmittedForCurrentYear(null, "SourceId3"));
        }

        [Fact]
        public void GetTotalSubmittedForCurrentYearWhenInvalidSourceId()
        {
            Assert.ThrowsAsync<InvalidArgumentException>(() => GetService().GetTotalSubmittedForCurrentYear("SourceType1", null));
        }

        [Fact]
        public void GetByStatusWhenNoSourceTypeInformed()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> statuses = new[] { "100.01", "100.02" };

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                sourceType = "";
                await service.GetByStatus(sourceType, sourceId, statuses);
            });

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                sourceType = string.Empty;
                await service.GetByStatus(sourceType, sourceId, statuses);
            });
        }

        [Fact]
        public void GetByStatusWhenNoSourceIdInformed()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> statuses = new[] { "100.01", "100.02" };

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                sourceId = "";
                await service.GetByStatus(sourceType, sourceId, statuses);
            });

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                sourceId = string.Empty;
                await service.GetByStatus(sourceType, sourceId, statuses);
            });
        }

        [Fact]
        public void GetByStatusWhenNoStatusInformed()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> statuses = new[] { "100.01", "100.02" };

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                statuses = null;
                await service.GetByStatus(sourceType, sourceId, statuses);
            });

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                statuses = Enumerable.Empty<string>();
                await service.GetByStatus(sourceType, sourceId, statuses);
            });
        }

        [Fact]
        public void GetByStatusWhenExecutionOkWithData()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> statuses = new[] { "100.01", "100.02" };
            var repo = new FakeRepository();
            for (int i = 0; i < 10; i++)
            {
                var item = GetData();
                item.SourceType = sourceType;
                item.SourceId = sourceId;
                item.StatusCode = statuses.First();
                repo.Add(item);
            }

            for (int i = 0; i < 10; i++)
            {
                var item = GetData();
                item.SourceType = "borrower";
                item.SourceId = "BOW001";
                item.StatusCode = statuses.First();
                repo.Add(item);
            }

            var service = GetService(repo);
            var data = service.GetByStatus(sourceType, sourceId, statuses).Result;

            Assert.NotNull(data);
            Assert.NotEmpty(data);
            Assert.True(data.ToList().TrueForAll(x => x.StatusCode == statuses.First()));
            Assert.True(data.ToList().TrueForAll(x => x.SourceId == sourceId));
            Assert.True(data.ToList().TrueForAll(x => x.SourceType == sourceType));
            Assert.Equal(10, data.Count());
        }

        [Fact]
        public void GetByStatusWhenExecutionOkButIfNoData()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> statuses = new[] { "100.01", "100.02" };
            var service = GetService();
            var data = service.GetByStatus(sourceType, sourceId, statuses).Result;

            Assert.NotNull(data);
            Assert.Empty(data);
        }

        [Fact]
        public void GetCountByStatusWhenNoSourceTypeInformed()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> statuses = new[] { "100.01", "100.02" };

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                sourceType = "";
                await service.GetCountByStatus(sourceType, sourceId, statuses);
            });

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                sourceType = string.Empty;
                await service.GetCountByStatus(sourceType, sourceId, statuses);
            });
        }

        [Fact]
        public void GetCountByStatusWhenNoSourceIdInformed()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> statuses = new[] { "100.01", "100.02" };

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                sourceId = "";
                await service.GetCountByStatus(sourceType, sourceId, statuses);
            });

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                sourceId = string.Empty;
                await service.GetCountByStatus(sourceType, sourceId, statuses);
            });
        }

        [Fact]
        public void GetCountByStatusWhenNoStatusInformed()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> statuses = new[] { "100.01", "100.02" };

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                statuses = null;
                await service.GetCountByStatus(sourceType, sourceId, statuses);
            });

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                statuses = Enumerable.Empty<string>();
                await service.GetCountByStatus(sourceType, sourceId, statuses);
            });
        }

        [Fact]
        public void GetCountByStatusWhenExecutionOkWithData()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> statuses = new[] { "100.01", "100.02" };
            var repo = new FakeRepository();
            for (int i = 0; i < 10; i++)
            {
                var item = GetData();
                item.SourceType = sourceType;
                item.SourceId = sourceId;
                item.StatusCode = statuses.First();
                repo.Add(item);
            }

            for (int i = 0; i < 10; i++)
            {
                var item = GetData();
                item.SourceType = "borrower";
                item.SourceId = "BOW001";
                item.StatusCode = statuses.First();
                repo.Add(item);
            }

            var service = GetService(repo);
            var data = service.GetCountByStatus(sourceType, sourceId, statuses).Result;

            Assert.NotNull(data);
            Assert.Equal(10, data);
        }

        [Fact]
        public void GetCountByStatusWhenExecutionButIfNoData()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> statuses = new[] { "100.01", "100.02" };
            var service = GetService();
            var data = service.GetCountByStatus(sourceType, sourceId, statuses).Result;

            Assert.NotNull(data);
            Assert.Equal(0, data);
        }

        [Fact]
        public void GetByTagWhenNoSourceTypeInformed()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> tags = new[] { "100.01", "100.02" };

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                sourceType = "";
                await service.GetByTag(sourceType, sourceId, tags);
            });

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                sourceType = string.Empty;
                await service.GetByTag(sourceType, sourceId, tags);
            });
        }

        [Fact]
        public void GetByTagWhenNoSourceIdInformed()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> tags = new[] { "100.01", "100.02" };

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                sourceId = "";
                await service.GetByTag(sourceType, sourceId, tags);
            });

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                sourceId = string.Empty;
                await service.GetByTag(sourceType, sourceId, tags);
            });
        }

        [Fact]
        public void GetByTagWhenNoTagsInformed()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> tags = new[] { "100.01", "100.02" };

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                tags = null;
                await service.GetByTag(sourceType, sourceId, tags);
            });

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                tags = Enumerable.Empty<string>();
                await service.GetByTag(sourceType, sourceId, tags);
            });
        }

        [Fact]
        public void GetByTagWhenExecutionOkWithData()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> tags = new[] { "tag-b" };
            var repo = new FakeRepository();
            for (int i = 0; i < 10; i++)
            {
                var item = GetData();
                item.SourceType = sourceType;
                item.SourceId = sourceId;
                item.Tags = tags;
                repo.Add(item);
            }

            for (int i = 0; i < 10; i++)
            {
                var item = GetData();
                item.SourceType = sourceType;
                item.SourceId = sourceId;
                item.Tags = new[] { "tag-a" };
                repo.Add(item);
            }

            var service = GetService(repo);
            var data = service.GetByTag(sourceType, sourceId, tags).Result;

            Assert.NotNull(data);
            Assert.NotEmpty(data);
            Assert.Equal(10, (from a in data
                              from b in a.Tags
                              where b == tags.First()
                              select b).Count());
            Assert.True(data.ToList().TrueForAll(x => x.SourceId == sourceId));
            Assert.True(data.ToList().TrueForAll(x => x.SourceType == sourceType));
        }

        [Fact]
        public void GetByTagWhenExecutionOkWithNoData()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> tags = new[] { "tag-b" };
            var service = GetService();
            var data = service.GetByTag(sourceType, sourceId, tags).Result;

            Assert.NotNull(data);
            Assert.Empty(data);
        }

        [Fact]
        public void GetCountByTagWhenNoSourceTypeInformed()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> tags = new[] { "100.01", "100.02" };

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                sourceType = "";
                await service.GetCountByTag(sourceType, sourceId, tags);
            });

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                sourceType = string.Empty;
                await service.GetCountByTag(sourceType, sourceId, tags);
            });
        }

        [Fact]
        public void GetCountByTagWhenNoSourceIdInformed()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> tags = new[] { "100.01", "100.02" };

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                sourceId = "";
                await service.GetCountByTag(sourceType, sourceId, tags);
            });

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                sourceId = string.Empty;
                await service.GetCountByTag(sourceType, sourceId, tags);
            });
        }

        [Fact]
        public void GetCountByTagWhenNoTagsInformed()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> tags = new[] { "100.01", "100.02" };

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                tags = null;
                await service.GetCountByTag(sourceType, sourceId, tags);
            });

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                tags = Enumerable.Empty<string>();
                await service.GetCountByTag(sourceType, sourceId, tags);
            });
        }

        [Fact]
        public void GetCountByTagWhenExecutionOkWithData()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> tags = new[] { "tag-b" };
            var repo = new FakeRepository();
            for (int i = 0; i < 10; i++)
            {
                var item = GetData();
                item.SourceType = sourceType;
                item.SourceId = sourceId;
                item.Tags = tags;
                repo.Add(item);
            }

            for (int i = 0; i < 10; i++)
            {
                var item = GetData();
                item.SourceType = sourceType;
                item.SourceId = sourceId;
                item.Tags = new[] { "tag-a" };
                repo.Add(item);
            }

            var service = GetService(repo);
            var data = service.GetCountByTag(sourceType, sourceId, tags).Result;

            Assert.NotNull(data);
            Assert.Equal(10, data);
        }

        [Fact]
        public void GetCountByTagWhenExecutionOkWithNoData()
        {
            string sourceType = "merchant";
            string sourceId = "MER001";
            IEnumerable<string> tags = new[] { "tag-b" };
            var service = GetService();
            var data = service.GetCountByTag(sourceType, sourceId, tags).Result;

            Assert.NotNull(data);
            Assert.Equal(0, data);
        }

        [Fact]
        public void GetByNameExecuteOk()
        {
            var sourceType = "merchant";
            var sourceid = "MER0001";
            var name = "test";

            var fakeRepository = new FakeRepository();

            for (var i = 0; i < 10; i++)
            {
                var item = GetData();
                item.SourceId = sourceid;
                item.SourceType = sourceType;
                item.Applicant = name;

                fakeRepository.Add(item);
            }

            var service = GetService(fakeRepository);
            var response = service.GetByName(sourceType, sourceid, name).Result;

            Assert.NotNull(response);
            Assert.NotEmpty(response);
            Assert.Equal(response.FirstOrDefault().Applicant, name);
        }

        [Fact]
        public void GetByNameExecuteOkButWithoutData()
        {
            var sourceType = "merchant";
            var sourceid = "MER0001";
            var name = "test";

            var fakeRepository = new FakeRepository();
            var item = GetData();
            fakeRepository.Add(item);

            var service = GetService(fakeRepository);
            var response = service.GetByName(sourceType, sourceid, name).Result;

            Assert.Equal(response.Count(), 0);
        }

        [Fact]
        public void GetByNameExecuteWithInvalidSourceType()
        {
            var sourceid = "MER0001";
            var name = "test";

            var fakeRepository = new FakeRepository();
            var item = GetData();
            fakeRepository.Add(item);

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                string sourceType = null;
                await service.GetByName(sourceType, sourceid, name);
            });

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                var sourceType = string.Empty;
                await service.GetByName(sourceType, sourceid, name);
            });
        }

        [Fact]
        public void GetByNameExecuteWithInvalidSourceId()
        {
            var sourceType = "merchant";
            var name = "test";

            var fakeRepository = new FakeRepository();
            var item = GetData();
            fakeRepository.Add(item);

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                string sourceid = null;
                await service.GetByName(sourceType, sourceid, name);
            });

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                var sourceid = string.Empty;
                await service.GetByName(sourceType, sourceid, name);
            });
        }

        [Fact]
        public void GetByNameExecuteWithEmptyName()
        {
            var sourceType = "merchant";
            var sourceid = "MER0001";

            var fakeRepository = new FakeRepository();
            var item = GetData();
            fakeRepository.Add(item);

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                string name = null;
                await service.GetByName(sourceType, sourceid, name);
            });

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = GetService();
                var name = string.Empty;
                await service.GetByName(sourceType, sourceid, name);
            });
        }

        [Fact]
        public void GetApprovedLoanAmountTotalBySourceWhenNullSourceType()
        {
            Assert.Throws<InvalidArgumentException>(() => GetService().GetTotalAmountApprovedBySource(null, "SourceId3"));
        }

        [Fact]
        public void GetApprovedLoanAmountTotalBySourceWhenNullSourceId()
        {
            Assert.Throws<InvalidArgumentException>(() => GetService().GetTotalAmountApprovedBySource("SourceType3", null));
        }

        [Fact]
        public void GetApprovedLoanAmountTotalBySourceWhenNullConfigurationProvided()
        {
            Assert.Throws<ArgumentException>(
                () => GetService().GetTotalAmountApprovedBySource("SourceType3", "SourceId3"));
        }

        [Fact]
        public void GetApprovedLoanAmountTotalBySourceWhenInvalidSourceType()
        {
            var results = GetService(null, null, null, null, new Configuration() { ApprovedStatuses = new[] { "100.013" } }).GetTotalAmountApprovedBySource("invalid", "invalid");
            Assert.NotNull(results);
            Assert.Equal(results, default(double));
        }

        [Fact]
        public void GetApprovedLoanAmountTotalBySourceWhenInvalidSourceId()
        {
            var results = GetService(null, null, null, null, new Configuration() { ApprovedStatuses = new[] { "100.013" } }).GetTotalAmountApprovedBySource("SourceType3", "invalid");
            Assert.NotNull(results);
            Assert.Equal(results, default(double));
        }

        [Fact]
        public void GetApprovedLoanAmountTotalBySourceWhenItDoesNotQualifiesAnyRecords()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            repository.Add(new FilterView
            {
                ApplicationId = "application001",
                Applicant = "applicant001",
                Amount = 40000,
                MaxApproval = 30000,
                PaymentAmount = 10000,
                Payout = 10000,
                SourceId = _sourceId,
                SourceType = _sourceType,
                StatusCode = "100.01",
                StatusName = "Rejected"
            });

            var configuration = new Configuration()
            {
                ApprovedStatuses = new[] { "300.03", "500.03" }
            };

            // try gets the object back            
            var service = GetService(repository, null, null, null, configuration);
            var result = service.GetTotalAmountApprovedBySource(_sourceType, _sourceId);

            Assert.NotNull(result);
            Assert.Equal(result, default(double));
        }

        [Fact]
        public void GetApprovedLoanAmountTotalBySourceWhenItDoesQualifiesRecords()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            repository.Add(new FilterView
            {
                ApplicationId = "application001",
                Applicant = "applicant001",
                Amount = 40000,
                MaxApproval = 30000,
                PaymentAmount = 10000,
                Payout = 10000,
                SourceId = _sourceId,
                SourceType = _sourceType,
                StatusCode = "300.03",
                StatusName = "Approved"
            });

            repository.Add(new FilterView
            {
                ApplicationId = "application001",
                Applicant = "applicant001",
                Amount = 40000,
                MaxApproval = 30000,
                PaymentAmount = 10000,
                Payout = 10000,
                SourceId = _sourceId,
                SourceType = _sourceType,
                StatusCode = "100.01",
                StatusName = "Rejected"
            });

            repository.Add(new FilterView
            {
                ApplicationId = "application001",
                Applicant = "applicant001",
                Amount = 5025.25,
                MaxApproval = 30000,
                PaymentAmount = 10000,
                Payout = 10000,
                SourceId = _sourceId,
                SourceType = _sourceType,
                StatusCode = "300.03",
                StatusName = "Approved"
            });

            repository.Add(new FilterView
            {
                ApplicationId = "application001",
                Applicant = "applicant001",
                Amount = 6025.25,
                MaxApproval = 30000,
                PaymentAmount = 10000,
                Payout = 10000,
                SourceId = _sourceId,
                SourceType = _sourceType,
                StatusCode = "500.03",
                StatusName = "Approved"
            });

            var configuration = new Configuration()
            {
                ApprovedStatuses = new[] { "300.03", "500.03" }
            };

            // try gets the object back
            var service = GetService(repository, null, null, null, configuration);
            var result = service.GetTotalAmountApprovedBySource(_sourceType, _sourceId);

            Assert.NotNull(result);
            Assert.Equal(result, 51050.5);
        }

        [Fact]
        public void GetApprovedLoanAmountTotalBySourceWhenItHasNoData()
        {
            var _sourceType = "merchant";
            var _sourceId = "merchant001";

            // try gets the object back
            var configuration = new Configuration()
            {
                ApprovedStatuses = new[] { "300.03", "500.03" }
            };

            var service = GetService(null, null, null, null, configuration);

            var result = service.GetTotalAmountApprovedBySource(_sourceType, _sourceId);

            Assert.NotNull(result);
            Assert.Equal(result, default(double));
        }

        [Fact]
        public void GetByApplicationNumberExecutionOk()
        {
            // arrange
            var repo = new FakeRepository();
            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var entries = new[]
            {
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView
                };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                entry.ApplicationId = $"ApplicationId{i}";
                entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(1).Day);

                return entry;
            }).ToList().ForEach((filterView) =>
            {
                repo.Add(filterView);
            });

            Logger.Setup(x => x.Error(It.IsAny<string>()));

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            var firstDayOfMonth = new DateTimeOffset(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0));

            // action
            var result = GetService(repo, null, Logger.Object, TenantTime.Object).GetByApplicationNumber(sourceType, sourceId, "ApplicationId2");

            // assert
            Assert.NotNull(result);
            Assert.Equal("ApplicationId2", result.Result.ApplicationId);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void GetByApplicationNumberExecutionOkButIfNoData()
        {
            Assert.ThrowsAsync<NotFoundException>(async () =>
           {
               // arrange
               var repo = new FakeRepository();
               var sourceType = InMemoryFilterView.SourceType;
               var sourceId = InMemoryFilterView.SourceId;
               var entries = new[]
               {
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView
               };
               entries.Select((entry, i) =>
               {
                   i++;
                   entry.Id = $"ApplicationId{i}";
                   entry.ApplicationId = $"ApplicationId{i}";
                   entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(1).Day);

                   return entry;
               }).ToList().ForEach((filterView) =>
               {
                   repo.Add(filterView);
               });

               Logger.Setup(x => x.Error(It.IsAny<string>()));

               TenantTime.Setup(x => x.TimeZone)
                   .Returns(TimeZoneInfo.Utc);

               TenantTime.Setup(x => x.Now)
                   .Returns(DateTime.Now);

               var firstDayOfMonth = new DateTimeOffset(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0));

               // action
               var result = await GetService(repo, null, Logger.Object, TenantTime.Object).GetByApplicationNumber(sourceType, sourceId, "ApplicationNumber");
           });
        }

        [Fact]
        public void GetByApplicationNumberWhenInvalidSourceType()
        {
            Assert.ThrowsAsync<InvalidArgumentException>(() => GetService().GetByApplicationNumber(null, "SourceId3", "ApplicationNumber"));
        }

        [Fact]
        public void GetByApplicationNumberWhenInvalidSourceId()
        {
            Assert.ThrowsAsync<InvalidArgumentException>(() => GetService().GetByApplicationNumber("SourceType1", null, "ApplicationNumber"));
        }

        [Fact]
        public void GetByApplicationNumberWhenInvalidApplicationNumber()
        {
            Assert.ThrowsAsync<InvalidArgumentException>(() => GetService().GetByApplicationNumber("SourceType1", "SourceId1", null));
        }
        [Fact]
        public async Task GetAllExpiredApplicationsWhenNullStatusesNotExpired()
        {
            TenantTime.Setup(x => x.TimeZone)
                    .Returns(TimeZoneInfo.Utc);
            TenantTime.Setup(x => x.Now)
               .Returns(DateTime.Now);
            TenantTime.Setup(x => x.Today)
                .Returns(DateTime.Now);
            var repository = new FakeRepository(TenantTime.Object);

            // creates a filter view entry in the repository   
            var sourceType = "application";
            var sourceId = "application001";
            var submittedDate = new DateTime(2010, 01, 01);

            // filter object 1
            var filter = GetData();
            filter.SourceId = sourceId;
            filter.SourceType = sourceType;
            filter.StatusCode = "200.01";
            filter.Submitted = submittedDate;

            repository.Add(filter);


            // filter object 2
            filter = GetData();
            filter.SourceId = sourceId;
            filter.SourceType = sourceType;
            filter.StatusCode = "300.02";
            filter.Submitted = submittedDate;

            repository.Add(filter);
            
           
          
            // creates a service and gets item to match
            var service = GetService(repository, EventHubClient.Object, Logger.Object, TenantTime.Object, new Configuration());

          Assert.ThrowsAsync<ArgumentException>(() => GetService().GetAllExpiredApplications());
        

        }
        [Fact]
        public async Task GetAllExpiredApplicationsWhenEmptyStatusesNotExpired()
        {
            TenantTime.Setup(x => x.TimeZone)
                    .Returns(TimeZoneInfo.Utc);
            TenantTime.Setup(x => x.Now)
               .Returns(DateTime.Now);
            TenantTime.Setup(x => x.Today)
                .Returns(DateTime.Now);
            var repository = new FakeRepository(TenantTime.Object);

            // creates a filter view entry in the repository   
            var sourceType = "application";
            var sourceId = "application001";
            var submittedDate = new DateTime(2010, 01, 01);

            // filter object 1
            var filter = GetData();
            filter.SourceId = sourceId;
            filter.SourceType = sourceType;
            filter.StatusCode = "200.01";
            filter.Submitted = submittedDate;

            repository.Add(filter);


            // filter object 2
            filter = GetData();
            filter.SourceId = sourceId;
            filter.SourceType = sourceType;
            filter.StatusCode = "300.02";
            filter.Submitted = submittedDate;

            repository.Add(filter);



            // creates a service and gets item to match
            var service = GetService(repository, EventHubClient.Object, Logger.Object, TenantTime.Object, new Configuration() { StatusesNotExpired = new string[] { } });

            Assert.ThrowsAsync<ArgumentException>(() => GetService().GetAllExpiredApplications());


        }
        [Fact]
        public async Task GetAllExpiredApplicationsWhenItHasData()
        {
            TenantTime.Setup(x => x.TimeZone)
                    .Returns(TimeZoneInfo.Utc);
            TenantTime.Setup(x => x.Now)
               .Returns(DateTime.Now);
            TenantTime.Setup(x => x.Today)
                .Returns(DateTime.Now);
            var repository = new FakeRepository(TenantTime.Object);

            // creates a filter view entry in the repository   
            var sourceType = "application";
            var sourceId = "application001";
            var submittedDate = new DateTime(2010, 01, 01);

            // filter object 1
            var filter = GetData();
            filter.SourceId = sourceId;
            filter.SourceType = sourceType;
            filter.StatusCode = "200.01";
            filter.Submitted = submittedDate;

            repository.Add(filter);


            // filter object 2
            filter = GetData();
            filter.SourceId = sourceId;
            filter.SourceType = sourceType;
            filter.StatusCode = "300.02";
            filter.Submitted = submittedDate;

            repository.Add(filter);

            // creates a service and gets item to match
            var service = GetService(repository, EventHubClient.Object, Logger.Object, TenantTime.Object, new Configuration() { StatusesNotExpired = new string[] { "400.03", "500.02", "500.03", "500.05", "500.06", "600.01", "600.02", "600.03", "600.04" } });

            var data = await service.GetAllExpiredApplications();
            Assert.NotNull(data);
            Assert.NotEmpty(data);
            Assert.True(data.ToList().TrueForAll(x => x.SourceId == sourceId));
            Assert.True(data.ToList().TrueForAll(x => x.SourceType == sourceType));
            Assert.Equal(data.Count(), 2);
        }
        [Fact]
        public async Task GetAllExpiredApplicationsWhenItHasNoData()
        {
            TenantTime.Setup(x => x.TimeZone)
                    .Returns(TimeZoneInfo.Utc);
            TenantTime.Setup(x => x.Now)
               .Returns(DateTime.Now);
            TenantTime.Setup(x => x.Today)
                .Returns(DateTime.Now);
            var repository = new FakeRepository(TenantTime.Object);

            // creates a filter view entry in the repository   
            var sourceType = "application";
            var sourceId = "application001";
            var submittedDate = new DateTime(2010, 01, 01);

            // filter object 1
            var filter = GetData();
            filter.SourceId = sourceId;
            filter.SourceType = sourceType;
            filter.StatusCode = "400.03";
            filter.Submitted = submittedDate;

            repository.Add(filter);


            // filter object 2
            filter = GetData();
            filter.SourceId = sourceId;
            filter.SourceType = sourceType;
            filter.StatusCode = "500.02";
            filter.Submitted = submittedDate;

            repository.Add(filter);

            // creates a service and gets item to match
            var service = GetService(repository, EventHubClient.Object, Logger.Object, TenantTime.Object, new Configuration() { StatusesNotExpired = new string[] { "400.03", "500.02", "500.03", "500.05", "500.06", "600.01", "600.02", "600.03", "600.04" } });

            var data = await service.GetAllExpiredApplications();
            Assert.NotNull(data);
            Assert.Empty(data);
           
        }
        [Fact]
        public void InitServiceWithoutTokenReader()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new ApplicationFilterService
                (
                    Mock.Of<IFilterViewRepository>(),
                    Mock.Of<ILogger>(),
                    Mock.Of<ITenantTime>(),
                    Mock.Of<Configuration>(),
                    null,
                    Mock.Of<ITokenHandler>(),
                    Mock.Of<IIdentityService>()
                );
            });
        }

        [Fact]
        public void InitServiceWithoutTokenHandler()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new ApplicationFilterService
                (
                    Mock.Of<IFilterViewRepository>(),
                    Mock.Of<ILogger>(),
                    Mock.Of<ITenantTime>(),
                    Mock.Of<Configuration>(),
                    Mock.Of<ITokenReader>(),
                    null,
                    Mock.Of<IIdentityService>()
                );
            });
        }

        [Fact]
        public void GetAllAssignedApplicationsWhenNotUserFound()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var tokenParser = new Mock<ITokenHandler>();
                tokenParser.Setup(x => x.Parse(It.IsAny<string>())).Returns(new Token { });
                var service = GetService(null, null, null, null, null, null, tokenParser.Object);
                service.GetAllAssignedApplications();
            });
        }



        [Fact]
        public async void GetAllUnassigned()
        {
            // arrange
            var fakeRepository = new FakeRepository();
            var filterViews = new[]
            {
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView
            };

            filterViews.Select((item, idx) =>
            {
                idx++;

                if (idx == 1)
                {
                    return new FilterView
                    {
                        Id = $"ApplicationId2{idx}",
                        ApplicationId = $"ApplicationNumber00{idx}",
                        Applicant = $"Applicant00{idx}",
                        Ssn = "ABCD1234",
                        Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                        ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                        StatusCode = "100.02",
                        Amount = 12000,
                        MaxApproval = 10000,
                        PaymentAmount = 5000,
                        Payout = 5000,
                        SourceId = "SourceId1",
                        SourceType = "SourceType1",
                        Tags = new[] { "tag001", "tag002", "tag003" },
                        Assignees = new[] {
                        new Assignment
                        {
                            Id = "1",
                            AssignedBy = "Eduardo",
                            AssignedOn = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            Assignee = "Eduardo",
                            EntityId = "EntityId",
                            EntityType = "Application",
                            EscalationId = "EscalationId",
                            Role = "Credit Specialist",
                            TenantId = TenantId
                        }
                        }.ToDictionary(x => x.Role, x => x.Assignee)
                    };
                }

                if (idx == 2)
                {
                    return new FilterView
                    {
                        Id = $"ApplicationId2{idx}",
                        ApplicationId = $"ApplicationNumber00{idx}",
                        Applicant = $"Applicant00{idx}",
                        Ssn = "ABCD1234",
                        Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                        ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                        StatusCode = "100.02",
                        Amount = 12000,
                        MaxApproval = 10000,
                        PaymentAmount = 5000,
                        Payout = 5000,
                        SourceId = "SourceId1",
                        SourceType = "SourceType1",
                        Tags = new[] { "tag001", "tag002", "tag003" },
                        Assignees = null
                    };
                }

                return new FilterView
                {
                    Id = $"ApplicationId2{idx}",
                    ApplicationId = $"ApplicationNumber00{idx}",
                    Applicant = $"Applicant00{idx}",
                    Ssn = "ABCD1234",
                    Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                    ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                    StatusCode = "100.02",
                    Amount = 12000, 
                    MaxApproval = 10000,
                    PaymentAmount = 5000,
                    Payout = 5000,
                    SourceId = "SourceId1",
                    SourceType = "SourceType1",
                    Tags = new[] { "tag001", "tag002", "tag003" },
                    Assignees = new[] {
                        new Assignment
                        {
                            Id = "1",
                            AssignedBy = "Eduardo",
                            AssignedOn = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            Assignee = "Eduardo",
                            EntityId = "EntityId",
                            EntityType = "Application",
                            EscalationId = "EscalationId",
                            Role = "Credit Specialist",
                            TenantId = TenantId
                        },
                        new Assignment
                        {
                            Id = "1",
                            AssignedBy = "Eduardo",
                            AssignedOn = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            Assignee = "Eduardo",
                            EntityId = "EntityId",
                            EntityType = "Application",
                            EscalationId = "EscalationId",
                            Role = "QA",
                            TenantId = TenantId
                        },
                                        new Assignment
                        {
                            Id = "1",
                            AssignedBy = "Marcio",
                            AssignedOn = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            Assignee = "Marcio",
                            EntityId = "EntityId",
                            EntityType = "Application",
                            EscalationId = "EscalationId",
                            Role = "VP of Sales",
                            TenantId = TenantId
                        }
                    }.ToDictionary(x => x.Role, x => x.Assignee)
                };

            }).ToList().ForEach(item => fakeRepository.Add(item));

            TenantTime.Setup(x => x.TimeZone)
                    .Returns(TimeZoneInfo.Utc);
            TenantTime.Setup(x => x.Now)
               .Returns(DateTime.Now);
            TenantTime.Setup(x => x.Today)
                .Returns(DateTime.Now);

            TokenHandler.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(InMemoryToken);

            var roles = new[] { "QA", "VP of Sales" };
            IdentityService.Setup(x => x.GetUserRoles(It.IsAny<string>()))
                .ReturnsAsync(roles);

            // action
            var service = GetService
                (
                    fakeRepository, 
                    null, 
                    null, 
                    TenantTime.Object, 
                    null, 
                    null,
                    TokenHandler.Object, 
                    IdentityService.Object
                );

            var allUnassigned = await service.GetAllUnassigned();

            // assert
            Assert.NotEmpty(allUnassigned);
            Assert.Equal(2, allUnassigned.Count());
        }

        [Fact]
        public void GetAllUnassignedWhenCurrentUserHasNoRoles()
        {
        }


        [Fact]
        public void GetAssignedByStatusWhenNullUsername()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { });
           
            var service = GetService(repository,null, null, null, null, null, tokenParser.Object);
            
            Assert.ThrowsAsync<ArgumentException>(async () => await service.GetAssignedByStatus(new string[] { "SourceId3" }));
        }

        [Fact]
        public void GetAssignedByStatusWhenNullStatuses()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { Subject = "Kinjal" });

            var service = GetService(repository, null, null, null, null, null, tokenParser.Object);

            Assert.ThrowsAsync<InvalidArgumentException>(async () => await service.GetAssignedByStatus(null));
            Assert.ThrowsAsync<InvalidArgumentException>(async () => await service.GetAssignedByStatus(new string[] { }));
        }

        [Fact]
        public void GetAssignedByStatusWhenValidStatuses()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Assignees =  new Dictionary<string, string> { ["kinjal"]= "kinjal" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { Subject = "kinjal" });

            var service = GetService(repository, null, null, null, null, null, tokenParser.Object);

           var result = Task.Run(async() => await service.GetAssignedByStatus(new string[] { "100.01" })).Result;

            Assert.NotNull(result);
            Assert.NotEmpty(result);

        }

        [Fact]
        public void GetAssignedByTagsWhenNullUsername()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Tags = new string[] { "tag1" };
            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Tags = new string[] { "tag1" };
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { });

            var service = GetService(repository, null, null, null, null, null, tokenParser.Object);

            Assert.ThrowsAsync<ArgumentException>(async () => await service.GetAssignedByTags(new string[] { "tag1" }));
        }

        [Fact]
        public void GetAssignedByTagsWhenNullTags()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Tags = new string[] { "tag1" };
            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Tags = new string[] { "tag1" };
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { Subject = "Kinjal" });

            var service = GetService(repository, null, null, null, null, null, tokenParser.Object);

            Assert.ThrowsAsync<InvalidArgumentException>(async () => await service.GetAssignedByTags (null));
            Assert.ThrowsAsync<InvalidArgumentException>(async () => await service.GetAssignedByTags (new string[] { }));
        }

        [Fact]
        public void GetAssignedByTagsWhenValidTags()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { Subject = "kinjal" });

            var service = GetService(repository, null, null, null, null, null, tokenParser.Object);

            var result = Task.Run(async () => await service.GetAssignedByTags(new string[] { "tag1" })).Result;

            Assert.NotNull(result);
            Assert.NotEmpty(result);

        }

        [Fact]
        public void GetAllWhenValid()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };
            repository.Add(filter);
            // creates a service and gets item to match
           

            var service = GetService(repository, null, null, null, null, null,null);

            var result = Task.Run(async () => await service.GetAll()).Result;

            Assert.NotNull(result);
            Assert.NotEmpty(result);

        }

        [Fact]
        public void GetAlldByStatusesWhenNullStatuses()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };
            repository.Add(filter);

            Assert.ThrowsAsync<InvalidArgumentException>(async () => await GetService().GetByStatus(null));
            Assert.ThrowsAsync<InvalidArgumentException>(async () => await GetService().GetByStatus(new string[] { }));

        }

        [Fact]
        public void GetAlldByStatusesWhenValid()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };
            repository.Add(filter);
            var service = GetService(repository);
            var result = Task.Run(async () => await service.GetByStatus(new string[] {"100.02" })).Result;

            Assert.NotNull(result);
            Assert.NotEmpty(result);

        }


        [Fact]
        public void GetAlldByTagsWhenNullTags()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };
            repository.Add(filter);

            Assert.ThrowsAsync<InvalidArgumentException>(async () => await GetService().GetByTags(null));
            Assert.ThrowsAsync<InvalidArgumentException>(async () => await GetService().GetByTags(new string[] { }));

        }

        [Fact]
        public void GetAlldByTagsWhenValid()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };
            repository.Add(filter);
            var service = GetService(repository);
            var result = Task.Run(async () => await service.GetByTags(new string[] { "tag1" })).Result;

            Assert.NotNull(result);
            Assert.NotEmpty(result);

        }
    }
}